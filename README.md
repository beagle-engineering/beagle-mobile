# Beagle app

## Get up and running with Flutter

Flutter is a cross-platform mobile/web framework used to build natively compiled apps. We are using it to deploy Beagle's Android and iOS apps.

### Setup

- [Install Flutter](https://flutter.dev/docs/get-started/install)
- [Set up your editor](https://flutter.dev/docs/get-started/editor?tab=vscode)
- Clone the beagle project
- Get dependencies
```
flutter pub get
```
- add `.env` file at the root of the project and add the url pointing to your local Beagle API:
```
BEAGLE_API_URL=http://localhost:5000
```

### Run & Debug

[Run & debug your flutter app](https://flutter.dev/docs/get-started/test-drive?tab=vscode)

### Documentation

- [Flutter cookbooks](https://flutter.dev/docs/cookbook)
- [Testing flutter apps](https://flutter.dev/docs/testing)
- [Dart language docs](https://dart.dev/guides)
- [Dart and Flutter publicly available packages](https://pub.dev/)

## Beagle

### Project structure

- All custom code is under `/lib`
  - `/l10n` is the auto-generated code used for internationalization.
  - `/models` is used to declare business objects and classes used throughout the app.
  - `/ui` for all UI related code: mainly custom widgets and screens.
  - `/test` holds all tests.

- All platform specific compiled code is under `/android` ans `/ios`, It also holds platform/app store specific deployment configurations.

- `pubspec.yaml` holds dependency configuration.


#### Commands

Splash screen
```
flutter pub pub run flutter_native_splash:create
```
