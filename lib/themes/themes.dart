import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'base.dart';

class BeagleClassicTheme extends BaseTheme {
  static const beagleColor = Color(0xFF2356FF);
  static const green = Color(0xFF52DE97);
  static const red = Color(0xFFFF575F);
  static const yellow = Color(0xFFFFBF00);
  // Background color.
  static const blueWhite = Color(0xFFECEFF8);
  static const grey = Color(0xFFE5E5E5);
  static const blueGrey = Color(0xFFECEFF8);

  static const dark = Color(0xFF2B3451);
  static const light = Color(0xFF9EA6BF);
  static const white = Color(0xFFFFFFFF);
  static const simpleBlack = Color(0xFF000000);
  static const darkerGrey = Color(0xFF5F6883);
  static const pink = Color(0xFFEE8EBC);
  static const orange = Color(0xFFFDC28A);

  Color primary = beagleColor;

  Color success = green;
  Color failure = red;
  Color warning = yellow;

  Color progressBackground = blueGrey;
  Color background = blueWhite;
  Color neutralBackground = white;
  Color settingMenu = darkerGrey;
  Color classicPink = pink;
  Color classicOrange = orange;

  JourneysBaseTheme journeys =
      JourneysBaseTheme(beagleColor, yellow, red, white, white);

  ListTheme lists = ListTheme(light, dark, white);

  TextualContentTheme textContent = TextualContentTheme(dark, light, light);

  Color text = simpleBlack;

  Color title2 = light;

  Color title = dark;

  Color overlay;

  Color animationOverlayMedium = simpleBlack.withOpacity(0.7);
  Color animationOverlayStrong = simpleBlack.withOpacity(0.85);

  Brightness brightness = Brightness.dark;
  SystemUiOverlayStyle statusBar =
      SystemUiOverlayStyle.dark.copyWith(statusBarColor: Colors.black);

  BoxShadow boxShadow = BoxShadow(color: Colors.transparent);
  BoxShadow boxShadowButton = BoxShadow(color: Colors.transparent);

  AppIconEnum appIcon = AppIconEnum.BEAGLECLASSIC;
}

class BeagleDarkTheme extends BaseTheme {
  static const blue = Color(0xFF4A90E2);

  static const orange = Color(0xFFF9AE42);

  static const orangeDark = Color(0xFF9C671E);

  static const blueDark = Color(0xFF000034);

  static const blueLightish = Color(0xFF080840);

  static const blueDarkest = Color(0xFF000034);

  static const curWhite = Color(0xFFFFFFFF);

  static const curBlack = Color(0xFF000000);
  static const darkerGrey = Color(0xFF5F6883);
  static const pink = Color(0xFFEE8EBC);

  Color classicPink = pink;
  Color classicOrange = orange;

  Color primary = blue;
  Color settingMenu = darkerGrey;
  Color primary60 = blue.withOpacity(0.6);
  Color primary45 = blue.withOpacity(0.45);
  Color primary30 = blue.withOpacity(0.3);
  Color primary20 = blue.withOpacity(0.2);
  Color primary15 = blue.withOpacity(0.15);
  Color primary10 = blue.withOpacity(0.1);
  Color progressBackground = blue;

  Color success = orange;
  Color success60 = orange.withOpacity(0.6);
  Color success30 = orange.withOpacity(0.3);
  Color success15 = orange.withOpacity(0.15);

  Color successDark = orangeDark;
  Color successDark30 = orangeDark.withOpacity(0.3);

  Color background = blueDark;
  Color background40 = blueDark.withOpacity(0.4);
  Color background00 = blueDark.withOpacity(0.0);

  Color backgroundDark = blueLightish;
  Color backgroundDark00 = blueLightish.withOpacity(0.0);

  Color backgroundDarkest = blueDarkest;

  Color text = curWhite.withOpacity(0.9);
  Color text60 = curWhite.withOpacity(0.6);
  Color text45 = curWhite.withOpacity(0.45);
  Color text30 = curWhite.withOpacity(0.3);
  Color text20 = curWhite.withOpacity(0.2);
  Color text15 = curWhite.withOpacity(0.15);
  Color text10 = curWhite.withOpacity(0.1);
  Color text05 = curWhite.withOpacity(0.05);
  Color text03 = curWhite.withOpacity(0.03);

  Color overlay90 = curBlack.withOpacity(0.9);
  Color overlay85 = curBlack.withOpacity(0.85);
  Color overlay80 = curBlack.withOpacity(0.8);
  Color overlay70 = curBlack.withOpacity(0.7);
  Color overlay50 = curBlack.withOpacity(0.5);
  Color overlay30 = curBlack.withOpacity(0.3);
  Color overlay20 = curBlack.withOpacity(0.2);

  Color animationOverlayMedium = curBlack.withOpacity(0.7);
  Color animationOverlayStrong = curBlack.withOpacity(0.85);

  Brightness brightness = Brightness.dark;
  SystemUiOverlayStyle statusBar =
      SystemUiOverlayStyle.light.copyWith(statusBarColor: Colors.transparent);

  BoxShadow boxShadow = BoxShadow(color: Colors.transparent);
  BoxShadow boxShadowButton = BoxShadow(color: Colors.transparent);

  AppIconEnum appIcon = AppIconEnum.BEAGLEDARK;
}
