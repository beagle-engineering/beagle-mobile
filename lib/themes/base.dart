import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

abstract class BaseTheme {
  Color primary;

  Color success;
  Color failure;
  Color warning;

  Color background;
  Color neutralBackground;
  Color progressBackground;

  JourneysBaseTheme journeys;
  ListTheme lists;

  Color text;
  Color title;
  Color title2;
  Color settingMenu;
  Color classicOrange;
  Color classicPink;

  Color overlay;

  Color animationOverlayMedium;
  Color animationOverlayStrong;

  Brightness brightness;
  SystemUiOverlayStyle statusBar;

  BoxShadow boxShadow;
  BoxShadow boxShadowButton;

  // App icon (iOS only)
  AppIconEnum appIcon;
}

class JourneysBaseTheme {
  Color first;
  Color second;
  Color third;

  Color title;
  Color description;

  JourneysBaseTheme(
      this.first, this.second, this.third, this.title, this.description);
}

class TextualContentTheme {
  Color title;
  Color body;
  Color section;

  TextualContentTheme(this.title, this.body, this.section);
}

class ListTheme {
  Color elementName;
  Color elementBackground;
  Color title;

  ListTheme(this.title, this.elementName, this.elementBackground);
}

enum AppIconEnum { BEAGLECLASSIC, BEAGLEDARK }

class AppIcon {
  static const _channel = MethodChannel('fappchannel');

  static Future<void> setAppIcon(AppIconEnum iconToChange) async {
    if (!Platform.isIOS) {
      return null;
    }
    String iconStr = "BEAGLECLASSIC";
    switch (iconToChange) {
      case AppIconEnum.BEAGLEDARK:
        iconStr = "beagledark";
        break;
      case AppIconEnum.BEAGLECLASSIC:
      default:
        iconStr = "beagleclassic";
        break;
    }
    final Map<String, dynamic> params = <String, dynamic>{
      'icon': iconStr,
    };
    return await _channel.invokeMethod('changeIcon', params);
  }
}
