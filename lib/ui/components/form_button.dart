import 'package:flutter/material.dart';
import 'package:getbeagle/appstate_container.dart';

class FormButton extends StatelessWidget {
  const FormButton(
      {Key key,
      this.leftMargin,
      this.rightMargin,
      this.heroTag,
      this.text,
      this.onPressed,
      this.backgroundColor,
      this.textColor,
      this.height,
      this.borderRadius})
      : super(key: key);

  FormButton.whiteDefault(
      {BuildContext context,
      String heroTag,
      String text,
      void Function() onPressed})
      : leftMargin = 30,
        rightMargin = 30,
        height = 58,
        backgroundColor = StateContainer.of(context).curTheme.neutralBackground,
        borderRadius = 12,
        textColor = StateContainer.of(context).curTheme.primary,
        onPressed = onPressed,
        text = text,
        heroTag = heroTag;

  FormButton.primaryDefault(
      {BuildContext context,
      String heroTag,
      String text,
      void Function() onPressed})
      : leftMargin = 30,
        rightMargin = 30,
        height = 58,
        backgroundColor = StateContainer.of(context).curTheme.primary,
        borderRadius = 12,
        textColor = StateContainer.of(context).curTheme.neutralBackground,
        onPressed = onPressed,
        text = text,
        heroTag = heroTag;

  final String heroTag;
  final double leftMargin;
  final double rightMargin;
  final double height;
  final double borderRadius;
  final Color backgroundColor;
  final Color textColor;
  final String text;

  final void Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: leftMargin, right: rightMargin),
      width: double.infinity,
      height: height,
      child: FloatingActionButton.extended(
        heroTag: heroTag,
        backgroundColor: backgroundColor,
        elevation: 0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius)),
        label: Text(text,
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 16, color: textColor)),
        onPressed: onPressed,
      ),
    );
  }
}
