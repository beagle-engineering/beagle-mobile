import 'package:flutter/material.dart';
import 'package:getbeagle/appstate_container.dart';

class ContentCard extends StatelessWidget {
  ContentCard({Key key, this.title, this.onTap, this.cardColor})
      : super(key: key);

  final String title;
  final void Function() onTap;
  final Color cardColor;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onTap,
        child: Card(
          margin: const EdgeInsets.only(left: 16, bottom: 4),
          color: cardColor != null
              ? cardColor
              : StateContainer.of(context).curTheme.primary.withAlpha(250),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25.0),
          ),
          child: ContentCardContent(
            title: title,
            cardColor: cardColor,
          ),
        ));
  }
}

class ContentCardContent extends StatelessWidget {
  ContentCardContent({Key key, this.title = '', this.cardColor})
      : super(key: key);

  final String title;
  final Color cardColor;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(24),
        child: SizedBox(
            width: 100,
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Expanded(
                  child: Align(
                alignment: Alignment.bottomCenter,
                child: Text(
                  title.isNotEmpty ? title : "",
                  style: TextStyle(
                      color: StateContainer.of(context)
                          .curTheme
                          .neutralBackground
                          .withOpacity(0.95),
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                      height: 1.2),
                ),
              )),
            ])));
  }
}
