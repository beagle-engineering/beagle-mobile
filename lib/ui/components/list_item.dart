import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/utils/strings.dart';
import 'package:percent_indicator/percent_indicator.dart';

class ListItem extends StatelessWidget {
  const ListItem(
      {Key key,
      this.title,
      this.iconColor,
      this.detail,
      this.icon,
      this.progressValue = 0.0,
      this.progressBarColor,
      this.progressValueString,
      this.onTap,
      this.svgImage})
      : super(key: key);

  final String title;
  final String detail;
  final double progressValue;
  final IconData icon;
  final Color iconColor;
  final Color progressBarColor;
  final String progressValueString;
  final void Function() onTap;
  final String svgImage;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onTap,
        child: Container(
            margin: const EdgeInsets.only(bottom: 10),
            decoration: BoxDecoration(
                color: Colors.transparent,
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.08),
                    blurRadius: 16.0,
                    offset: Offset(0, 4),
                    spreadRadius: -9,
                  ),
                ],
                borderRadius: BorderRadius.circular(24.0)),
            child: Card(
              color: StateContainer.of(context).curTheme.neutralBackground,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24.0),
              ),
              child: ListItemContent(
                  icon: icon,
                  iconColor: iconColor,
                  title: title,
                  detail: detail,
                  progressValue: progressValue,
                  progressBarColor: progressBarColor,
                  progressValueString: progressValueString,
                  svgImage: svgImage),
            )));
  }
}

class ListItemContent extends StatelessWidget {
  ListItemContent(
      {Key key,
      this.icon,
      this.onTap,
      this.iconColor,
      this.progressBarColor,
      this.title = '',
      this.detail = '',
      this.progressValueString = '',
      this.progressValue = 0.0,
      this.svgImage})
      : super(key: key);

  final IconData icon;
  final String title;
  final String detail;
  final void Function() onTap;
  final double progressValue;
  final String progressValueString;
  final Color iconColor;
  final Color progressBarColor;
  final String svgImage;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
        child: Center(
            child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          svgImage == null
              ? Container(
                  margin: const EdgeInsets.only(top: 23, left: 24, bottom: 20),
                  alignment: Alignment.center,
                  width: 57,
                  height: 57,
                  padding: const EdgeInsets.all(7),
                  decoration: BoxDecoration(
                      color: iconColor.withOpacity(0.2),
                      borderRadius: BorderRadius.all(Radius.circular(13))),
                  child: Center(
                      heightFactor: 0.1,
                      child: Icon(icon, color: iconColor.withOpacity(0.9))))
              : Container(
                  width: 57,
                  height: 57,
                  //padding: const EdgeInsets.all(7),
                  decoration: BoxDecoration(
                      //color: iconColor.withOpacity(0.2),
                      borderRadius: BorderRadius.all(Radius.circular(13))),
                  margin: const EdgeInsets.only(
                      top: 23, left: 24, bottom: 20, right: 8),
                  alignment: Alignment.center,
                  child: SvgPicture.asset(
                    svgImage,
                    width: 57,
                    height: 57,
                    placeholderBuilder: (BuildContext context) => Container(
                        margin: const EdgeInsets.only(
                            top: 23, left: 24, bottom: 20),
                        alignment: Alignment.center,
                        width: 57,
                        height: 57,
                        padding: const EdgeInsets.all(7),
                        decoration: BoxDecoration(
                            color: iconColor.withOpacity(0.2),
                            borderRadius:
                                BorderRadius.all(Radius.circular(13))),
                        child: Center(
                            heightFactor: 0.1,
                            child:
                                Icon(icon, color: iconColor.withOpacity(0.9)))),
                  )),
          Column(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
            Container(
                width: 210,
                child: Row(children: [
                  Flexible(
                    child: Container(
                        //color: Colors.yellow,
                        padding: const EdgeInsets.only(top: 16, left: 8),
                        //height: 28,
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            title.isNotEmpty ? capitalize(title) : "",
                            style: TextStyle(
                                color:
                                    StateContainer.of(context).curTheme.title,
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                height: 1.21),
                            overflow: TextOverflow.ellipsis,
                          ),
                        )),
                  ),
                  progressValueString != null
                      ? Container(
                          margin:
                              const EdgeInsets.only(top: 16, left: 3, right: 0),
                          height: 28,
                          width: 50,
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              progressValueString,
                              style: TextStyle(
                                color: progressBarColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 12,
                                //height: 0.5
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ))
                      : Container(height: 0),
                ])),
            Container(
              //color: Colors.blue,
              padding: const EdgeInsets.only(left: 10, bottom: 8),
              //height: 20,
              width: 210,
              child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    detail.isNotEmpty ? detail : '',
                    style: TextStyle(
                        color: StateContainer.of(context).curTheme.title2,
                        fontWeight: FontWeight.w600,
                        fontSize: 14,
                        height: 1.21),
                    textAlign: TextAlign.center,
                  )),
            ),
            progressValue != null
                ? Container(
                    margin: const EdgeInsets.only(left: 5, bottom: 16),
                    child: LinearPercentIndicator(
                      width: 210.0,
                      lineHeight: 8.0,
                      percent: progressValue,
                      progressColor: progressBarColor,
                      backgroundColor: StateContainer.of(context)
                          .curTheme
                          .progressBackground,
                    ),
                  )
                : Container(),
          ])
        ])));
  }
}
