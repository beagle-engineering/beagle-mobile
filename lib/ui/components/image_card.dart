import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';

class ImageCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 100,
        width: 50,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(25)),
        child: Card(
            elevation: 0,
            child: Stack(
              children: <Widget>[
                Icon(FeatherIcons.bookOpen),
                Text('test'),
              ],
            )));
  }
}
