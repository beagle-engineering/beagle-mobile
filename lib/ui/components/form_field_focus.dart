import 'package:flutter/material.dart';
import 'package:getbeagle/appstate_container.dart';

/// A text form widget applying our own styling on focus.
class StyledFocusForm extends StatefulWidget {
  final String Function(String) validator;
  final void Function(String) onSaved;
  final String label;
  final bool obscureText;

  StyledFocusForm({this.validator, this.onSaved, this.label, this.obscureText});

  @override
  _StyledFocusFormState createState() => _StyledFocusFormState();
}

class _StyledFocusFormState extends State<StyledFocusForm> {
  FocusNode _focusNode;
  Color usedColor;

  @override
  void initState() {
    super.initState();

    _focusNode = FocusNode();
    _focusNode.addListener(() {
      setState(() {
        if (_focusNode.hasFocus) {
          usedColor = StateContainer.of(context).curTheme.primary;
        } else {
          usedColor = StateContainer.of(context).curTheme.title;
        }
      });
    });
  }

  @override
  void dispose() {
    _focusNode.dispose();

    super.dispose();
  }

  void _requestFocus() {
    setState(() {
      FocusScope.of(context).requestFocus(_focusNode);
    });
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      focusNode: _focusNode,
      onTap: _requestFocus,
      decoration: InputDecoration(
        labelText: widget.label,
        labelStyle: TextStyle(
            color: _focusNode.hasFocus
                ? StateContainer.of(context).curTheme.primary
                : usedColor),
        focusedBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: StateContainer.of(context).curTheme.primary),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide:
              BorderSide(color: StateContainer.of(context).curTheme.background),
        ),
      ),
      validator: widget.validator,
      onSaved: widget.onSaved,
      obscureText: widget.obscureText,
    );
  }
}
