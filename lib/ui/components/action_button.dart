import 'package:flutter/material.dart';
import 'package:getbeagle/appstate_container.dart';

class ActionButton extends StatelessWidget {
  const ActionButton(
      {Key key,
      this.icon,
      this.iconColor,
      this.borderColor,
      this.onTap,
      this.width,
      this.height,
      this.borderRadius})
      : super(key: key);

  ActionButton.whiteSmall(
      {BuildContext context,
      IconData icon,
      void Function() onTap,
      Color iconColor})
      : icon = icon,
        onTap = onTap,
        iconColor = iconColor != null
            ? iconColor
            : StateContainer.of(context).curTheme.primary,
        borderColor = StateContainer.of(context).curTheme.neutralBackground,
        width = 60,
        height = 60,
        borderRadius = 30;

  ActionButton.primarySmall(
      {BuildContext context, IconData icon, void Function() onTap})
      : icon = icon,
        onTap = onTap,
        iconColor = StateContainer.of(context).curTheme.neutralBackground,
        borderColor = StateContainer.of(context).curTheme.primary,
        width = 60,
        height = 60,
        borderRadius = 30;

  ActionButton.whiteBig(
      {BuildContext context,
      IconData icon,
      void Function() onTap,
      Color iconColor})
      : icon = icon,
        onTap = onTap,
        iconColor = iconColor != null
            ? iconColor
            : StateContainer.of(context).curTheme.primary,
        borderColor = StateContainer.of(context).curTheme.neutralBackground,
        width = 80,
        height = 80,
        borderRadius = 40;

  ActionButton.primaryBig(
      {BuildContext context, IconData icon, void Function() onTap})
      : icon = icon,
        onTap = onTap,
        iconColor = StateContainer.of(context).curTheme.neutralBackground,
        borderColor = StateContainer.of(context).curTheme.primary,
        width = 80,
        height = 80,
        borderRadius = 40;

  final IconData icon;
  final Color iconColor;
  final Color borderColor;
  final void Function() onTap;
  final double width;
  final double height;
  final double borderRadius;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onTap,
        splashColor: iconColor.withOpacity(0.12),
        child: SizedBox(
            width: width,
            height: height,
            child: Container(
                padding: const EdgeInsets.all(5),
                decoration: BoxDecoration(
                    color: this.borderColor.withOpacity(0.35),
                    borderRadius:
                        BorderRadius.all(Radius.circular(borderRadius))),
                child: Container(
                    decoration: BoxDecoration(
                        color: this.borderColor,
                        borderRadius:
                            BorderRadius.all(Radius.circular(borderRadius))),
                    child: Center(child: Icon(icon, color: this.iconColor))))));
  }
}
