import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:getbeagle/appstate_container.dart';

/// A text form widget applying our own styling on focus.
class StyledFocusForm2 extends StatefulWidget {
  final String Function(String) validator;
  final void Function(String) onSaved;
  final String label;
  final bool obscureText;
  final Widget icon;
  final List<TextInputFormatter> inputFormatters;
  final void Function() customFocusBehaviour;
  final void Function() customUnfocusBehaviour;
  final TextInputType keyboardType;
  final TextEditingController controller;

  StyledFocusForm2(
      {this.validator,
      this.onSaved,
      this.label,
      this.obscureText,
      this.inputFormatters,
      this.icon,
      this.customFocusBehaviour,
      this.customUnfocusBehaviour,
      this.keyboardType,
      this.controller});

  @override
  _StyledFocusFormState2 createState() => _StyledFocusFormState2();
}

class _StyledFocusFormState2 extends State<StyledFocusForm2> {
  FocusNode _focusNode;
  Color usedColor;

  bool _obscureText = true;
  TextEditingController _controller = TextEditingController();

  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  void initState() {
    super.initState();
    _obscureText = widget.obscureText;
    _focusNode = FocusNode();
    if (widget.controller != null) {
      _controller = widget.controller;
    }
    _focusNode.addListener(() {
      setState(() {
        if (_focusNode.hasFocus) {
          usedColor = StateContainer.of(context).curTheme.primary;
        } else {
          usedColor = StateContainer.of(context).curTheme.title;
        }
      });
    });

    if (widget.customUnfocusBehaviour != null ||
        widget.customFocusBehaviour != null) {
      _focusNode.addListener(() {
        setState(() {
          if (_focusNode.hasFocus && widget.customFocusBehaviour != null) {
            widget.customFocusBehaviour();
          } else if (widget.customUnfocusBehaviour != null) {
            widget.customUnfocusBehaviour();
          }
        });
      });
    }
  }

  @override
  void dispose() {
    _focusNode.dispose();

    super.dispose();
  }

  void _requestFocus() {
    setState(() {
      FocusScope.of(context).requestFocus(_focusNode);
    });
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: widget.keyboardType,
      focusNode: _focusNode,
      onTap: _requestFocus,
      decoration: InputDecoration(
        suffixIcon: widget.obscureText
            ? IconButton(
                icon: Icon(
                    _obscureText ? FeatherIcons.eye : FeatherIcons.eyeOff,
                    color: StateContainer.of(context).curTheme.title2),
                onPressed: _toggle)
            : null,
        icon: widget.icon,
        labelText: widget.label,
        labelStyle: TextStyle(
            color: _focusNode.hasFocus
                ? StateContainer.of(context).curTheme.primary
                : StateContainer.of(context).curTheme.title2,
            fontSize: 14),
        focusedBorder: UnderlineInputBorder(
          borderSide:
              BorderSide(color: StateContainer.of(context).curTheme.primary),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide:
              BorderSide(color: StateContainer.of(context).curTheme.title2),
        ),
      ),
      validator: widget.validator,
      onSaved: widget.onSaved,
      obscureText: _obscureText,
      inputFormatters: widget.inputFormatters,
      controller: _controller,
    );
  }
}
