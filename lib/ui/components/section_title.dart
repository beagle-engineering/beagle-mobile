import 'package:flutter/material.dart';
import 'package:getbeagle/appstate_container.dart';

class SectionTitle extends StatelessWidget {
  const SectionTitle({
    Key key,
    this.title,
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(24, 20, 0, 20),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(title,
            style: TextStyle(
                color: StateContainer.of(context).curTheme.lists.title,
                fontWeight: FontWeight.bold,
                fontSize: 24)),
      ),
    );
  }
}
