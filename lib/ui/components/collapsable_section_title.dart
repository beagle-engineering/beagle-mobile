import 'package:flutter/material.dart';
import 'package:getbeagle/appstate_container.dart';

class CollapsableSectionTitle extends StatelessWidget {
  const CollapsableSectionTitle({
    Key key,
    this.title,
    this.collapseIcon,
  }) : super(key: key);

  final String title;
  final Widget collapseIcon;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(3, 20, 0, 16),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Row(children: [
          collapseIcon,
          Expanded(
              child: Container(
                  width: 250,
                  child: Text(
                    title,
                    style: TextStyle(
                        color: StateContainer.of(context).curTheme.lists.title,
                        fontWeight: FontWeight.bold,
                        fontSize: 24),
                    overflow: TextOverflow.ellipsis,
                  )))
        ]),
      ),
    );
  }
}
