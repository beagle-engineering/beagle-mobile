import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/ui/components/action_button.dart';

class ActionCard extends StatelessWidget {
  ActionCard(
      {Key key,
      this.icon,
      this.title,
      this.description,
      this.onTap,
      this.cardColor})
      : super(key: key);

  final IconData icon;
  final String title;
  final String description;
  final void Function() onTap;
  final Color cardColor;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onTap,
        child: Card(
          color: cardColor != null
              ? cardColor
              : StateContainer.of(context).curTheme.primary.withAlpha(250),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(24.0),
          ),
          child: CardContent(
            icon: icon,
            onTap: onTap,
            title: title,
            description: description,
            cardColor: cardColor,
          ),
        ));
  }
}

class CardContent extends StatelessWidget {
  CardContent(
      {Key key,
      this.icon,
      this.onTap,
      this.title = '',
      this.description = '',
      this.cardColor})
      : super(key: key);

  final IconData icon;
  final String title;
  final String description;
  final void Function() onTap;
  final Color cardColor;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
            padding: const EdgeInsets.only(left: 24),
            child: SizedBox(
                width: 200,
                child: Column(
                    //crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(children: [
                        Container(
                            margin: const EdgeInsets.only(top: 10),
                            alignment: Alignment.centerLeft,
                            width: 45,
                            height: 44,
                            padding: const EdgeInsets.all(7),
                            decoration: BoxDecoration(
                                color: StateContainer.of(context)
                                    .curTheme
                                    .neutralBackground
                                    .withOpacity(0.2),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(12))),
                            child: Center(
                                child: Icon(icon,
                                    color: StateContainer.of(context)
                                        .curTheme
                                        .neutralBackground
                                        .withOpacity(0.9)))),
                        Expanded(
                            child: Padding(
                                padding: EdgeInsets.only(
                                    left: 15, top: 24, bottom: 19),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text(
                                    title.isNotEmpty ? title : "",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: StateContainer.of(context)
                                            .curTheme
                                            .neutralBackground
                                            .withOpacity(0.95),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15,
                                        height: 1.2),
                                  ),
                                )))
                      ]),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 5, bottom: 12, top: 0),
                          child: Text(
                            description.isNotEmpty ? description : '',
                            style: TextStyle(
                                color: StateContainer.of(context)
                                    .curTheme
                                    .neutralBackground,
                                height: 1.5),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 3,
                          ),
                        ),
                      )
                    ]))),
        Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: Center(
              child: ActionButton.whiteSmall(
                  context: context,
                  icon: FeatherIcons.arrowRight,
                  onTap: onTap,
                  iconColor: cardColor != null
                      ? cardColor
                      : StateContainer.of(context).curTheme.primary),
            ))
      ],
    );
  }
}
