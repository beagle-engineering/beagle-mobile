import 'package:dotted_border/dotted_border.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/ui/components/action_button.dart';

class BigActionCard extends StatelessWidget {
  BigActionCard(
      {Key key,
      this.icon,
      this.title,
      this.description,
      this.onTap,
      this.cardColor})
      : super(key: key);

  final IconData icon;
  final String title;
  final String description;
  final void Function() onTap;
  final Color cardColor;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: cardColor != null
          ? cardColor
          : StateContainer.of(context).curTheme.primary,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(24.0),
      ),
      child: BigCardContent(
          icon: icon,
          onTap: onTap,
          title: title,
          description: description,
          cardColor: cardColor),
    );
  }
}

class BigCardContent extends StatelessWidget {
  BigCardContent(
      {Key key,
      this.icon,
      this.onTap,
      this.title = '',
      this.description = '',
      this.cardColor})
      : super(key: key);

  final IconData icon;
  final String title;
  final String description;
  final void Function() onTap;
  final Color cardColor;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(24, 33, 24, 33),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          //Expanded(
          //child:
          Text(
            title.isNotEmpty ? title : "",
            style: TextStyle(
                color: StateContainer.of(context)
                    .curTheme
                    .neutralBackground
                    .withOpacity(0.95),
                fontWeight: FontWeight.bold,
                fontSize: 15,
                height: 1.2),
            textAlign: TextAlign.left,
          ),
          //),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 8),
              child: Text(
                description.isNotEmpty ? description : '',
                style: TextStyle(
                    color:
                        StateContainer.of(context).curTheme.neutralBackground,
                    height: 1.5),
              ),
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(top: 0),
              child: Align(
                  alignment: Alignment.center,
                  child: DottedBorder(
                      color:
                          StateContainer.of(context).curTheme.neutralBackground,
                      borderType: BorderType.RRect,
                      radius: Radius.circular(20),
                      dashPattern: [4, 2],
                      child: Container(
                          width: 270,
                          height: 80,
                          margin: const EdgeInsets.fromLTRB(30, 20, 30, 20),
                          child: Align(
                              alignment: Alignment.center,
                              child: Column(children: [
                                SizedBox(
                                    height: 80,
                                    child: ActionButton.whiteBig(
                                        context: context,
                                        icon: icon,
                                        onTap: onTap,
                                        iconColor: cardColor)),
                              ]))))))
        ]));
  }
}
