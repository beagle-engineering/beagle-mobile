import 'package:flutter/material.dart';

class IconBox extends StatelessWidget {
  const IconBox(
      {Key key,
      this.icon,
      this.iconColor,
      this.boxColor,
      this.height,
      this.width})
      : super(key: key);

  final IconData icon;
  final Color iconColor;
  final Color boxColor;
  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(left: 30),
        alignment: Alignment.center,
        width: width,
        height: height,
        padding: const EdgeInsets.all(0),
        decoration: BoxDecoration(
            color: boxColor,
            borderRadius: BorderRadius.all(Radius.circular(7))),
        child: Center(child: Icon(icon, color: iconColor)));
  }
}
