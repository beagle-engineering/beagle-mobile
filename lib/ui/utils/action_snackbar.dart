import 'package:flutter/material.dart';
import 'package:getbeagle/appstate_container.dart';

void onErrorSnackbar(String message, GlobalKey<ScaffoldState> scaffoldKey,
    BuildContext context, Duration duration) {
  scaffoldKey.currentState.showSnackBar(SnackBar(
      duration: duration,
      backgroundColor: StateContainer.of(context).curTheme.failure,
      content: Text(
        message,
        style: TextStyle(
            color: StateContainer.of(context).curTheme.neutralBackground),
      )));
}

void onSuccessSnackbar(String message, GlobalKey<ScaffoldState> scaffoldKey,
    BuildContext context, Duration duration) {
  scaffoldKey.currentState.showSnackBar(SnackBar(
      duration: duration,
      backgroundColor: StateContainer.of(context).curTheme.success,
      content: Text(
        message,
        style: TextStyle(
            color: StateContainer.of(context).curTheme.neutralBackground),
      )));
}

void neutralSnackbar(String message, GlobalKey<ScaffoldState> scaffoldKey,
    BuildContext context, Duration duration) {
  scaffoldKey.currentState.showSnackBar(SnackBar(
      duration: duration,
      backgroundColor: StateContainer.of(context).curTheme.settingMenu,
      content: Text(
        message,
        style: TextStyle(
            color: StateContainer.of(context).curTheme.neutralBackground),
      )));
}
