import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// Solid tab bar indicator.
class SolidIndicator extends Decoration {
  @override
  BoxPainter createBoxPainter([VoidCallback onChanged]) {
    return _SolidIndicatorPainter(this, onChanged);
  }
}

class _SolidIndicatorPainter extends BoxPainter {
  final SolidIndicator decoration;

  _SolidIndicatorPainter(this.decoration, VoidCallback onChanged)
      : assert(decoration != null),
        super(onChanged);

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    assert(configuration != null);
    assert(configuration.size != null);

    //Rect rect = offset & configuration.size;
    final Paint paint = Paint();
    paint.color = Color(0xFF2356FF);
    paint.style = PaintingStyle.fill;
    canvas.drawCircle(
        Offset(offset.distance + configuration.size.width / 2,
            configuration.size.height / 2),
        28.0,
        paint);
  }
}
