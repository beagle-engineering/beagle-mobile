import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:getbeagle/models/screens/add_expenses_revenues.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:getbeagle/ui/components/form_button.dart';
import 'package:getbeagle/appstate_container.dart';

class IntroductionScreen extends StatefulWidget {
  static String route = '/intro_screen';

  final bool firstConnection;
  final String assetId;
  final bool hasRecurrentTransactions;

  IntroductionScreen(
      {Key key,
      this.firstConnection,
      this.assetId,
      this.hasRecurrentTransactions})
      : super(key: key);

  @override
  _IntroductionScreenState createState() => _IntroductionScreenState();
}

class _IntroductionScreenState extends State<IntroductionScreen> {
  _IntroductionScreenState();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  String _content =
      '''<p>Tes finances persos sont constitu&eacute;s de deux &eacute;l&eacute;ments :&nbsp;</p>
<ul>
<li>💰 <strong>Capital :</strong> c&rsquo;est ce qui est fixe, ce qui est sur tes comptes ou encore un bien immobilier,</li>
<li>🔄 <strong>Flux :</strong> c&rsquo;est les mouvements entre tes comptes comme par exemple un salaire en CDI, un versement r&eacute;gulier sur un livret A.&nbsp;</li>
</ul>
<br/>
</ul>
<p>Pour avoir un avant go&ucirc;t de mes capacit&eacute;s, je te propose pour commencer de me partager <strong>ton compte courant</strong>, <strong>ton revenu mensuel</strong> et <strong>tes d&eacute;penses mensuelles</strong>.&nbsp;</p>
<p>✅ Tu pourra mettre à jour ou supprimer ces informations depuis ton onglet Finances.</p>
''';

// <p>Dans l&rsquo;onglet finance, tu pourras :&nbsp;</p>
// <ul>
// <li>✅ Mettre &agrave; jour ou supprimer ces informations</li>
// <li>🆕 Ajouter tes autres placements pour simuler le futur de ton patrimoine</li>

  @override
  Widget build(BuildContext context) {
    // Use the Todo to create the UI.
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: StateContainer.of(context).curTheme.background,
        body: SafeArea(
            child: Scrollbar(
                child: SingleChildScrollView(
                    //controller: _controller,
                    child: Column(children: <Widget>[
          Container(
              alignment: Alignment.center,
              //padding: const EdgeInsets.only(top: 20),
              margin: const EdgeInsets.fromLTRB(24, 67, 24, 0),
              child: Text(
                'Tes finances persos',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: StateContainer.of(context).curTheme.title,
                    fontSize: 42,
                    height: 1.2),
                textAlign: TextAlign.center,
              )),
          Column(
            children: [
              Container(
                  alignment: Alignment.center,
                  //padding: const EdgeInsets.only(top: 20),
                  margin: EdgeInsets.fromLTRB(24, 24, 24, 0),
                  child: Html(
                    data: _content,
                    onLinkTap: (url) async {
                      if (await canLaunch(url)) {
                        await launch(url);
                      } else {
                        throw 'Could not launch ${url}';
                      }
                    },
                    style: {
                      "p": Style(
                        color: StateContainer.of(context).curTheme.title2,
                        fontSize: FontSize(16),
                        fontWeight: FontWeight.w500,
                        textAlign: TextAlign.left,
                        letterSpacing: 0.4,
                      ),
                      "ul": Style(
                        color: StateContainer.of(context).curTheme.title2,
                        fontSize: FontSize(16),
                        fontWeight: FontWeight.w500,
                        textAlign: TextAlign.left,
                        letterSpacing: 0.4,
                      ),
                      "li": Style(
                          color: StateContainer.of(context).curTheme.title2,
                          fontSize: FontSize(16),
                          fontWeight: FontWeight.w500,
                          textAlign: TextAlign.left,
                          letterSpacing: 0.4,
                          padding: const EdgeInsets.only(bottom: 10)),
                      "a": Style(
                        color: StateContainer.of(context).curTheme.primary,
                        fontSize: FontSize(16),
                        fontWeight: FontWeight.w500,
                        textDecoration: TextDecoration.underline,
                        textAlign: TextAlign.left,
                        letterSpacing: 0.4,
                      ),
                      "strong": Style(
                        color: StateContainer.of(context).curTheme.title2,
                        fontSize: FontSize(16),
                        fontWeight: FontWeight.bold,
                        textAlign: TextAlign.left,
                        letterSpacing: 0.4,
                      )
                    },
                  )),
              SizedBox(height: 30.0),
              FormButton.primaryDefault(
                  context: context,
                  heroTag: 'go_introduction',
                  text: 'En Avant !',
                  onPressed: () {
                    print(widget.assetId);
                    if (widget.assetId == null) {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          '/add_current_account',
                          (Route<dynamic> route) => false,
                          arguments: widget.firstConnection);
                    } else if (!widget.hasRecurrentTransactions) {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          '/add_expenses_revenues',
                          (Route<dynamic> route) => false,
                          arguments: AddExpensesRevenuesArgs(
                              assetId: widget.assetId,
                              firstConnection: widget.firstConnection));
                    } else if (widget.firstConnection) {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          '/choose_interests', (Route<dynamic> route) => false,
                          arguments: widget.firstConnection);
                    } else {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          '/home', (Route<dynamic> route) => false);
                    }
                  }),
              SizedBox(height: 30.0),
            ],
          ),
        ])))));
  }
}
