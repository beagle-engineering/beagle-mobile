import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/models/api/user.dart';
import 'package:getbeagle/models/screens/intro.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/asset.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/services/api/recurrent_transactions.dart';
import 'package:getbeagle/services/api/user.dart';
import 'package:getbeagle/localization.dart';
import 'package:getbeagle/services/firebase/analytics_service.dart';
import 'package:getbeagle/ui/components/form_button.dart';
import 'package:getbeagle/ui/components/form_text_field.dart';
import 'package:getbeagle/ui/components/icon_box.dart';
import 'package:getbeagle/utils/sharedprefsutil.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginScreen extends StatefulWidget {
  static final String id = 'login_screen';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  String _email, _password;

  ScrollController _controller;
  double _bottomPadding = 200;
  @override
  void initState() {
    _controller = ScrollController();
    super.initState();

    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          if (visible) {
            _controller.animateTo(_controller.position.maxScrollExtent,
                curve: Curves.linear, duration: Duration(milliseconds: 500));
          } else {
            _controller.animateTo(_controller.position.minScrollExtent,
                curve: Curves.linear, duration: Duration(milliseconds: 500));
          }
        });
      },
    );
  }

  _loginSucess() async {
    await sl.get<AnalyticsService>().logLogin();

    await getCurrentUser(api: sl.get<ApiClient>()).then((User user) async {
      await setUserPrefs(user);
      bool firstLaunch =
          (await sl.get<SharedPrefsUtil>().getUserFirstLaunch(user.email));
      String mainAsset = await getMainCurrentAccount(sl.get<ApiClient>());
      bool hasRecTransactions;
      if (mainAsset != null) {
        hasRecTransactions = await hasOnBoardingRecurrentTransaction(
            sl.get<ApiClient>(), mainAsset);
      }
      print('first launch from login ${firstLaunch}');
      if (firstLaunch || mainAsset == null || !hasRecTransactions) {
        await sl.get<SharedPrefsUtil>().setUserFirstLaunch(user.email);
        await await Navigator.of(context).pushReplacementNamed('/intro_screen',
            arguments: IntroArgs(
                assetId: mainAsset,
                firstConnection: firstLaunch,
                hasRecurrentTransactions: hasRecTransactions));
      } else {
        // if (isPremium(user.plan)) {
        //   await Navigator.of(context)
        //       .pushReplacementNamed('/home', arguments: true);
        // } else {
        //   await Navigator.of(context)
        //       .pushReplacementNamed('/free_home', arguments: true);
        // }
        await Navigator.of(context)
            .pushReplacementNamed('/home', arguments: true);
      }
    }).catchError((Object e) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
          backgroundColor: StateContainer.of(context).curTheme.failure,
          content: Text(
            'Un problème est survenu, veuillez réessayer plus tard.',
            style: TextStyle(
                color: StateContainer.of(context).curTheme.neutralBackground),
          )));
    });
  }

  _loginFailure() {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: StateContainer.of(context).curTheme.failure,
        content: Text(
          'Tes identifiants sont incorrectes.',
          style: TextStyle(
              color: StateContainer.of(context).curTheme.neutralBackground),
        )));
  }

  _submit() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }

    AppLocalization.load(Locale("fr", "FR"));

    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      login(sl.get<ApiClient>(), _email, _password).then((e) async {
        _loginSucess();
      }).catchError((Object o) {
        _loginFailure();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: StateContainer.of(context).curTheme.background,
          body: SafeArea(
              child: Scrollbar(
                  child: SingleChildScrollView(
                      controller: _controller,
                      child: Column(
                        children: <Widget>[
                          Container(
                              alignment: Alignment.centerLeft,
                              margin: const EdgeInsets.only(left: 32, top: 100),
                              child: SvgPicture.asset(
                                  'assets/images/logo_blue.svg',
                                  width: 48,
                                  height: 48)),
                          Container(
                              alignment: Alignment.centerLeft,
                              margin: const EdgeInsets.only(left: 32, top: 24),
                              child: Text('Bienvenue!',
                                  style: TextStyle(
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .title,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 42,
                                      height: 1.2))),
                          Form(
                            key: _formKey,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 0, top: 37.0),
                                  child: Row(children: [
                                    Padding(
                                        padding: const EdgeInsets.only(top: 20),
                                        child: IconBox(
                                          icon: FeatherIcons.user,
                                          iconColor: StateContainer.of(context)
                                              .curTheme
                                              .neutralBackground,
                                          boxColor: StateContainer.of(context)
                                              .curTheme
                                              .classicOrange,
                                          width: 35,
                                          height: 45,
                                        )),
                                    Container(
                                        width: 230,
                                        margin: const EdgeInsets.only(left: 20),
                                        child: StyledFocusForm2(
                                          validator: (input) => input
                                                      .isNotEmpty &&
                                                  !input.contains('@')
                                              ? 'Veuillez entrer une adresse mail valide'
                                              : null,
                                          onSaved: (input) =>
                                              _email = input.trim(),
                                          label: 'Email',
                                          obscureText: false,
                                        )),
                                  ]),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 0, top: 30.0),
                                  child: Row(children: [
                                    Padding(
                                        padding: const EdgeInsets.only(top: 20),
                                        child: IconBox(
                                          icon: FeatherIcons.lock,
                                          iconColor: StateContainer.of(context)
                                              .curTheme
                                              .neutralBackground,
                                          boxColor: StateContainer.of(context)
                                              .curTheme
                                              .classicPink,
                                          width: 35,
                                          height: 45,
                                        )),
                                    Container(
                                        width: 230,
                                        margin: const EdgeInsets.only(left: 20),
                                        child: StyledFocusForm2(
                                          validator: (input) => input
                                                  .isEmpty // TODO for test purposes
                                              ? 'Mot de passe requis'
                                              : null,
                                          onSaved: (input) => _password = input,
                                          label: 'Mot de passe',
                                          obscureText: true,
                                        ))
                                  ]),
                                ),
                                SizedBox(height: 64.0),
                                FormButton.primaryDefault(
                                    context: context,
                                    heroTag: 'connection',
                                    text: 'Se connecter',
                                    onPressed: _submit),
                                SizedBox(height: 18.0),
                                FlatButton(
                                  child: Text(
                                    'Mot de passe oublié?',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 14,
                                        height: 1.7,
                                        color: StateContainer.of(context)
                                            .curTheme
                                            .title2),
                                  ),
                                  onPressed: () async {
                                    final Uri params = Uri(
                                        scheme: 'mailto',
                                        path: 'contact@getbeagle.app',
                                        query:
                                            'subject=J\'ai oublié mon mot de passe beagle');
                                    String url = params.toString();
                                    //final url =
                                    //    'mailto:contact@getbeagle.app?subject=Feedbacks%20${platform}%20app';
                                    //final url = 'http://google.com';
                                    if (await canLaunch(url)) {
                                      await launch(url);
                                    } else {
                                      throw 'Could not launch $url';
                                    }
                                  },
                                ),
                                SizedBox(height: 48.0),
                                FormButton.whiteDefault(
                                    context: context,
                                    heroTag: 'signup',
                                    text: 'Créer un compte',
                                    onPressed: () => Navigator.of(context)
                                        .pushNamedAndRemoveUntil('/signup',
                                            (Route<dynamic> route) => false)),
                                SizedBox(height: _bottomPadding),
                              ],
                            ),
                          ),
                        ],
                      )))),
        ));
  }
}
