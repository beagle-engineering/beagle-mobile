import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/models/api/asset.dart';
import 'package:getbeagle/models/api/project.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/asset.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/services/api/project.dart';
import 'package:getbeagle/ui/components/form_button.dart';
import 'package:getbeagle/ui/components/form_text_field.dart';
import 'package:getbeagle/ui/components/multiselectDialog.dart';
import 'package:intl/intl.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';

class CreateProjectScreen extends StatefulWidget {
  CreateProjectScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _CreateProjectScreenState createState() => _CreateProjectScreenState();
}

class _CreateProjectScreenState extends State<CreateProjectScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  String _name;
  DateTime _endedAt;
  String _description;
  String _amount;
  String _label_id;
  String _current;
  String _aggDescription = '';
  Future<List<ProjectLabel>> labels;
  Future<List<AssetType>> types;

  final format = DateFormat.yMMMMd('fr_FR');

  ScrollController _controller;

  @override
  void initState() {
    _controller = ScrollController();
    super.initState();
    labels = getProjectLabels(sl.get<ApiClient>());
    types = getAssetTypes(sl.get<ApiClient>());

    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          if (visible) {
            _controller.animateTo(_controller.position.maxScrollExtent,
                curve: Curves.linear, duration: Duration(milliseconds: 500));
          } else {
            _controller.animateTo(_controller.position.minScrollExtent,
                curve: Curves.linear, duration: Duration(milliseconds: 500));
          }
        });
      },
    );
  }

  void _onError(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: StateContainer.of(context).curTheme.failure,
        content: Text(
          message,
          style: TextStyle(
              color: StateContainer.of(context).curTheme.neutralBackground),
        )));
  }

  _create() {
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _amount = _amount.replaceAll(",", ".");
      _current = _current.replaceAll(",", ".");
      Project p = Project(
          name: _name,
          label: ProjectLabel(id: _label_id),
          endedAt: _endedAt,
          description: _aggDescription,
          goal: (double.parse(_amount) * 100).round(),
          current: (double.parse(_current) * 100).round(),
          startedAt: DateTime.now(),
          active: false);
      createProject(sl.get<ApiClient>(), p)
          .then((e) => Navigator.of(context).pushNamedAndRemoveUntil(
              '/home', (Route<dynamic> route) => false))
          .catchError((Object e) {
        _onError('Une erreur s\'est produite. Veuillez réessayer plus tard.');
      });
    } else {
      _onError('Veuillez ne laisser aucun champs vide');
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: StateContainer.of(context).curTheme.background,
          appBar: PreferredSize(
              preferredSize: Size.fromHeight(67.0),
              child: AppBar(
                  backgroundColor: Colors.transparent,
                  elevation: 0.0,
                  automaticallyImplyLeading: false,
                  title: InkWell(
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      child: Container(
                          margin: const EdgeInsets.only(
                              top: 50, right: 20, bottom: 20),
                          child: Row(children: [
                            Icon(FeatherIcons.chevronLeft,
                                color:
                                    StateContainer.of(context).curTheme.title2,
                                size: 19),
                            Container(
                                padding:
                                    const EdgeInsets.only(left: 12, top: 4),
                                alignment: Alignment.bottomLeft,
                                child: Text('Retour',
                                    style: TextStyle(
                                        fontSize: 14,
                                        height: 1.18,
                                        color: StateContainer.of(context)
                                            .curTheme
                                            .title2))),
                          ])),
                      onTap: () => Navigator.of(context).pop()))),
          body: SafeArea(
              child: SingleChildScrollView(
                  controller: _controller,
                  child: Column(children: <Widget>[
                    Container(
                        alignment: Alignment.center,
                        //padding: const EdgeInsets.only(top: 20),
                        margin: const EdgeInsets.fromLTRB(20, 10, 20, 0),
                        child: Text(
                          "Crée ton projet",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: StateContainer.of(context).curTheme.title,
                              fontSize: 30),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        height: 480,
                        padding:
                            const EdgeInsets.only(left: 24, right: 24, top: 24),
                        child: Form(
                            key: _formKey,
                            autovalidate: false,
                            child: ListView(
                              physics: NeverScrollableScrollPhysics(),
                              padding: const EdgeInsets.only(bottom: 5.0),
                              children: <Widget>[
                                StyledFocusForm2(
                                  validator: (input) => input.isEmpty
                                      ? 'Veuillez spécifier un nom pour votre projet'
                                      : null,
                                  onSaved: (input) => _name = input,
                                  label: "Nom",
                                  obscureText: false,
                                ),
                                SizedBox(
                                  height: 16,
                                ),
                                StyledFocusForm2(
                                  keyboardType: TextInputType.number,
                                  validator: (input) => input.isEmpty
                                      ? 'Veuillez spécifier votre objectif'
                                      : null,
                                  onSaved: (input) => _amount = input,
                                  label: "Montant visé",
                                  obscureText: false,
                                  inputFormatters: [
                                    //WhitelistingTextInputFormatter.digitsOnly,
                                    WhitelistingTextInputFormatter(RegExp(
                                        '^\$|^(0|([1-9][0-9]{0,}))((\\.|\\,)[0-9]{0,})?\$'))
                                  ],
                                ),
                                SizedBox(
                                  height: 16,
                                ),
                                StyledFocusForm2(
                                  keyboardType: TextInputType.number,
                                  validator: (input) => input.isEmpty
                                      ? 'Veuillez spécifier votre montant détenu'
                                      : null,
                                  onSaved: (input) => _current = input,
                                  label:
                                      "Montant détenu actuellement pour ce projet",
                                  obscureText: false,
                                  inputFormatters: [
                                    //WhitelistingTextInputFormatter.digitsOnly,
                                    WhitelistingTextInputFormatter(RegExp(
                                        '^\$|^(0|([1-9][0-9]{0,}))((\\.|\\,)[0-9]{0,})?\$'))
                                  ],
                                ),
                                SizedBox(
                                  height: 16,
                                ),
                                DateTimeField(
                                  format: format,
                                  decoration: const InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xFF9EA6BF)),
                                      ),
                                      //icon: Icon(FeatherIcons.crosshair),
                                      labelText: 'Date de fin visée',
                                      labelStyle: TextStyle(
                                        color: Color(0xFF9EA6BF),
                                        fontSize: 14,
                                      )
                                      //fontSize: 15),
                                      ),
                                  onShowPicker: (context, currentValue) {
                                    return showDatePicker(
                                      context: context,
                                      firstDate: DateTime.now(),
                                      initialDate:
                                          currentValue ?? DateTime.now(),
                                      lastDate: DateTime(2100),
                                      builder: (context, child) =>
                                          Localizations.override(
                                        context: context,
                                        locale: Locale('fr'),
                                        child: child,
                                      ),
                                    );
                                  },
                                  validator: (input) => input == null ||
                                          input.toIso8601String().isEmpty
                                      ? 'Veuillez ajouter une date de fin à votre objectif'
                                      : null,
                                  onSaved: (input) => _endedAt = input,
                                ),
                                SizedBox(
                                  height: 16,
                                ),
                                FutureBuilder<List<ProjectLabel>>(
                                    future: labels,
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData) {
                                        return FormField(
                                          validator: (input) => input == null ||
                                                  input.toString().isEmpty
                                              ? 'Veuillez choisir une catégorie de projet'
                                              : null,
                                          builder: (FormFieldState state) {
                                            return InputDecorator(
                                              decoration: InputDecoration(
                                                labelText: "Catégorie",
                                                labelStyle: TextStyle(
                                                    color: StateContainer.of(
                                                            context)
                                                        .curTheme
                                                        .title2),
                                                focusedBorder:
                                                    UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: StateContainer.of(
                                                              context)
                                                          .curTheme
                                                          .primary),
                                                ),
                                                enabledBorder:
                                                    UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: StateContainer.of(
                                                              context)
                                                          .curTheme
                                                          .title2),
                                                ),
                                              ),
                                              isEmpty: _label_id == '',
                                              child:
                                                  DropdownButtonHideUnderline(
                                                child: DropdownButton(
                                                  value: _label_id,
                                                  isDense: true,
                                                  onChanged: (String newValue) {
                                                    setState(() {
                                                      //newContact.favoriteColor = newValue;
                                                      _label_id = newValue;
                                                      state.didChange(newValue);
                                                    });
                                                  },
                                                  items: snapshot.data.map(
                                                      (ProjectLabel value) {
                                                    return DropdownMenuItem(
                                                      value: value.id,
                                                      child: Text(value.name),
                                                    );
                                                  }).toList(),
                                                ),
                                              ),
                                            );
                                          },
                                        );
                                      } else if (snapshot.hasError) {
                                        return Text("${snapshot.error}");
                                      }
                                      return CircularProgressIndicator();
                                    }),
                                SizedBox(height: 16),
                                FutureBuilder<List<AssetType>>(
                                    future: types,
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData) {
                                        List<MultiSelectDialogItem> items =
                                            List<MultiSelectDialogItem>();
                                        for (final t in snapshot.data) {
                                          if (t.name == 'Autre') {
                                            continue;
                                          }
                                          items.add(MultiSelectDialogItem(
                                              t.name, t.name));
                                        }
                                        Set initialValues = Set();
                                        return TextField(
                                            onTap: () async {
                                              final selectedValues =
                                                  await showMultiSelect(
                                                      context: context,
                                                      items: items,
                                                      title: 'Placements',
                                                      initialValues:
                                                          initialValues);
                                              setState(() {
                                                if (selectedValues != null) {
                                                  _aggDescription =
                                                      selectedValues.join(',');
                                                  initialValues.clear();
                                                  initialValues
                                                      .addAll(selectedValues);
                                                  print(_aggDescription);
                                                }
                                              });
                                            },
                                            decoration: InputDecoration(
                                              labelText:
                                                  "Placements pour ce projet",
                                              labelStyle: TextStyle(
                                                  color:
                                                      StateContainer.of(context)
                                                          .curTheme
                                                          .title2),
                                              hintText: _aggDescription,
                                              hintStyle: TextStyle(
                                                  color:
                                                      StateContainer.of(context)
                                                          .curTheme
                                                          .title),
                                              focusedBorder:
                                                  UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: StateContainer.of(
                                                            context)
                                                        .curTheme
                                                        .primary),
                                              ),
                                              enabledBorder:
                                                  UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: StateContainer.of(
                                                            context)
                                                        .curTheme
                                                        .title2),
                                              ),
                                            ));
                                        // return FormField(
                                        //   builder: (FormFieldState state) {
                                        //     return InputDecorator(
                                        //       decoration: InputDecoration(
                                        //         labelText: "Type de placement",
                                        //         labelStyle: TextStyle(
                                        //             color: StateContainer.of(
                                        //                     context)
                                        //                 .curTheme
                                        //                 .title2),
                                        //         focusedBorder:
                                        //             UnderlineInputBorder(
                                        //           borderSide: BorderSide(
                                        //               color: StateContainer.of(
                                        //                       context)
                                        //                   .curTheme
                                        //                   .primary),
                                        //         ),
                                        //         enabledBorder:
                                        //             UnderlineInputBorder(
                                        //           borderSide: BorderSide(
                                        //               color: StateContainer.of(
                                        //                       context)
                                        //                   .curTheme
                                        //                   .title2),
                                        //         ),
                                        //       ),
                                        //       isEmpty: _description == '',
                                        //       child:
                                        //           DropdownButtonHideUnderline(
                                        //         child: DropdownButton(
                                        //           value: _description,
                                        //           isDense: true,
                                        //           onChanged: (String newValue) {
                                        //             setState(() {
                                        //               //newContact.favoriteColor = newValue;

                                        //               _description = newValue;
                                        //               _aggDescription +=
                                        //                   _description;
                                        //               state.didChange(newValue);
                                        //             });
                                        //           },
                                        //           items: snapshot.data
                                        //               .map((AssetType value) {
                                        //             return DropdownMenuItem(
                                        //               value: value.name,
                                        //               child: Text(value.name),
                                        //             );
                                        //           }).toList(),
                                        //         ),
                                        //       ),
                                        //     );
                                        //   },
                                        // );
                                      } else if (snapshot.hasError) {
                                        return Text("${snapshot.error}");
                                      }
                                      return CircularProgressIndicator();
                                    }),
                              ],
                            ))),
                    SizedBox(
                      height: 32,
                    ),
                    Container(
                      margin: const EdgeInsets.only(bottom: 150),
                      child: FormButton.primaryDefault(
                        context: context,
                        heroTag: 'create_project',
                        text: 'Créer',
                        onPressed: _create,
                      ),
                    )
                  ]))),
        ));
  }
}
