import 'package:flutter/material.dart';

import 'package:feather_icons_flutter/feather_icons_flutter.dart';

import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/models/api/user.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/services/api/user.dart';
import 'package:getbeagle/services/firebase/dynamic_links_service.dart';
import 'package:getbeagle/services/firebase/notification_service.dart';
import 'package:getbeagle/ui/screens/choose_interest_screen.dart';

import 'package:getbeagle/ui/screens/tabs/knowledge.dart';
import 'package:getbeagle/utils/users.dart';

class FreeHomeScreen extends StatefulWidget {
  static final String id = 'free_home_screen';

  final bool onLaunch;

  const FreeHomeScreen({Key key, this.onLaunch = false}) : super(key: key);

  @override
  _FreeHomeScreenState createState() => _FreeHomeScreenState();
}

class _FreeHomeScreenState extends State<FreeHomeScreen> {
  @override
  void initState() {
    super.initState();
    if (widget.onLaunch) {
      handleOnLaunch();
    }
  }

  void handleOnLaunch() async {
    await getCurrentUser(api: sl.get<ApiClient>()).then((User user) async {
      await setUserPrefs(user);
      // if logged in, check if user's first launch.
      // init firebase components
      await sl.get<DynamicLinkService>().handleDynamicLinks();
      await sl.get<PushNotificationService>().initialise();
      await sl.get<PushNotificationService>().subscribe('all');
      if (isPremium(user.plan)) {
        sl.get<PushNotificationService>().subscribe('premium');
      } else {
        sl.get<PushNotificationService>().subscribe('free');
      }

      if (user.email == 'freeUser@getbeagle.app' ||
          user.email == 'test_user_api@getbeagle.app') {
        sl.get<PushNotificationService>().subscribe('testers');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: StateContainer.of(context).curTheme.background,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(90.0),
            child: Container(
                margin: const EdgeInsets.fromLTRB(15, 40, 0, 20),
                child: AppBar(
                    title: Container(
                        alignment: Alignment.bottomLeft,
                        child: Text(
                          'Home',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: StateContainer.of(context).curTheme.title,
                              fontSize: 30),
                          textAlign: TextAlign.left,
                        )),
                    backgroundColor: Colors.transparent,
                    elevation: 0.0,
                    actions: <Widget>[
                      GestureDetector(
                          onTap: () =>
                              Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => ChooseInterestScreen(),
                              )),
                          child: Padding(
                            padding: EdgeInsets.only(right: 24.0),
                            child: Icon(FeatherIcons.edit,
                                color:
                                    StateContainer.of(context).curTheme.title),
                          )),
                      GestureDetector(
                          onTap: () =>
                              Navigator.of(context).pushNamed('/settings'),
                          child: Padding(
                            padding: EdgeInsets.only(right: 24.0),
                            child: Icon(FeatherIcons.settings,
                                color:
                                    StateContainer.of(context).curTheme.title),
                          )),
                    ]))),
        body: KnowledgeTab());
  }
}
