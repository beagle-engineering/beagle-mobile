import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/models/api/asset.dart';
import 'package:getbeagle/models/screens/add_expenses_revenues.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/asset.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/ui/components/form_button.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:url_launcher/url_launcher.dart';

class AddCurrentAccountScreen extends StatefulWidget {
  static const route = '/add_current_account';
  AddCurrentAccountScreen({Key key, this.firstConnection}) : super(key: key);

  final bool firstConnection;

  @override
  _AddCurrentAccountScreenState createState() =>
      _AddCurrentAccountScreenState();
}

class _AddCurrentAccountScreenState extends State<AddCurrentAccountScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  FocusNode _focusNode;

  String _name = 'Autre';
  String _balance;
  String _providerId;
  String _otherName = '';
  Future<List<AssetType>> types;
  Future<List<AssetProvider>> providers;

  ScrollController _controller;

  @override
  void initState() {
    _controller = ScrollController();
    super.initState();
    providers = getAssetProviders(sl.get<ApiClient>());
    // types = getAssetTypes(sl.get<ApiClient>());
    _focusNode = FocusNode();

    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          if (visible) {
            _controller.animateTo(_controller.position.maxScrollExtent,
                curve: Curves.linear, duration: Duration(milliseconds: 500));
          } else {
            _controller.animateTo(_controller.position.minScrollExtent,
                curve: Curves.linear, duration: Duration(milliseconds: 500));
          }
        });
      },
    );
  }

  void _onError(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: StateContainer.of(context).curTheme.failure,
        content: Text(
          message,
          style: TextStyle(
              color: StateContainer.of(context).curTheme.neutralBackground),
        )));
  }

  _create() {
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _balance = _balance.replaceAll(",", ".");
      Asset a = Asset(
          name: 'Compte courant',
          type: AssetType(id: '46242805-5742-4c00-846f-dc6882db6c52'),
          provider: AssetProvider(id: _providerId),
          balance: (double.parse(_balance) * 100).round());
      createAsset(sl.get<ApiClient>(), a)
          .then((e) => Navigator.of(context).pushNamedAndRemoveUntil(
              '/add_expenses_revenues', (Route<dynamic> route) => false,
              arguments: AddExpensesRevenuesArgs(
                  assetId: e, firstConnection: widget.firstConnection)))
          .catchError((Object e) {
        _onError(
            'Une erreur s\'est produite. Veuillez réessayer plus tard. ${e.toString()}');
      });
    } else {
      _onError('Veuillez ne laisser aucun champs vide');
    }
  }

  String introText = '''
  <p>🥅 Mon but: que tu deviennes le maître de tes finances persos. Et ça commence par être le maître de ses données !</p>
  </br>
  <p>Je suis régulé par l’Autorité des Marchés Financiers (#AMF), je suis enregistré en tant que Conseiller en Investissement Financier (#CIF).</p>
<p>Avec moi, tu peux ajouter, supprimer ou modifier tes données à tout moment et en toute sécurité.</p>
</br>
<p>💻 Une offre premium en cours de développement te permet de disposer de fonctionnalités avancées en toute sérénité.</p>
  ''';

  void _requestFocus() {
    setState(() {
      FocusScope.of(context).requestFocus(_focusNode);
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: StateContainer.of(context).curTheme.background,
          body: SafeArea(
              child: SingleChildScrollView(
                  controller: _controller,
                  child: Column(children: <Widget>[
                    Container(
                        alignment: Alignment.center,
                        //padding: const EdgeInsets.only(top: 20),
                        margin: const EdgeInsets.fromLTRB(24, 62, 24, 0),
                        child: Text(
                          "Ta première donnée",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: StateContainer.of(context).curTheme.title,
                              fontSize: 42,
                              height: 1.2),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        alignment: Alignment.center,
                        //padding: const EdgeInsets.only(top: 20),
                        margin: EdgeInsets.fromLTRB(24, 24, 24, 0),
                        child: Html(
                          data: introText,
                          onLinkTap: (url) async {
                            if (await canLaunch(url)) {
                              await launch(url);
                            } else {
                              throw 'Could not launch ${url}';
                            }
                          },
                          style: {
                            "p": Style(
                              color: StateContainer.of(context).curTheme.title2,
                              fontSize: FontSize(16),
                              fontWeight: FontWeight.w500,
                              textAlign: TextAlign.left,
                              letterSpacing: 0.4,
                              //height: 200,
                            ),
                            "ul": Style(
                              color: StateContainer.of(context).curTheme.title2,
                              fontSize: FontSize(16),
                              fontWeight: FontWeight.w500,
                              textAlign: TextAlign.left,
                              letterSpacing: 0.4,
                            ),
                            "li": Style(
                                color:
                                    StateContainer.of(context).curTheme.title2,
                                fontSize: FontSize(16),
                                fontWeight: FontWeight.w500,
                                textAlign: TextAlign.left,
                                letterSpacing: 0.4,
                                padding: const EdgeInsets.only(bottom: 10)),
                            "a": Style(
                              color:
                                  StateContainer.of(context).curTheme.primary,
                              fontSize: FontSize(16),
                              fontWeight: FontWeight.w500,
                              textDecoration: TextDecoration.underline,
                              textAlign: TextAlign.left,
                              letterSpacing: 0.4,
                            ),
                            "strong": Style(
                              color: StateContainer.of(context).curTheme.title2,
                              fontSize: FontSize(16),
                              fontWeight: FontWeight.bold,
                              textAlign: TextAlign.left,
                              letterSpacing: 0.4,
                            )
                          },
                        )),
                    Container(
                        height: 210,
                        padding:
                            const EdgeInsets.only(left: 24, right: 24, top: 16),
                        child: Form(
                            key: _formKey,
                            autovalidate: true,
                            child: ListView(
                              physics: NeverScrollableScrollPhysics(),
                              padding: const EdgeInsets.only(bottom: 0.0),
                              children: <Widget>[
                                Text(
                                    '💰 Montant sur ton compte courant principal',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: _focusNode.hasFocus
                                          ? StateContainer.of(context)
                                              .curTheme
                                              .primary
                                          : StateContainer.of(context)
                                              .curTheme
                                              .title2,
                                    )),
                                TextFormField(
                                  focusNode: _focusNode,
                                  onTap: _requestFocus,
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: StateContainer.of(context)
                                        .curTheme
                                        .title,
                                  ),
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    isDense: true,
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: StateContainer.of(context)
                                              .curTheme
                                              .primary),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: StateContainer.of(context)
                                              .curTheme
                                              .title2),
                                    ),
                                  ),
                                  validator: (input) => null,
                                  onSaved: (input) => input.isNotEmpty
                                      ? _balance = input
                                      : _balance = '0',
                                  obscureText: false,
                                  inputFormatters: [
                                    //WhitelistingTextInputFormatter.digitsOnly,
                                    WhitelistingTextInputFormatter(RegExp(
                                        '^\$|^(0|([1-9][0-9]{0,}))((\\.|\\,)[0-9]{0,})?\$'))
                                  ],
                                ),
                                SizedBox(
                                  height: 24,
                                ),
                                Text(
                                    '🏦 Établissement de ton compte courant principal',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .title2,
                                    )),
                                FutureBuilder<List<AssetProvider>>(
                                    future: providers,
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData) {
                                        return FormField(
                                          builder: (FormFieldState state) {
                                            return InputDecorator(
                                              decoration: InputDecoration(
                                                isDense: true,
                                                focusedBorder:
                                                    UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: StateContainer.of(
                                                              context)
                                                          .curTheme
                                                          .primary),
                                                ),
                                                enabledBorder:
                                                    UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: StateContainer.of(
                                                              context)
                                                          .curTheme
                                                          .title2),
                                                ),
                                              ),
                                              isEmpty: _providerId == '',
                                              child:
                                                  DropdownButtonHideUnderline(
                                                child: DropdownButton(
                                                  value: _providerId,
                                                  isDense: true,
                                                  onChanged: (String newValue) {
                                                    setState(() {
                                                      _focusNode.unfocus();
                                                      //newContact.favoriteColor = newValue;
                                                      _providerId = newValue;
                                                      state.didChange(newValue);
                                                    });
                                                  },
                                                  items: snapshot.data.map(
                                                      (AssetProvider value) {
                                                    return DropdownMenuItem(
                                                      value: value.id,
                                                      child: Text(value.name,
                                                          style: TextStyle(
                                                            fontSize: 14,
                                                            color: StateContainer
                                                                    .of(context)
                                                                .curTheme
                                                                .title,
                                                          )),
                                                    );
                                                  }).toList(),
                                                ),
                                              ),
                                            );
                                          },
                                        );
                                      } else if (snapshot.hasError) {
                                        return Text("${snapshot.error}");
                                      }
                                      return CircularProgressIndicator();
                                    }),
                              ],
                            ))),
                    Container(
                      margin: const EdgeInsets.only(bottom: 150),
                      child: FormButton.primaryDefault(
                        context: context,
                        heroTag: 'add_current_account',
                        text: 'Ajouter',
                        onPressed: _create,
                      ),
                    ),
                  ]))),
        ));
  }
}
