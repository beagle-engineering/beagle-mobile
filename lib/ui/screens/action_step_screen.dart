import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/models/api/action.dart';
import 'package:getbeagle/models/api/knowledge.dart';
import 'package:getbeagle/models/screens/knowledge_content.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/action.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/services/api/knowledge.dart';
import 'package:getbeagle/ui/components/form_button.dart';
import 'package:getbeagle/ui/screens/knowledge_content.dart';
import 'package:url_launcher/url_launcher.dart';

class ActionStepScreen extends StatefulWidget {
  // Declare a field that holds the Todo.
  final String actionId;

  // In the constructor, require a Todo.
  ActionStepScreen({Key key, @required this.actionId}) : super(key: key);

  @override
  _ActionStepScreenState createState() => _ActionStepScreenState();
}

class _ActionStepScreenState extends State<ActionStepScreen> {
  _ActionStepScreenState();
  var _controller = ScrollController();
  String _appBarTitle = '';
  Future<RecommendedActionStep> currentStep;

  @override
  void initState() {
    super.initState();
    currentStep = getcurrentActionStep(sl.get<ApiClient>(), widget.actionId);
  }

  List<Widget> getButtons(RecommendedActionStep currentStep) {
    List<ActionStepChoice> choices = currentStep.choices;
    choices.sort((a, b) => a.getChoiceWeight().compareTo(b.getChoiceWeight()));

    Color buttonColor;

    List<Widget> buttonRows = <Widget>[];
    for (int i = 0; i < choices.length; i++) {
      if (choices[i].finalizing && choices[i].isNextStep()) {
        buttonColor = StateContainer.of(context).curTheme.success;
      } else if (!choices[i].isNextStep()) {
        buttonColor = StateContainer.of(context).curTheme.primary;
      } else {
        buttonColor = StateContainer.of(context).curTheme.warning;
      }

      buttonRows.add(Container(
        margin: EdgeInsets.only(bottom: 24),
        child: FormButton(
          heroTag: choices[i].label,
          backgroundColor: buttonColor,
          textColor: StateContainer.of(context).curTheme.neutralBackground,
          text: choices[i].label,
          leftMargin: 30,
          rightMargin: 30,
          height: 58,
          borderRadius: 12,
          onPressed: () async {
            await visitChoice(
                sl.get<ApiClient>(), currentStep.actionId, choices[i].id);
            if (choices[i].isNextStep()) {
              if (choices[i].value == null) {
                await Navigator.of(context).pushNamedAndRemoveUntil(
                    '/home', (Route<dynamic> route) => false);
              } else {
                await Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ActionStepScreen(
                          actionId: currentStep.actionId,
                        )));
              }
            } else if (choices[i].isKnowledgeContent()) {
              KnowledgeContent content =
                  await getContentById(sl.get<ApiClient>(), choices[i].value);
              if (choices[i].finalizing) {
                await Navigator.of(context).pushNamedAndRemoveUntil(
                  '/knowledge_content',
                  (Route<dynamic> route) => false,
                  arguments: KnowledgeContentArgs(
                      fullContent: content,
                      nextSteps: List<KnowledgeContent>()),
                );
              } else {
                await Navigator.of(context).pushNamed('/knowledge_content',
                    arguments: KnowledgeContentArgs(
                        fullContent: content,
                        nextSteps: List<KnowledgeContent>(),
                        isStarred: false));
              }
            } else if (choices[i].isExternalUrl()) {
              if (await canLaunch(choices[i].value)) {
                await launch(choices[i].value);
                await Navigator.of(context).pushNamedAndRemoveUntil(
                    '/home', (Route<dynamic> route) => false);
              } else {
                throw 'Could not launch ${choices[i].value}';
              }
            }
          },
        ),
      ));
    }

    return buttonRows;
  }

  int _index = 0;
  @override
  Widget build(BuildContext context) {
    // Use the Todo to create the UI.
    return Scaffold(
        backgroundColor: StateContainer.of(context).curTheme.background,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(67.0),
            child: AppBar(
                title: Text(
                  _appBarTitle,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: StateContainer.of(context).curTheme.title,
                      fontSize: 10),
                ),
                backgroundColor: Colors.transparent,
                elevation: 0.0,
                automaticallyImplyLeading: true,
                leading: IconButton(
                  padding: const EdgeInsets.only(top: 30, left: 10),
                  icon: Icon(FeatherIcons.arrowLeft),
                  color: StateContainer.of(context).curTheme.title,
                  onPressed: () => Navigator.of(context).pop(),
                  tooltip: 'retour',
                ))),
        body: Scrollbar(
          controller: _controller,
          child: SingleChildScrollView(
            child: FutureBuilder<RecommendedActionStep>(
                future: currentStep,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Column(
                      children: <Widget>[
                        Container(
                            alignment: Alignment.centerLeft,
                            //padding: const EdgeInsets.only(top: 20),
                            margin: const EdgeInsets.fromLTRB(24, 24, 24, 0),
                            child: Text(
                              snapshot.data.name,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color:
                                      StateContainer.of(context).curTheme.title,
                                  fontSize: 36),
                              textAlign: TextAlign.left,
                            )),
                        Container(
                            alignment: Alignment.center,
                            //padding: const EdgeInsets.only(top: 20),
                            margin: const EdgeInsets.fromLTRB(24, 24, 24, 0),
                            child: Html(
                              data: snapshot.data.content,
                              style: {
                                "p": Style(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .title2,
                                  fontSize: FontSize(16),
                                  fontWeight: FontWeight.w400,
                                  textAlign: TextAlign.left,
                                  letterSpacing: 0.4,
                                ),
                                "ul": Style(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .title2,
                                  fontSize: FontSize(16),
                                  fontWeight: FontWeight.w400,
                                  textAlign: TextAlign.left,
                                  letterSpacing: 0.4,
                                ),
                                "li": Style(
                                    color: StateContainer.of(context)
                                        .curTheme
                                        .title2,
                                    fontSize: FontSize(16),
                                    fontWeight: FontWeight.w400,
                                    textAlign: TextAlign.left,
                                    letterSpacing: 0.4,
                                    padding: const EdgeInsets.only(bottom: 10)),
                                "a": Style(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .primary,
                                  fontSize: FontSize(16),
                                  fontWeight: FontWeight.w400,
                                  textDecoration: TextDecoration.underline,
                                  textAlign: TextAlign.left,
                                  letterSpacing: 0.4,
                                ),
                                "strong": Style(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .title2,
                                  fontSize: FontSize(16),
                                  fontWeight: FontWeight.bold,
                                  textAlign: TextAlign.left,
                                  letterSpacing: 0.4,
                                )
                              },
                            )),
                        SizedBox(height: 30),
                        Container(
                            alignment: Alignment.center,
                            child: Column(children: getButtons(snapshot.data)))
                      ],
                    );
                  } else if (snapshot.hasError) {
                    return SnackBar(
                        backgroundColor:
                            StateContainer.of(context).curTheme.failure,
                        content: Text(
                          'Veuillez vous assurer que vous avez internet',
                          style: TextStyle(
                              color: StateContainer.of(context)
                                  .curTheme
                                  .neutralBackground),
                        ));
                  }
                  return CircularProgressIndicator();
                }),
          ),
        ));
  }
}
