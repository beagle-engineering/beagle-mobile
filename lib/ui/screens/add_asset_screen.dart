import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/models/api/asset.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/asset.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/ui/components/form_button.dart';
import 'package:getbeagle/ui/components/form_text_field.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';

class AddAssetScreen extends StatefulWidget {
  AddAssetScreen({Key key}) : super(key: key);

  @override
  _AddAssetScreenState createState() => _AddAssetScreenState();
}

class _AddAssetScreenState extends State<AddAssetScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  String _name = 'Autre';
  String _balance;
  String _riskProfile = '';
  String _typeId;
  String _providerId;
  String _otherName = '';
  Future<List<AssetType>> types;
  Future<List<AssetProvider>> providers;

  ScrollController _controller;

  @override
  void initState() {
    _controller = ScrollController();
    super.initState();
    providers = getAssetProviders(sl.get<ApiClient>());
    types = getAssetTypes(sl.get<ApiClient>());

    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          if (visible) {
            _controller.animateTo(_controller.position.maxScrollExtent,
                curve: Curves.linear, duration: Duration(milliseconds: 500));
          } else {
            _controller.animateTo(_controller.position.minScrollExtent,
                curve: Curves.linear, duration: Duration(milliseconds: 500));
          }
        });
      },
    );
  }

  void _onError(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: StateContainer.of(context).curTheme.failure,
        content: Text(
          message,
          style: TextStyle(
              color: StateContainer.of(context).curTheme.neutralBackground),
        )));
  }

  _create() {
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _balance = _balance.replaceAll(",", ".");
      String newName = _otherName.isEmpty ? _name : _otherName;
      Asset a = Asset(
          name: newName,
          type: AssetType(id: _typeId),
          provider: AssetProvider(id: _providerId),
          balance: (double.parse(_balance) * 100).round(),
          riskProfile: _riskProfile);
      createAsset(sl.get<ApiClient>(), a)
          .then((e) => Navigator.of(context).pop(true))
          .catchError((Object e) {
        _onError('Une erreur s\'est produite. Veuillez réessayer plus tard.');
      });
    } else {
      _onError('Veuillez ne laisser aucun champs vide');
    }
  }

  final Map<String, Map<String, bool>> _namesByType = {
    '7dc8f02b-b84c-4716-bce0-5982773e3aeb': {
      "Livret A": false,
      "Livret Jeune": false,
      "LDDS": false,
      "Livret ou compte épargne": false,
      "CEL": false,
      "PEL": false,
      "Autre": true,
    },
    '22421c26-5ad9-459f-a01f-b1777bb2de85': {
      "Compte-titres": false,
      "PEA": false,
      "PEA-PME": false,
      "Cryptomonnaies": false,
      "Autre": true,
    },
    'a734866b-b036-49db-874b-8289eca7d51a': {
      "Résidence Principale": false,
      "Résidence Secondaire": false,
      "SCPI": false,
      "Investissement locatif": false,
      "Autre": true,
    },
    '70217a80-f795-4557-9b6a-a73933d62f98': {
      "PEE": false,
      "PEI": false,
      "PEG": false,
      "Autre": true,
    },
    '5c3d3c9b-419a-4fb3-9fb9-d63f3f2b7574': {
      "PER": false,
      "PERCO": false,
      "Autre": true,
    },
    '33201cef-f2a3-4826-8a70-6492f585a1e2': {
      "Crédit immobilier": false,
      "Crédit étudiant": false,
      "Crédit à la consommation": false,
      "Autre": true,
    },
    '46242805-5742-4c00-846f-dc6882db6c52': {
      "Compte courant": false,
      "Autre": true,
    },
    '88dba8f1-33ca-46d6-95d5-20a5a47f6226': {
      "Assurance vie": false,
      "Autre": true,
    },
  };

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: StateContainer.of(context).curTheme.background,
          appBar: PreferredSize(
              preferredSize: Size.fromHeight(67.0),
              child: AppBar(
                  backgroundColor: Colors.transparent,
                  elevation: 0.0,
                  automaticallyImplyLeading: false,
                  title: InkWell(
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      child: Container(
                          margin: const EdgeInsets.only(
                              top: 50, right: 20, bottom: 20),
                          child: Row(children: [
                            Icon(FeatherIcons.chevronLeft,
                                color:
                                    StateContainer.of(context).curTheme.title2,
                                size: 19),
                            Container(
                                padding:
                                    const EdgeInsets.only(left: 12, top: 4),
                                alignment: Alignment.bottomLeft,
                                child: Text('Retour',
                                    style: TextStyle(
                                        fontSize: 14,
                                        height: 1.18,
                                        color: StateContainer.of(context)
                                            .curTheme
                                            .title2))),
                          ])),
                      onTap: () => Navigator.of(context).pop()))),
          body: SafeArea(
              child: SingleChildScrollView(
                  controller: _controller,
                  child: Column(children: <Widget>[
                    Container(
                        alignment: Alignment.center,
                        //padding: const EdgeInsets.only(top: 20),
                        margin: const EdgeInsets.fromLTRB(20, 10, 20, 15),
                        child: Text(
                          "Ajoute un placement",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: StateContainer.of(context).curTheme.title,
                              fontSize: 30),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        height: 600,
                        padding:
                            const EdgeInsets.only(left: 30, right: 30, top: 30),
                        child: Form(
                            key: _formKey,
                            autovalidate: true,
                            child: ListView(
                              padding: const EdgeInsets.only(bottom: 5.0),
                              children: <Widget>[
                                FutureBuilder<List<AssetType>>(
                                    future: types,
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData) {
                                        return FormField(
                                          builder: (FormFieldState state) {
                                            return InputDecorator(
                                              decoration: InputDecoration(
                                                labelText: "Type de placement",
                                                labelStyle: TextStyle(
                                                    color: StateContainer.of(
                                                            context)
                                                        .curTheme
                                                        .title2),
                                                focusedBorder:
                                                    UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: StateContainer.of(
                                                              context)
                                                          .curTheme
                                                          .primary),
                                                ),
                                                enabledBorder:
                                                    UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: StateContainer.of(
                                                              context)
                                                          .curTheme
                                                          .title2),
                                                ),
                                              ),
                                              isEmpty: _typeId == '',
                                              child:
                                                  DropdownButtonHideUnderline(
                                                child: DropdownButton(
                                                  value: _typeId,
                                                  isDense: true,
                                                  onChanged: (String newValue) {
                                                    setState(() {
                                                      //newContact.favoriteColor = newValue;
                                                      _typeId = newValue;
                                                      _name = _namesByType
                                                              .containsKey(
                                                                  _typeId)
                                                          ? _namesByType[
                                                                  _typeId]
                                                              .keys
                                                              .toList()[0]
                                                          : 'Autre';
                                                      state.didChange(newValue);
                                                    });
                                                  },
                                                  items: snapshot.data
                                                      .map((AssetType value) {
                                                    return DropdownMenuItem(
                                                      value: value.id,
                                                      child: Text(value.name),
                                                    );
                                                  }).toList(),
                                                ),
                                              ),
                                            );
                                          },
                                        );
                                      } else if (snapshot.hasError) {
                                        return Text("${snapshot.error}");
                                      }
                                      return CircularProgressIndicator();
                                    }),
                                _typeId ==
                                        '06a3149d-0c2a-4886-9025-fe7501bc8144'
                                    ? SizedBox(
                                        height: 30,
                                      )
                                    : Container(),
                                _typeId ==
                                        '06a3149d-0c2a-4886-9025-fe7501bc8144'
                                    ? StyledFocusForm2(
                                        validator: (input) => input.isEmpty
                                            ? 'Quel est le type de ce placement'
                                            : null,
                                        onSaved: (input) =>
                                            _riskProfile = input,
                                        label: "Donne un type à ce placement",
                                        obscureText: false,
                                      )
                                    : Container(),
                                SizedBox(
                                  height: 30,
                                ),
                                _namesByType.containsKey(_typeId)
                                    ? FormField(
                                        validator: (input) => _name.isEmpty
                                            ? 'choisir un nom dans la liste'
                                            : null,
                                        builder: (FormFieldState state) {
                                          return InputDecorator(
                                            decoration: InputDecoration(
                                              labelText: "Nom du placement",
                                              labelStyle: TextStyle(
                                                  color:
                                                      StateContainer.of(context)
                                                          .curTheme
                                                          .title2),
                                              focusedBorder:
                                                  UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: StateContainer.of(
                                                            context)
                                                        .curTheme
                                                        .primary),
                                              ),
                                              enabledBorder:
                                                  UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: StateContainer.of(
                                                            context)
                                                        .curTheme
                                                        .title2),
                                              ),
                                            ),
                                            isEmpty: _name == '',
                                            child: DropdownButtonHideUnderline(
                                              child: DropdownButton(
                                                value: _name,
                                                isDense: true,
                                                onChanged: (String newValue) {
                                                  setState(() {
                                                    //newContact.favoriteColor = newValue;
                                                    _name = newValue;
                                                    state.didChange(newValue);
                                                  });
                                                },
                                                items: _namesByType[_typeId]
                                                    .keys
                                                    .map((String value) {
                                                  return DropdownMenuItem(
                                                    value: value,
                                                    child: Text(value),
                                                  );
                                                }).toList(),
                                              ),
                                            ),
                                          );
                                        },
                                      )
                                    : FormField(
                                        builder: (FormFieldState state) {
                                          return InputDecorator(
                                            decoration: InputDecoration(
                                              labelText: "Nom du placement",
                                              labelStyle: TextStyle(
                                                  color:
                                                      StateContainer.of(context)
                                                          .curTheme
                                                          .title2),
                                              focusedBorder:
                                                  UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: StateContainer.of(
                                                            context)
                                                        .curTheme
                                                        .primary),
                                              ),
                                              enabledBorder:
                                                  UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: StateContainer.of(
                                                            context)
                                                        .curTheme
                                                        .title2),
                                              ),
                                            ),
                                            isEmpty: _name == '',
                                            child: DropdownButtonHideUnderline(
                                              child: DropdownButton(
                                                value: _name,
                                                isDense: true,
                                                onChanged: (String newValue) {
                                                  setState(() {
                                                    //newContact.favoriteColor = newValue;
                                                    _name = newValue;
                                                    state.didChange(newValue);
                                                  });
                                                },
                                                items: [
                                                  DropdownMenuItem(
                                                    value: 'Autre',
                                                    child: Text('Autre'),
                                                  )
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                _name == 'Autre'
                                    ? SizedBox(
                                        height: 30,
                                      )
                                    : Container(),
                                _name == 'Autre'
                                    ? StyledFocusForm2(
                                        validator: (input) => input.isEmpty
                                            ? 'Quel est le nom de ce placement'
                                            : null,
                                        onSaved: (input) => _otherName = input,
                                        label: "Donne un nom à ce placement",
                                        obscureText: false,
                                      )
                                    : Container(),
                                SizedBox(
                                  height: 30,
                                ),
                                StyledFocusForm2(
                                  keyboardType: TextInputType.number,
                                  validator: (input) => null,
                                  onSaved: (input) => input.isNotEmpty
                                      ? _balance = input
                                      : _balance = '0',
                                  label:
                                      "Quel est la valeur actuelle de ce placement?",
                                  obscureText: false,
                                  inputFormatters: [
                                    //WhitelistingTextInputFormatter.digitsOnly,
                                    WhitelistingTextInputFormatter(RegExp(
                                        '^\$|^(0|([1-9][0-9]{0,}))((\\.|\\,)[0-9]{0,})?\$'))
                                  ],
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                FutureBuilder<List<AssetProvider>>(
                                    future: providers,
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData) {
                                        return FormField(
                                          builder: (FormFieldState state) {
                                            return InputDecorator(
                                              decoration: InputDecoration(
                                                labelText:
                                                    "Dans quel établissement?",
                                                labelStyle: TextStyle(
                                                    color: StateContainer.of(
                                                            context)
                                                        .curTheme
                                                        .title2),
                                                focusedBorder:
                                                    UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: StateContainer.of(
                                                              context)
                                                          .curTheme
                                                          .primary),
                                                ),
                                                enabledBorder:
                                                    UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: StateContainer.of(
                                                              context)
                                                          .curTheme
                                                          .title2),
                                                ),
                                              ),
                                              isEmpty: _providerId == '',
                                              child:
                                                  DropdownButtonHideUnderline(
                                                child: DropdownButton(
                                                  value: _providerId,
                                                  isDense: true,
                                                  onChanged: (String newValue) {
                                                    setState(() {
                                                      //newContact.favoriteColor = newValue;
                                                      _providerId = newValue;
                                                      state.didChange(newValue);
                                                    });
                                                  },
                                                  items: snapshot.data.map(
                                                      (AssetProvider value) {
                                                    return DropdownMenuItem(
                                                      value: value.id,
                                                      child: Text(value.name),
                                                    );
                                                  }).toList(),
                                                ),
                                              ),
                                            );
                                          },
                                        );
                                      } else if (snapshot.hasError) {
                                        return Text("${snapshot.error}");
                                      }
                                      return CircularProgressIndicator();
                                    }),
                                SizedBox(
                                  height: 60,
                                ),
                                Container(
                                  margin: const EdgeInsets.only(bottom: 150),
                                  child: FormButton.primaryDefault(
                                    context: context,
                                    heroTag: 'add_asset',
                                    text: 'Ajouter',
                                    onPressed: _create,
                                  ),
                                ),
                              ],
                            ))),
                  ]))),
        ));
  }
}
