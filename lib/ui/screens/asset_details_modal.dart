import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/asset.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/utils/strings.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';

import '../../appstate_container.dart';

class AssetDetailsModal extends StatefulWidget {
  AssetDetailsModal(
      {Key key,
      this.icon,
      this.scrollController,
      this.balance = '',
      this.name = '',
      this.id = '',
      this.riskProfile,
      this.scaffoldKey})
      : super(key: key);

  final Widget icon;
  final String name;
  final String balance;
  final String id;
  final String riskProfile;
  final ScrollController scrollController;
  final GlobalKey<ScaffoldState> scaffoldKey;

  @override
  _AssetDetailsModalState createState() => _AssetDetailsModalState();
}

class _AssetDetailsModalState extends State<AssetDetailsModal> {
  FocusNode _focusNode;
  final myController = TextEditingController();

  bool _isEnabled = false;
  String _balance;
  double _height = 10;

  void initState() {
    super.initState();

    _focusNode = FocusNode();
    _balance = widget.balance;
    myController.text = _balance;
    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          if (visible) {
            _height = 200;
          } else {
            _height = 10;
          }
        });
      },
    );
  }

  @override
  void dispose() {
    _focusNode.dispose();
    myController.dispose();
    super.dispose();
  }

  void _onError(String message) async {
    widget.scaffoldKey.currentState.showSnackBar(SnackBar(
        duration: Duration(milliseconds: 2000),
        backgroundColor: StateContainer.of(context).curTheme.failure,
        content: Text(
          message,
          style: TextStyle(
              color: StateContainer.of(context).curTheme.neutralBackground),
        )));
  }

  _updateBalance(String newBalance) {
    int b = (double.parse(newBalance) * 100).round();
    updateAsset(
            api: sl.get<ApiClient>(),
            balance: b,
            riskProfile: widget.riskProfile,
            assetId: widget.id,
            name: widget.name)
        .catchError((Object e) {
      _onError('Une erreur s\'est produite. Veuillez réessayer plus tard.');
    }).then((value) {
      Navigator.of(context).pop(true);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: false,
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          ListTile(
            contentPadding: const EdgeInsets.only(top: 20, left: 30),
            title: Container(
                child: Text(
                    widget.name.isNotEmpty ? capitalize(widget.name) : "",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: StateContainer.of(context).curTheme.title,
                        fontSize: 16))),
            leading: widget.icon,
          ),
          ListTile(
            contentPadding: const EdgeInsets.only(left: 30, bottom: 10),
            title: TextField(
              keyboardType: TextInputType.number,
              inputFormatters: [
                //WhitelistingTextInputFormatter.digitsOnly,
                WhitelistingTextInputFormatter(
                    RegExp('^\$|^(0|([1-9][0-9]{0,}))((\\.|\\,)[0-9]{0,})?\$'))
              ],
              focusNode: _focusNode,
              controller: myController,
              onTap: () {
                if (!_isEnabled) {
                  setState(() {
                    _isEnabled = !_isEnabled;
                  });
                  _focusNode.requestFocus();
                }
              },
              //enabled: _isEnabled,

              decoration: InputDecoration(
                enabledBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
                // hintText: _balance,
                // hintStyle: TextStyle(
                //     color: StateContainer.of(context).curTheme.title,
                //     fontSize: 16)
              ),
              onEditingComplete: () {
                _focusNode.unfocus();
                setState(() {
                  _isEnabled = !_isEnabled;
                });
                if (myController.text.isNotEmpty) {
                  _updateBalance(myController.text);
                }
              },
            ),
            trailing: !_isEnabled
                ? InkWell(
                    onTap: () {
                      setState(() {
                        _isEnabled = !_isEnabled;
                      });
                      _focusNode.requestFocus();
                    },
                    child: Container(
                        child: Icon(FeatherIcons.edit2,
                            color: StateContainer.of(context).curTheme.title),
                        margin: const EdgeInsets.only(right: 32)))
                : InkWell(
                    onTap: () {
                      _focusNode.unfocus();
                      setState(() {
                        _isEnabled = !_isEnabled;
                      });
                      if (myController.text.isNotEmpty) {
                        _updateBalance(myController.text);
                      }
                    },
                    child: Container(
                        child: Text('OK',
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: StateContainer.of(context)
                                    .curTheme
                                    .success)),
                        margin: const EdgeInsets.only(right: 32))),
          ),
          Divider(
            height: 0.2,
            color: StateContainer.of(context).curTheme.title2,
            indent: 20,
            endIndent: 20,
          ),
          InkWell(
              onTap: () async {
                bool shouldClose = true;
                await showCupertinoDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                          backgroundColor: StateContainer.of(context)
                              .curTheme
                              .neutralBackground,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          title: Text(
                            'Supprimer ce placement ?',
                            style: TextStyle(
                                color:
                                    StateContainer.of(context).curTheme.title),
                          ),
                          actions: <Widget>[
                            CupertinoButton(
                              child: Text(
                                'Supprimer',
                                style: TextStyle(
                                    color: StateContainer.of(context)
                                        .curTheme
                                        .failure),
                              ),
                              onPressed: () {
                                shouldClose = true;
                                Navigator.of(context).pop();
                              },
                            ),
                            CupertinoButton(
                              child: Text(
                                'Annuler',
                                style: TextStyle(
                                    color: StateContainer.of(context)
                                        .curTheme
                                        .primary),
                              ),
                              onPressed: () {
                                shouldClose = false;
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        ));

                if (shouldClose) {
                  await deleteAsset(sl.get<ApiClient>(), widget.id)
                      .then((e) => Navigator.of(context).pop(true))
                      .catchError((Object e) {
                    Scaffold.of(context).showSnackBar(SnackBar(
                        backgroundColor:
                            StateContainer.of(context).curTheme.failure,
                        content: Text(
                          'Une erreur s\'est produite. Veuillez réessayer plus tard.',
                          style: TextStyle(
                              color: StateContainer.of(context)
                                  .curTheme
                                  .neutralBackground),
                        )));
                  });
                }
              },
              child: Center(
                  heightFactor: 2.5,
                  child: Icon(
                    FeatherIcons.trash2,
                    color: StateContainer.of(context).curTheme.failure,
                  ))),
          SizedBox(height: _height)
        ]));
  }
}
