import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/services/api/user.dart';
import 'package:getbeagle/localization.dart';
import 'package:getbeagle/services/firebase/analytics_service.dart';
import 'package:getbeagle/ui/components/form_button.dart';
import 'package:getbeagle/ui/components/form_text_field.dart';
import 'package:getbeagle/ui/components/icon_box.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';

class SignupScreen extends StatefulWidget {
  static final String route = 'signup_screen';

  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  String _email, _password, _firstName, _lastName;

  ScrollController _controller;
  double _bottomPadding = 50;
  final TextEditingController _pass = TextEditingController();

  @override
  void initState() {
    _controller = ScrollController();
    super.initState();

    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          if (visible) {
            _controller.animateTo(_controller.position.maxScrollExtent,
                curve: Curves.linear, duration: Duration(milliseconds: 500));
          } else {
            _controller.animateTo(_controller.position.minScrollExtent,
                curve: Curves.linear, duration: Duration(milliseconds: 500));
          }
        });
      },
    );
  }

  _submit() {
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    print(AppLocalization.currentLocale);
    AppLocalization.load(Locale("fr", "FR"));
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      signup(
              api: sl.get<ApiClient>(),
              email: _email,
              password: _password,
              firstName: _firstName,
              lastName: _lastName)
          .then((e) async {
        if (e == null) {
          await sl.get<AnalyticsService>().logSignUp();
          _scaffoldKey.currentState.showSnackBar(SnackBar(
              backgroundColor: StateContainer.of(context).curTheme.success,
              duration: Duration(milliseconds: 3000),
              content: Text(
                'Bienvenue Chez Beagle 🐶\nConnecte toi avec tes identifiants.',
                style: TextStyle(
                    color:
                        StateContainer.of(context).curTheme.neutralBackground),
              )));
          await sl.get<ApiClient>().deleteJWT();
          await Future.delayed(const Duration(seconds: 3));
          await Navigator.of(context).pushNamedAndRemoveUntil(
              '/login', (Route<dynamic> route) => false);
        } else {
          String _message = '';
          e.errors.forEach((key, value) {
            if (key == 'Password') {
              _message +=
                  'Le mot de passe doit faire 8 caractères ou plus et possèder au moins:\n\nun chiffre, une lettre minuscule, une lettre majuscule, un symbole.';
            }
            if (key == 'Mail') {
              _message += 'Adresse email déjà utilisée';
            } else {
              _message += 'Un problème est survenu, veuillez réessayer';
            }
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            backgroundColor: StateContainer.of(context).curTheme.failure,
            content: Text(
              _message,
              style: TextStyle(
                  color: StateContainer.of(context).curTheme.neutralBackground,
                  fontSize: 16),
            ),
            duration: Duration(seconds: 10),
            onVisible: () => _controller.animateTo(
                _controller.position.maxScrollExtent - 100,
                curve: Curves.linear,
                duration: Duration(milliseconds: 500)),
          ));
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: StateContainer.of(context).curTheme.background,
          body: SafeArea(
              child: Scrollbar(
                  child: SingleChildScrollView(
                      controller: _controller,
                      child: Column(
                        children: <Widget>[
                          Container(
                              alignment: Alignment.centerLeft,
                              margin: const EdgeInsets.only(left: 32, top: 30),
                              child: SvgPicture.asset(
                                  'assets/images/logo_blue.svg',
                                  width: 48,
                                  height: 48)),
                          Container(
                              alignment: Alignment.centerLeft,
                              margin: const EdgeInsets.only(left: 32, top: 24),
                              child: Text('Bienvenue!',
                                  style: TextStyle(
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .title,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 30))),
                          Container(
                              alignment: Alignment.centerLeft,
                              margin: const EdgeInsets.only(left: 32, top: 9),
                              child: Text('Crée ton compte Beagle',
                                  style: TextStyle(
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .title2,
                                      fontSize: 16))),
                          Form(
                            key: _formKey,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 0, top: 37.0),
                                  child: Row(children: [
                                    Padding(
                                        padding: const EdgeInsets.only(top: 20),
                                        child: IconBox(
                                          icon: FeatherIcons.user,
                                          iconColor: StateContainer.of(context)
                                              .curTheme
                                              .neutralBackground,
                                          boxColor: StateContainer.of(context)
                                              .curTheme
                                              .success,
                                          width: 35,
                                          height: 45,
                                        )),
                                    Container(
                                        width: 110,
                                        margin: const EdgeInsets.only(left: 20),
                                        child: StyledFocusForm2(
                                          onSaved: (input) => _firstName =
                                              input != null ? input : '',
                                          label: 'Prénom',
                                          obscureText: false,
                                        )),
                                    Container(
                                        width: 110,
                                        margin: const EdgeInsets.only(left: 20),
                                        child: StyledFocusForm2(
                                          onSaved: (input) => _lastName =
                                              input != null ? input : '',
                                          label: 'Nom',
                                          obscureText: false,
                                        ))
                                  ]),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 0, top: 30.0),
                                  child: Row(children: [
                                    Padding(
                                        padding: const EdgeInsets.only(top: 20),
                                        child: IconBox(
                                          icon: FeatherIcons.mail,
                                          iconColor: StateContainer.of(context)
                                              .curTheme
                                              .neutralBackground,
                                          boxColor: StateContainer.of(context)
                                              .curTheme
                                              .classicOrange,
                                          width: 35,
                                          height: 45,
                                        )),
                                    Container(
                                        width: 230,
                                        margin: const EdgeInsets.only(left: 20),
                                        child: StyledFocusForm2(
                                          validator: (input) => input
                                                      .isNotEmpty &&
                                                  !input.contains('@')
                                              ? 'Veuillez entrer une adresse mail valide'
                                              : null,
                                          onSaved: (input) =>
                                              _email = input.trim(),
                                          label: 'Email',
                                          obscureText: false,
                                        ))
                                  ]),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 0, top: 30.0),
                                  child: Row(children: [
                                    Padding(
                                        padding: const EdgeInsets.only(top: 20),
                                        child: IconBox(
                                          icon: FeatherIcons.lock,
                                          iconColor: StateContainer.of(context)
                                              .curTheme
                                              .neutralBackground,
                                          boxColor: StateContainer.of(context)
                                              .curTheme
                                              .classicPink,
                                          width: 35,
                                          height: 45,
                                        )),
                                    Container(
                                        width: 230,
                                        margin: const EdgeInsets.only(left: 20),
                                        child: StyledFocusForm2(
                                          controller: _pass,
                                          validator: (input) => input.isEmpty
                                              ? 'Mot de passe requis'
                                              : null,
                                          onSaved: (input) => _password = input,
                                          label: 'Mot de passe',
                                          obscureText: true,
                                        ))
                                  ]),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 0, top: 30.0),
                                  child: Row(children: [
                                    Padding(
                                        padding: const EdgeInsets.only(top: 20),
                                        child: IconBox(
                                          icon: FeatherIcons.lock,
                                          iconColor: StateContainer.of(context)
                                              .curTheme
                                              .neutralBackground,
                                          boxColor: StateContainer.of(context)
                                              .curTheme
                                              .classicPink,
                                          width: 35,
                                          height: 45,
                                        )),
                                    Container(
                                        width: 230,
                                        margin: const EdgeInsets.only(left: 20),
                                        child: StyledFocusForm2(
                                          validator: (input) {
                                            if (input.isEmpty) {
                                              return 'Confirmation requise';
                                            } else if (input != _pass.text) {
                                              return 'Entrez le même mot de passe';
                                            }
                                            return null;
                                          },
                                          onSaved: (input) => _password = input,
                                          label: 'Confirmez le mot de passe',
                                          obscureText: true,
                                        ))
                                  ]),
                                ),
                                SizedBox(height: 64.0),
                                FormButton.primaryDefault(
                                    context: context,
                                    heroTag: 'create_account',
                                    text: 'Créer',
                                    onPressed: _submit),
                                SizedBox(height: 18.0),
                                FlatButton(
                                  child: Text(
                                    'Vous avez un compte?',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 14,
                                        height: 1.7,
                                        color: StateContainer.of(context)
                                            .curTheme
                                            .title2),
                                  ),
                                  onPressed: () => Navigator.of(context)
                                      .pushNamedAndRemoveUntil('/login',
                                          (Route<dynamic> route) => false),
                                ),
                                SizedBox(height: _bottomPadding),
                              ],
                            ),
                          ),
                        ],
                      )))),
        ));
  }
}
