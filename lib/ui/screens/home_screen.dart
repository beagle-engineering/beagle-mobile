import 'package:flutter/material.dart';

import 'package:feather_icons_flutter/feather_icons_flutter.dart';

import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/models/api/user.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/services/api/user.dart';
import 'package:getbeagle/services/firebase/dynamic_links_service.dart';
import 'package:getbeagle/services/firebase/notification_service.dart';
import 'package:getbeagle/ui/screens/choose_interest_screen.dart';
import 'package:getbeagle/ui/screens/tabs/analytics.dart' as left;
import 'package:getbeagle/ui/screens/tabs/home.dart' as center;
import 'package:getbeagle/ui/screens/tabs/knowledge.dart' as right;
import 'package:getbeagle/ui/utils/solid_indicator.dart';
import 'package:getbeagle/utils/users.dart';

import '../../service_locator.dart';

class HomeScreen extends StatefulWidget {
  static final String id = 'home_screen';

  final bool onLaunch;

  const HomeScreen({Key key, this.onLaunch = false}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  String _tabTitle;

  @override
  void initState() {
    super.initState();
    if (widget.onLaunch) {
      handleOnLaunch();
    }

    _tabController = TabController(vsync: this, length: 3);
    _tabController.animateTo(1);
    _tabTitle = 'Home';
  }

  void handleOnLaunch() async {
    await getCurrentUser(api: sl.get<ApiClient>()).then((User user) async {
      await setUserPrefs(user);

      // init firebase components
      await sl.get<DynamicLinkService>().handleDynamicLinks();
      await sl.get<PushNotificationService>().initialise();
      await sl.get<PushNotificationService>().subscribe('all');
      if (isPremium(user.plan)) {
        sl.get<PushNotificationService>().subscribe('premium');
      } else {
        sl.get<PushNotificationService>().subscribe('free');
      }

      if (user.email == 'freeUser@getbeagle.app' ||
          user.email == 'test_user_api@getbeagle.app') {
        sl.get<PushNotificationService>().subscribe('testers');
      }
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void changeTitle(int index) {
    setState(() {
      switch (index) {
        case 0:
          {
            _tabTitle = 'Finances';
          }
          break;
        case 1:
          {
            _tabTitle = 'Home';
          }
          break;
        case 2:
          {
            _tabTitle = 'Bibliothèque';
          }
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: StateContainer.of(context).curTheme.background,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(90.0),
            child: Container(
                margin: const EdgeInsets.fromLTRB(0, 40, 0, 20),
                child: AppBar(
                  titleSpacing: 0.0,
                  centerTitle: false,
                  title: Container(
                      margin: const EdgeInsets.only(left: 24),
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        _tabTitle,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: StateContainer.of(context).curTheme.title,
                            fontSize: 30),
                        textAlign: TextAlign.left,
                      )),
                  backgroundColor: Colors.transparent,
                  elevation: 0.0,
                  actions: <Widget>[
                    GestureDetector(
                        onTap: () =>
                            Navigator.of(context).pushNamed('/settings'),
                        child: Container(
                            margin: EdgeInsets.only(right: 26.0),
                            width: 30,
                            height: 30,
                            child: Container(
                                padding: const EdgeInsets.all(2),
                                decoration: BoxDecoration(
                                    color: StateContainer.of(context)
                                        .curTheme
                                        .settingMenu,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(30))),
                                child: Container(
                                    //padding: EdgeInsets.only(right: 24.0),
                                    decoration: BoxDecoration(
                                        color: StateContainer.of(context)
                                            .curTheme
                                            .background,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(30))),
                                    child: Center(
                                        child: Icon(FeatherIcons.user,
                                            color: StateContainer.of(context)
                                                .curTheme
                                                .settingMenu)))))),
                  ],
                ))),
        bottomNavigationBar: Container(
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: StateContainer.of(context)
                      .curTheme
                      .text
                      .withOpacity(0.05),
                  blurRadius: 15.0, // has the effect of softening the shadow
                  spreadRadius: 1, // has the effect of extending the shadow
                  offset: Offset(
                    0.0, // horizontal, move right 0
                    0.0, // vertical, move down 0
                  ),
                )
              ],
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30.0),
                  topLeft: Radius.circular(30.0),
                ),
                child: SizedBox(
                    height: 95,
                    child: Material(
                        color: StateContainer.of(context)
                            .curTheme
                            .neutralBackground,
                        child: TabBar(
                          controller: _tabController,
                          indicator: SolidIndicator(),
                          tabs: [
                            Container(
                              height: 60,
                              child: Tab(icon: Icon(FeatherIcons.activity)),
                            ),
                            Container(
                              height: 60,
                              child: Tab(icon: Icon(FeatherIcons.home)),
                            ),
                            Container(
                              height: 60,
                              child: Tab(icon: Icon(FeatherIcons.book)),
                            ),
                          ],
                          unselectedLabelColor: StateContainer.of(context)
                              .curTheme
                              .text
                              .withOpacity(0.45),
                          onTap: changeTitle,
                        ))))),
        body: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            controller: _tabController,
            children: <Widget>[
              left.AnalyticsTab(),
              center.HomeTab(),
              right.KnowledgeTab()
            ]));
  }
}
