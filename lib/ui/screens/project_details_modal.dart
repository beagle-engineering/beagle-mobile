import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/services/api/project.dart';
import 'package:getbeagle/ui/utils/action_snackbar.dart';
import 'package:getbeagle/utils/strings.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';

import '../../appstate_container.dart';

class ProjectDetailsModal extends StatefulWidget {
  ProjectDetailsModal(
      {Key key,
      this.icon,
      this.iconColor,
      this.scrollController,
      this.target = '',
      this.title = '',
      this.current = '',
      this.endedAt = '',
      this.id = '',
      this.scaffoldKey})
      : super(key: key);

  final Color iconColor;
  final IconData icon;
  final String title;
  final String target;
  final String current;
  final String endedAt;
  final String id;
  final GlobalKey<ScaffoldState> scaffoldKey;

  final ScrollController scrollController;

  @override
  _ProjectDetailsModalState createState() => _ProjectDetailsModalState();
}

class _ProjectDetailsModalState extends State<ProjectDetailsModal> {
  FocusNode _focusNode;
  final myController = TextEditingController();
  String _current;
  double _height = 10;

  bool _isEnabled = false;

  void initState() {
    super.initState();

    _focusNode = FocusNode();
    _current = widget.current;
    myController.text = _current;
    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          if (visible) {
            _height = 200;
          } else {
            _height = 10;
          }
        });
      },
    );
  }

  _updateProject(String newCurrent) {
    int b = (double.parse(newCurrent) * 100).round();
    updateProject(
            api: sl.get<ApiClient>(),
            current: b,
            projectId: widget.id,
            name: widget.title)
        .catchError((Object e) {
      onErrorSnackbar(
          'Une erreur s\'est produite. Veuillez réessayer plus tard.',
          widget.scaffoldKey,
          context,
          Duration(milliseconds: 2000));
    }).then((value) {
      Navigator.of(context).pop(true);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: false,
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          ListTile(
            contentPadding: const EdgeInsets.only(top: 20, left: 20),
            title: Container(
                child: Text(
                    widget.title.isNotEmpty ? capitalize(widget.title) : "",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: StateContainer.of(context).curTheme.title,
                        fontSize: 16))),
            leading: Container(
                //margin: const EdgeInsets.only(top: 20),
                alignment: Alignment.center,
                width: 45,
                height: 45,
                padding: const EdgeInsets.all(7),
                decoration: BoxDecoration(
                    color: widget.iconColor.withOpacity(0.2),
                    borderRadius: BorderRadius.all(Radius.circular(8))),
                child: Center(
                    heightFactor: 0.1,
                    child: Icon(widget.icon,
                        color: widget.iconColor.withOpacity(0.9)))),
          ),
          ListTile(
              contentPadding: const EdgeInsets.only(left: 30),
              title: Container(
                  child: Text(widget.target,
                      style: TextStyle(
                          color: StateContainer.of(context).curTheme.title,
                          fontSize: 16))),
              leading: Icon(FeatherIcons.target,
                  color: StateContainer.of(context).curTheme.title)),
          ListTile(
              contentPadding: const EdgeInsets.only(left: 30),
              title: TextField(
                keyboardType: TextInputType.number,
                inputFormatters: [
                  //WhitelistingTextInputFormatter.digitsOnly,
                  WhitelistingTextInputFormatter(RegExp(
                      '^\$|^(0|([1-9][0-9]{0,}))((\\.|\\,)[0-9]{0,})?\$'))
                ],
                focusNode: _focusNode,
                controller: myController,
                onTap: () {
                  if (!_isEnabled) {
                    setState(() {
                      _isEnabled = !_isEnabled;
                    });
                    _focusNode.requestFocus();
                  }
                },
                //enabled: _isEnabled,

                decoration: InputDecoration(
                  enabledBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  // hintText: _balance,
                  // hintStyle: TextStyle(
                  //     color: StateContainer.of(context).curTheme.title,
                  //     fontSize: 16)
                ),
                onEditingComplete: () {
                  _focusNode.unfocus();
                  setState(() {
                    _isEnabled = !_isEnabled;
                  });
                  if (myController.text.isNotEmpty) {
                    _updateProject(myController.text);
                  }
                },
              ),
              trailing: !_isEnabled
                  ? InkWell(
                      onTap: () {
                        setState(() {
                          _isEnabled = !_isEnabled;
                        });
                        _focusNode.requestFocus();
                      },
                      child: Container(
                          child: Icon(FeatherIcons.edit2,
                              color: StateContainer.of(context).curTheme.title),
                          margin: const EdgeInsets.only(right: 32)))
                  : InkWell(
                      onTap: () {
                        _focusNode.unfocus();
                        setState(() {
                          _isEnabled = !_isEnabled;
                        });
                        if (myController.text.isNotEmpty) {
                          _updateProject(myController.text);
                        }
                      },
                      child: Container(
                          child: Text('OK',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .success)),
                          margin: const EdgeInsets.only(right: 32))),
              leading: Icon(FeatherIcons.dollarSign,
                  color: StateContainer.of(context).curTheme.title)),
          ListTile(
              contentPadding: const EdgeInsets.only(left: 30, bottom: 10),
              title: Container(
                  child: Text(widget.endedAt,
                      style: TextStyle(
                          color: StateContainer.of(context).curTheme.title,
                          fontSize: 16))),
              leading: Icon(FeatherIcons.calendar,
                  color: StateContainer.of(context).curTheme.title)),
          Divider(
            height: 0.2,
            color: StateContainer.of(context).curTheme.title2,
            indent: 20,
            endIndent: 20,
          ),
          InkWell(
              onTap: () async {
                bool shouldClose = true;
                await showCupertinoDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                          backgroundColor: StateContainer.of(context)
                              .curTheme
                              .neutralBackground,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          title: Text(
                            'Supprimer ce projet ?',
                            style: TextStyle(
                                color:
                                    StateContainer.of(context).curTheme.title),
                          ),
                          actions: <Widget>[
                            CupertinoButton(
                              child: Text(
                                'Supprimer',
                                style: TextStyle(
                                    color: StateContainer.of(context)
                                        .curTheme
                                        .failure),
                              ),
                              onPressed: () {
                                shouldClose = true;
                                Navigator.of(context).pop();
                              },
                            ),
                            CupertinoButton(
                              child: Text(
                                'Annuler',
                                style: TextStyle(
                                    color: StateContainer.of(context)
                                        .curTheme
                                        .primary),
                              ),
                              onPressed: () {
                                shouldClose = false;
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        ));

                if (shouldClose) {
                  deleteProject(sl.get<ApiClient>(), widget.id)
                      .then((e) => Navigator.of(context)
                          .pushNamedAndRemoveUntil(
                              '/home', (Route<dynamic> route) => false))
                      .catchError((Object e) {
                    Scaffold.of(context).showSnackBar(SnackBar(
                        backgroundColor:
                            StateContainer.of(context).curTheme.failure,
                        content: Text(
                          'Une erreur s\'est produite. Veuillez réessayer plus tard.',
                          style: TextStyle(
                              color: StateContainer.of(context)
                                  .curTheme
                                  .neutralBackground),
                        )));
                  });
                }
              },
              child: Center(
                  heightFactor: 2.5,
                  child: Icon(
                    FeatherIcons.trash2,
                    color: StateContainer.of(context).curTheme.failure,
                  ))),
          SizedBox(height: _height)
        ]));
  }
}
