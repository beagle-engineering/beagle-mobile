import 'package:dotted_border/dotted_border.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/models/api/knowledge.dart';
import 'package:getbeagle/models/screens/knowledge_content.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/services/api/knowledge.dart';
import 'package:getbeagle/ui/components/action_button.dart';
import 'package:getbeagle/ui/components/content_card.dart';
import 'package:getbeagle/ui/components/section_title.dart';
import 'package:getbeagle/ui/screens/choose_interest_screen.dart';
import 'package:getbeagle/ui/utils/custom_page_scroll.dart';

class KnowledgeTab extends StatefulWidget {
  const KnowledgeTab({Key key, this.updated}) : super(key: key);

  final bool updated;
  @override
  _KnowledgeTabState createState() => _KnowledgeTabState();
}

class _KnowledgeTabState extends State<KnowledgeTab> {
  _KnowledgeTabState();
  Future<List<KnowledgeJourney>> knowledgeJourneys;
  Future<List<KnowledgeContent>> starredContent;
  var _controller = ScrollController();
  bool _searchBarVisible = true;
  List<KnowledgeJourney> duplicateItems;
  List<KnowledgeJourney> allItems;
  Set<String> starredIds = Set<String>();

  @override
  void initState() {
    super.initState();
    setState(() {
      knowledgeJourneys = getKnowledgeByInterest(sl.get<ApiClient>());
      starredContent = getPinnedContent(sl.get<ApiClient>());
    });

    _controller.addListener(() {
      if (duplicateItems == null || duplicateItems.length > 2) {
        if (_controller.position.userScrollDirection ==
                ScrollDirection.forward &&
            !_searchBarVisible) {
          setState(() {
            _searchBarVisible = true;
          });
        }

        if (_controller.position.userScrollDirection !=
                ScrollDirection.forward &&
            _searchBarVisible) {
          setState(() {
            _searchBarVisible = false;
          });
        }
      }
    });
  }

  // This height will allow for all the Card's content to fit comfortably within the card.
  static const height = 200.0;

  Color _getCarouselColor(int i) {
    final List<Color> beagleColors = [
      StateContainer.of(context).curTheme.primary,
      StateContainer.of(context).curTheme.warning,
      StateContainer.of(context).curTheme.failure,
    ];
    return beagleColors[i % beagleColors.length];
  }

  TextEditingController editingController = TextEditingController();

  List<Widget> getbibli(List<KnowledgeJourney> duplicateItems) {
    List<Widget> bibli = [
      FutureBuilder<List<KnowledgeContent>>(
          future: starredContent,
          builder: (context, snapshot) {
            if (snapshot.hasData &&
                snapshot.connectionState == ConnectionState.done) {
              for (final s in snapshot.data) {
                starredIds.add(s.id);
              }
              if (snapshot.data.isEmpty) {
                return Container();
              } else {
                return Padding(
                    padding: const EdgeInsets.only(bottom: 16),
                    child: Column(children: [
                      Padding(
                          padding: const EdgeInsets.fromLTRB(24, 20, 0, 20),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Icon(Icons.bookmark,
                                    color: StateContainer.of(context)
                                        .curTheme
                                        .warning),
                                Text('Articles sauvegardés',
                                    style: TextStyle(
                                        color: StateContainer.of(context)
                                            .curTheme
                                            .lists
                                            .title,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 24)),
                              ])),
                      Container(
                          height: height,
                          child: ListView.builder(
                              padding:
                                  const EdgeInsets.only(left: 10, right: 20),
                              scrollDirection: Axis.horizontal,
                              physics: CustomScrollPhysics(itemDimension: 120),
                              itemCount: snapshot.data.length,
                              itemBuilder: (_, i) {
                                return Container(
                                    decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        boxShadow: [
                                          BoxShadow(
                                            color:
                                                Color.fromRGBO(0, 0, 0, 0.08),
                                            blurRadius: 16.0,
                                            offset: Offset(0, 4),
                                            spreadRadius: -9,
                                          ),
                                        ],
                                        borderRadius:
                                            BorderRadius.circular(24.0)),
                                    child: ContentCard(
                                      title: i < snapshot.data.length
                                          ? snapshot.data[i].title
                                          : '',
                                      onTap: () async {
                                        final bool updated =
                                            await Navigator.of(context)
                                                .pushNamed(
                                          '/knowledge_content',
                                          arguments: KnowledgeContentArgs(
                                              fullContent: snapshot.data[i],
                                              nextSteps:
                                                  snapshot.data.sublist(i + 1),
                                              moreColor:
                                                  StateContainer.of(context)
                                                      .curTheme
                                                      .warning
                                                      .withAlpha(240),
                                              isStarred: true),
                                        ) as bool;

                                        setState(() {
                                          snapshot.data[i].read = true;
                                        });

                                        if (updated != null && updated) {
                                          setState(() {
                                            starredIds.clear();

                                            starredContent = getPinnedContent(
                                                sl.get<ApiClient>());
                                          });
                                        }
                                      },
                                      cardColor: snapshot.data[i].read
                                          ? StateContainer.of(context)
                                              .curTheme
                                              .warning
                                              .withAlpha(140)
                                          : StateContainer.of(context)
                                              .curTheme
                                              .warning
                                              .withAlpha(240),
                                    ));
                              }))
                    ]));
              }
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            return CircularProgressIndicator();
          }),
    ];
    bibli.addAll(duplicateItems
        .asMap()
        .entries
        .map((knowledgeJourney) => Padding(
            padding: const EdgeInsets.only(bottom: 16),
            child: Column(
              children: [
                SectionTitle(title: knowledgeJourney.value.title),
                Container(
                    height: height,
                    child: ListView.builder(
                        padding: const EdgeInsets.only(left: 10, right: 20),
                        scrollDirection: Axis.horizontal,
                        physics: CustomScrollPhysics(itemDimension: 120),
                        itemCount: knowledgeJourney.value.content.length,
                        itemBuilder: (_, i) {
                          return Container(
                              decoration: BoxDecoration(
                                  color: Colors.transparent,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.08),
                                      blurRadius: 16.0,
                                      offset: Offset(0, 4),
                                      spreadRadius: -9,
                                    ),
                                  ],
                                  borderRadius: BorderRadius.circular(24.0)),
                              child: ContentCard(
                                title: i < knowledgeJourney.value.content.length
                                    ? knowledgeJourney.value.content[i].title
                                    : '',
                                onTap: () async {
                                  final bool updated =
                                      await Navigator.of(context).pushNamed(
                                    '/knowledge_content',
                                    arguments: KnowledgeContentArgs(
                                        fullContent:
                                            knowledgeJourney.value.content[i],
                                        nextSteps: knowledgeJourney
                                            .value.content
                                            .sublist(i + 1),
                                        moreColor: _getCarouselColor(
                                                knowledgeJourney.key)
                                            .withAlpha(240),
                                        isStarred: starredIds.contains(
                                            knowledgeJourney
                                                .value.content[i].id)),
                                  ) as bool;

                                  setState(() {
                                    knowledgeJourney.value.content[i].read =
                                        true;
                                  });

                                  if (updated != null && updated) {
                                    setState(() {
                                      starredIds.clear();
                                      starredContent =
                                          getPinnedContent(sl.get<ApiClient>());
                                    });
                                  }
                                },
                                cardColor: knowledgeJourney
                                        .value.content[i].read
                                    ? _getCarouselColor(knowledgeJourney.key)
                                        .withAlpha(140)
                                    : _getCarouselColor(knowledgeJourney.key)
                                        .withAlpha(240),
                              ));
                        }))
              ],
            )))
        .toList());
    bibli.add(_getEditInterest());
    return bibli;
  }

  Widget _getEditInterest() {
    return Padding(
        padding: const EdgeInsets.only(top: 20, bottom: 20),
        child: Align(
            alignment: Alignment.center,
            child: DottedBorder(
                color: StateContainer.of(context)
                    .curTheme
                    .lists
                    .title
                    .withOpacity(0.4),
                borderType: BorderType.RRect,
                radius: Radius.circular(20),
                dashPattern: [4, 2],
                child: Container(
                    width: 270,
                    height: 100,
                    margin: const EdgeInsets.fromLTRB(30, 20, 30, 20),
                    child: Align(
                        alignment: Alignment.topCenter,
                        child: Column(children: [
                          SizedBox(
                              height: 60,
                              child: ActionButton.primarySmall(
                                  context: context,
                                  icon: FeatherIcons.edit2,
                                  onTap: () async {
                                    final bool updated =
                                        await Navigator.of(context)
                                            .push(MaterialPageRoute(
                                      builder: (context) =>
                                          ChooseInterestScreen(),
                                    )) as bool;
                                    if (updated != null && updated) {
                                      setState(() {
                                        duplicateItems = null;
                                        knowledgeJourneys =
                                            getKnowledgeByInterest(
                                                sl.get<ApiClient>());
                                      });
                                    }
                                  })),
                          SizedBox(
                              height: 30,
                              child: Padding(
                                  padding: const EdgeInsets.only(top: 7),
                                  child: Text('Centres d\'intérêt',
                                      style: TextStyle(
                                          color: StateContainer.of(context)
                                              .curTheme
                                              .primary,
                                          //.withOpacity(0.8),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                          height: 1.5),
                                      textAlign: TextAlign.center))),
                        ]))))));
  }

  List<Widget> bibli;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Scaffold(
            backgroundColor: StateContainer.of(context).curTheme.background,
            body: Scrollbar(
              child: FutureBuilder<List<KnowledgeJourney>>(
                  future: knowledgeJourneys,
                  builder: (context, snapshot) {
                    if (snapshot.hasData &&
                        snapshot.connectionState == ConnectionState.done) {
                      if (duplicateItems == null) {
                        duplicateItems = List.from(snapshot.data);
                      }
                      allItems = List.from(snapshot.data);
                      return Column(children: [
                        _searchBarVisible
                            ? AnimatedPadding(
                                duration: Duration(milliseconds: 4000),
                                curve: Curves.linear,
                                padding:
                                    const EdgeInsets.fromLTRB(24, 8, 24, 16),
                                child: TextField(
                                  onChanged: (query) {
                                    query = query.toLowerCase();
                                    List<KnowledgeJourney> dummySearchList =
                                        List<KnowledgeJourney>();
                                    dummySearchList.addAll(
                                        duplicateItems.isNotEmpty
                                            ? duplicateItems
                                            : allItems);
                                    if (query.isNotEmpty) {
                                      List<KnowledgeJourney> dummyListData =
                                          List<KnowledgeJourney>();
                                      dummySearchList.forEach((item) {
                                        if (item.title
                                            .toLowerCase()
                                            .contains(query)) {
                                          dummyListData.add(item);

                                          // } else {
                                          //   KnowledgeJourney newItem =
                                          //       KnowledgeJourney(
                                          //           id: item.id,
                                          //           title: item.title,
                                          //           description:
                                          //               item.description);
                                          //   bool found = false;
                                          //   item.content.forEach((content) {
                                          //     if (content.title
                                          //         .toLowerCase()
                                          //         .contains(query)) {
                                          //       newItem.content.add(content);
                                          //       found = true;
                                          //     }
                                          //   });
                                          //   if (found) {
                                          //     dummyListData.add(newItem);
                                          //   }
                                        }
                                      });
                                      setState(() {
                                        duplicateItems =
                                            List.from(dummyListData);
                                      });
                                      return;
                                    } else {
                                      setState(() {
                                        duplicateItems.clear();
                                        duplicateItems.addAll(allItems);
                                      });
                                    }
                                  },
                                  controller: editingController,
                                  decoration: InputDecoration(
                                      hintStyle: TextStyle(
                                          color: StateContainer.of(context)
                                              .curTheme
                                              .title2),
                                      hintText: "Rechercher",
                                      prefixIcon: Icon(FeatherIcons.search),
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: StateContainer.of(context)
                                                  .curTheme
                                                  .primary),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(12.0))),
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: StateContainer.of(context)
                                                  .curTheme
                                                  .title2),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(12.0)))),
                                ),
                              )
                            : AnimatedContainer(
                                duration: Duration(milliseconds: 4000),
                                curve: Curves.linear),
                        Expanded(
                            child: ListView(
                                controller: _controller,
                                children: getbibli(duplicateItems))),
                      ]);
                    } else if (snapshot.hasError) {
                      return Text("${snapshot.error}");
                    }
                    return CircularProgressIndicator();
                  }),
            )));
  }
}
