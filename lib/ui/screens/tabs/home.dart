import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:getbeagle/appstate_container.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:getbeagle/models/api/action.dart';
import 'package:getbeagle/models/api/project.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/action.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/services/api/project.dart';
import 'package:getbeagle/ui/components/action_button.dart';
import 'package:getbeagle/ui/components/action_card.dart';
import 'package:getbeagle/ui/components/floating_bottom_modal.dart';
import 'package:getbeagle/ui/components/list_item.dart';
import 'package:getbeagle/ui/components/section_title.dart';
import 'package:getbeagle/ui/screens/project_details_modal.dart';
import 'package:getbeagle/utils/strings.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeTab extends StatefulWidget {
  const HomeTab({Key key}) : super(key: key);
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  _HomeTabState();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  //Future<List<Project>> projects;
  NumberFormat f = NumberFormat("# ###.##", "fr_FR");

  //Future<List> projectsAndActions;

  Future<List<Project>> projects;
  Future<List<RecommendedAction>> usersActions;

  @override
  void initState() {
    super.initState();
    //projectsAndActions = getCurrentUserProjectsAndActions(sl.get<ApiClient>());
    projects = getCurrentUserProjects(sl.get<ApiClient>());
    usersActions = getCurrentUserActions(sl.get<ApiClient>());
  }

  Map<String, List> getIconFromProjects(List<Project> projects) {
    Map<String, List> returnValue = {};
    for (final p in projects) {
      if (p.label.name.toLowerCase().contains("annuel")) {
        returnValue[p.id] = [
          FeatherIcons.compass,
          StateContainer.of(context).curTheme.failure
        ];
      } else if (p.label.name.toLowerCase().contains("precaution") ||
          p.label.name.toLowerCase().contains("précaution")) {
        returnValue[p.id] = [
          FeatherIcons.umbrella,
          StateContainer.of(context).curTheme.warning
        ];
      } else if (p.label.name.toLowerCase().contains("immobilier")) {
        returnValue[p.id] = [
          FeatherIcons.home,
          StateContainer.of(context).curTheme.primary
        ];
      } else if (p.label.name.toLowerCase().contains("dynamise")) {
        returnValue[p.id] = [
          FeatherIcons.trendingUp,
          StateContainer.of(context).curTheme.success
        ];
      } else {
        returnValue[p.id] = [
          FeatherIcons.heart,
          StateContainer.of(context).curTheme.failure
        ];
      }
    }
    return returnValue;
  }

  List getProgress(Project project) {
    double p = (project.current / project.goal);
    String ps = "${f.format(p * 100)}%";
    if (p > 1.0) {
      p = 1.0;
    }
    if (p < 0.5) {
      return [p, ps, StateContainer.of(context).curTheme.failure];
    } else if (p < 0.8) {
      return [p, ps, StateContainer.of(context).curTheme.primary];
    } else {
      return [p, ps, StateContainer.of(context).curTheme.success];
    }
  }

  Color getColor(int index) {
    if (index % 3 == 0) {
      return StateContainer.of(context).curTheme.failure;
    } else if (index % 3 == 1) {
      return StateContainer.of(context).curTheme.warning;
    }
    return StateContainer.of(context).curTheme.primary;
  }

  int _index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: StateContainer.of(context).curTheme.background,
        body: Scrollbar(
            child: SingleChildScrollView(
                child: Column(children: [
          FutureBuilder<List<RecommendedAction>>(
              future: usersActions,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  List<Widget> children = <Widget>[];
                  print(snapshot.data);
                  if (snapshot.data.isNotEmpty) {
                    children.add(SectionTitle(title: 'Actions recommendées'));
                    children.add(Container(
                        height: 180,
                        decoration: BoxDecoration(
                            color: Colors.transparent,
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.08),
                                blurRadius: 16.0,
                                offset: Offset(0, 4),
                                spreadRadius: -9,
                              ),
                            ],
                            borderRadius: BorderRadius.circular(24.0)),
                        child: PageView.builder(
                            itemCount: snapshot.data.length,
                            controller: PageController(viewportFraction: 0.9),
                            onPageChanged: (int index) =>
                                setState(() => _index = index),
                            itemBuilder: (_, i) {
                              return Transform.scale(
                                scale: i == _index ? 1 : 0.95,
                                child: ActionCard(
                                  icon: FeatherIcons.umbrella,
                                  title: i < snapshot.data.length
                                      ? snapshot.data[i].name
                                      : '',
                                  description: i < snapshot.data.length
                                      ? snapshot.data[i].description
                                      : '',
                                  onTap: () async {
                                    if (snapshot.data[i].url != null &&
                                        snapshot.data[i].url.isNotEmpty) {
                                      if (await canLaunch(
                                          snapshot.data[i].url)) {
                                        await launch(snapshot.data[i].url);
                                      } else {
                                        throw 'Could not launch ${snapshot.data[i].url}';
                                      }
                                    }
                                  },
                                  cardColor: getColor(i).withAlpha(240),
                                ),
                              );
                            })));
                    return Column(children: children);
                  } else {
                    return Container();
                  }
                } else if (snapshot.hasError) {
                  return SnackBar(
                      backgroundColor:
                          StateContainer.of(context).curTheme.failure,
                      content: Text(
                        'Veuillez vous assurer que vous avez internet',
                        style: TextStyle(
                            color: StateContainer.of(context)
                                .curTheme
                                .neutralBackground),
                      ));
                }
                return CircularProgressIndicator();
              }),
          FutureBuilder<List<Project>>(
              future: projects,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  Map<String, List> icons = getIconFromProjects(snapshot.data);
                  List<Widget> children = <Widget>[];
                  children.add(SectionTitle(title: 'Projets'));
                  children.add(Container(
                      height: 115.0 * snapshot.data.length,
                      padding: const EdgeInsets.only(
                          left: 20, right: 20, bottom: 10),
                      child: ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          children: snapshot.data
                              .asMap()
                              .entries
                              .map((project) => SizedBox(
                                  height: 115,
                                  child: ListItem(
                                    onTap: () async {
                                      final bool updated =
                                          await showFloatingModalBottomSheet(
                                        context: context,
                                        builder: (context, scrollController) =>
                                            ProjectDetailsModal(
                                          icon: icons[project.value.id][0]
                                              as IconData,
                                          iconColor: icons[project.value.id][1]
                                              as Color,
                                          title: project.value.name,
                                          target: formatAmountInCts(
                                                  project.value.goal) +
                                              '\u20ac',
                                          current: formatAmountInCts(
                                                  project.value.current) +
                                              '\u20ac',
                                          endedAt: DateFormat.yMMMMd('fr_FR')
                                              .format(project.value.endedAt),
                                          id: project.value.id,
                                          scrollController: scrollController,
                                          scaffoldKey: _scaffoldKey,
                                        ),
                                      );
                                      if (updated != null && updated) {
                                        setState(() {
                                          projects = getCurrentUserProjects(
                                              sl.get<ApiClient>());
                                        });
                                      }
                                    },
                                    title: project.value.name,
                                    detail: formatAmountInCts(
                                            project.value.current) +
                                        '\u20ac / ' +
                                        formatAmountInCts(project.value.goal) +
                                        '\u20ac',
                                    icon:
                                        icons[project.value.id][0] as IconData,
                                    iconColor:
                                        icons[project.value.id][1] as Color,
                                    progressBarColor:
                                        getProgress(project.value)[2] as Color,
                                    progressValue:
                                        getProgress(project.value)[0] as double,
                                    progressValueString:
                                        getProgress(project.value)[1] as String,
                                  )))
                              .toList())));
                  return Column(children: children);
                } else if (snapshot.hasError) {
                  return SnackBar(
                      backgroundColor:
                          StateContainer.of(context).curTheme.failure,
                      content: Text(
                        'Veuillez vous assurer que vous avez internet',
                        style: TextStyle(
                            color: StateContainer.of(context)
                                .curTheme
                                .neutralBackground),
                      ));
                }
                return CircularProgressIndicator();
              }),
          Padding(
              padding: const EdgeInsets.only(top: 20, bottom: 20),
              child: Align(
                  alignment: Alignment.center,
                  child: DottedBorder(
                      color: StateContainer.of(context)
                          .curTheme
                          .lists
                          .title
                          .withOpacity(0.4),
                      borderType: BorderType.RRect,
                      radius: Radius.circular(20),
                      dashPattern: [4, 2],
                      child: Container(
                          width: 270,
                          height: 100,
                          margin: const EdgeInsets.fromLTRB(30, 20, 30, 20),
                          child: Align(
                              alignment: Alignment.topCenter,
                              child: Column(children: [
                                SizedBox(
                                    height: 60,
                                    child: ActionButton.primarySmall(
                                        context: context,
                                        icon: FeatherIcons.plus,
                                        onTap: () {
                                          Navigator.of(context)
                                              .pushNamed('/create_project');
                                        })),
                                SizedBox(
                                    height: 30,
                                    child: Padding(
                                        padding: const EdgeInsets.only(top: 7),
                                        child: Text('Nouveau Projet',
                                            style: TextStyle(
                                                color:
                                                    StateContainer.of(context)
                                                        .curTheme
                                                        .primary,
                                                //.withOpacity(0.8),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16,
                                                height: 1.5),
                                            textAlign: TextAlign.center))),
                              ]))))))
        ]))));
  }
}
