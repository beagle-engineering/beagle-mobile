import 'package:dotted_border/dotted_border.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/models/api/asset.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/asset.dart';
import 'package:getbeagle/services/api/client.dart';

import 'package:getbeagle/ui/components/action_button.dart';
import 'package:getbeagle/ui/components/collapsable_section_title.dart';
import 'package:getbeagle/ui/components/floating_bottom_modal.dart';
import 'package:getbeagle/ui/components/list_item.dart';
import 'package:getbeagle/ui/screens/asset_details_modal.dart';
import 'package:getbeagle/utils/strings.dart';

class AnalyticsTab extends StatefulWidget {
  @override
  _AnalyticsTabState createState() => _AnalyticsTabState();
}

class _AnalyticsTabState extends State<AnalyticsTab> {
  _AnalyticsTabState();

  Future<List<Asset>> assetsFuture;

  @override
  void initState() {
    super.initState();
    // assetsFuture =
    //     Future.delayed(const Duration(milliseconds: 500), () => assets);
    assetsFuture = getCurrentUserAssets(sl.get<ApiClient>());
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  int totalWealth = 0;
  Map<String, bool> _collapsedSections;
  Map<String, List<Asset>> _assetsByTypes;
  double _assetListHeight = 0;

  double _computeAssetsListHeight(Map<String, List<Asset>> assets) {
    double height = 0;
    for (final key in _collapsedSections.keys) {
      if (assets.containsKey(key)) {
        height += 65;
        if (!_collapsedSections[key]) {
          height += 115 * assets[key].length;
        }
      }
    }
    return height;
  }

  List<Widget> _getAssetsWidgets(Map<String, List<Asset>> assetsMap) {
    List<Widget> widgets = List<Widget>();
    Map<String, bool> newCollapsedSections =
        Map<String, bool>.from(_collapsedSections);
    for (String key in assetsMap.keys) {
      widgets.add(InkWell(
          onTap: () {
            setState(() {
              newCollapsedSections[key] = !_collapsedSections[key];
              if (assetsMap.containsKey(key)) {
                if (newCollapsedSections[key]) {
                  _assetListHeight -= 115 * assetsMap[key].length;
                } else {
                  _assetListHeight += 115 * assetsMap[key].length;
                }
              }

              _collapsedSections = Map<String, bool>.from(newCollapsedSections);
            });
          },
          child: CollapsableSectionTitle(
              title: key,
              collapseIcon: Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: Icon(
                      _collapsedSections[key]
                          ? FeatherIcons.plusCircle
                          : FeatherIcons.minusCircle,
                      color: StateContainer.of(context).curTheme.title2)))));
      for (int i = 0; i < assetsMap[key].length; i++) {
        widgets.add(!_collapsedSections[key]
            ? SizedBox(
                height: 115,
                child: ListItem(
                    onTap: () async {
                      final bool updated = await showFloatingModalBottomSheet(
                        context: context,
                        builder: (context, scrollController) =>
                            AssetDetailsModal(
                          id: assetsMap[key][i].id,
                          icon: null,
                          name: assetsMap[key][i].name,
                          balance:
                              formatAmountInCts(assetsMap[key][i].balance) +
                                  '\u20ac',
                          scrollController: scrollController,
                          scaffoldKey: _scaffoldKey,
                          riskProfile: assetsMap[key][i].riskProfile,
                        ),
                      );
                      if (updated != null && updated) {
                        setState(() {
                          // assetsFuture = Future.delayed(
                          //     const Duration(milliseconds: 500), () => assets);
                          assetsFuture =
                              getCurrentUserAssets(sl.get<ApiClient>());
                        });
                      }
                    },
                    title: assetsMap[key][i].name,
                    detail:
                        formatAmountInCts(assetsMap[key][i].balance) + '\u20ac',
                    icon: FeatherIcons.dollarSign,
                    iconColor: StateContainer.of(context).curTheme.primary,
                    progressBarColor: null,
                    progressValue: null,
                    progressValueString: null,
                    svgImage: providersLogos
                            .containsKey(assetsMap[key][i].provider.id)
                        ? 'assets/images/finAssetProviders/${providersLogos[assetsMap[key][i].provider.id]}'
                        : null))
            : SizedBox());
      }
    }
    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: StateContainer.of(context).curTheme.background,
        body: Scrollbar(
            child: SingleChildScrollView(
                child: Column(children: [
          FutureBuilder<List<Asset>>(
              future: assetsFuture,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  _assetsByTypes = Map<String, List<Asset>>();

                  for (Asset asset in snapshot.data) {
                    if (_assetsByTypes[asset.type.name] == null) {
                      _assetsByTypes[asset.type.name] = List<Asset>();
                    }
                    _assetsByTypes[asset.type.name].add(asset);
                  }

                  totalWealth = 0;
                  for (Asset asset in snapshot.data) {
                    if (asset.type.id ==
                        '33201cef-f2a3-4826-8a70-6492f585a1e2') {
                      totalWealth -= asset.balance;
                    } else {
                      totalWealth += asset.balance;
                    }
                  }

                  if (_collapsedSections == null) {
                    _collapsedSections = Map<String, bool>();
                  }
                  Map<String, bool> newColl = Map<String, bool>();
                  for (String key in _assetsByTypes.keys) {
                    if (!_collapsedSections.containsKey(key)) {
                      _collapsedSections[key] = false;
                    }
                    newColl[key] = _collapsedSections[key];
                  }

                  _assetListHeight = _computeAssetsListHeight(_assetsByTypes);

                  return Column(children: [
                    Container(
                        margin: const EdgeInsets.only(
                            top: 24, bottom: 21, left: 24, right: 24),
                        height: 180,
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.16),
                              blurRadius: 32.0,
                              offset: Offset(0, 8),
                              spreadRadius: -9,
                            ),
                          ],
                        ),
                        child: Card(
                          // margin:
                          //     const EdgeInsets.only(left: 24, right: 24),
                          color: StateContainer.of(context)
                              .curTheme
                              .warning
                              .withAlpha(250),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(24.0),
                          ),
                          child: Align(
                            alignment: Alignment.center,
                            child: Text(
                              "${formatAmountInCts(totalWealth)} \u20ac",
                              style: TextStyle(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .neutralBackground
                                      .withOpacity(0.95),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 32,
                                  height: 1.2),
                            ),
                          ),
                        )),
                    Container(
                        height: _assetListHeight,
                        padding: const EdgeInsets.only(
                            left: 24, right: 24, bottom: 10),
                        child: ListView(
                            physics: const NeverScrollableScrollPhysics(),
                            children: _getAssetsWidgets(_assetsByTypes)))
                  ]);
                } else if (snapshot.hasError) {
                  return SnackBar(
                      backgroundColor:
                          StateContainer.of(context).curTheme.failure,
                      content: Text(
                        'Veuillez vous assurer que vous avez internet',
                        style: TextStyle(
                            color: StateContainer.of(context)
                                .curTheme
                                .neutralBackground),
                      ));
                }
                return CircularProgressIndicator();
              }),
          Padding(
              padding: const EdgeInsets.only(top: 20, bottom: 20),
              child: Align(
                  alignment: Alignment.center,
                  child: DottedBorder(
                      color: StateContainer.of(context)
                          .curTheme
                          .lists
                          .title
                          .withOpacity(0.4),
                      borderType: BorderType.RRect,
                      radius: Radius.circular(20),
                      dashPattern: [4, 2],
                      child: Container(
                          width: 270,
                          height: 100,
                          margin: const EdgeInsets.fromLTRB(30, 20, 30, 20),
                          child: Align(
                              alignment: Alignment.topCenter,
                              child: Column(children: [
                                SizedBox(
                                    height: 60,
                                    child: ActionButton.primarySmall(
                                        context: context,
                                        icon: FeatherIcons.plus,
                                        onTap: () async {
                                          final bool updated =
                                              await Navigator.of(context)
                                                      .pushNamed('/add_asset')
                                                  as bool;
                                          if (updated != null && updated) {
                                            setState(() {
                                              // assetsFuture = Future.delayed(
                                              //     const Duration(milliseconds: 500), () => assets);
                                              assetsFuture =
                                                  getCurrentUserAssets(
                                                      sl.get<ApiClient>());
                                            });
                                          }
                                        })),
                                SizedBox(
                                    height: 30,
                                    child: Padding(
                                        padding: const EdgeInsets.only(top: 7),
                                        child: Text('Ajouter un placement',
                                            style: TextStyle(
                                                color:
                                                    StateContainer.of(context)
                                                        .curTheme
                                                        .primary,
                                                //.withOpacity(0.8),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16,
                                                height: 1.5),
                                            textAlign: TextAlign.center))),
                              ]))))))
        ]))));
  }
}
