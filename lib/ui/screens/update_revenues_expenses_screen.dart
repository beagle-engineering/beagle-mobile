import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/models/api/recurrent_transaction.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/services/api/recurrent_transactions.dart';
import 'package:getbeagle/ui/components/form_button.dart';
import 'package:getbeagle/utils/strings.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';

class UpdateRevenuesExpensesScreen extends StatefulWidget {
  UpdateRevenuesExpensesScreen({Key key}) : super(key: key);

  @override
  _UpdateRevenuesExpensesScreenState createState() =>
      _UpdateRevenuesExpensesScreenState();
}

class _UpdateRevenuesExpensesScreenState
    extends State<UpdateRevenuesExpensesScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  FocusNode _focusNode;
  ScrollController _controller;

  Future<List<RecurrentTransaction>> rts;

  String _rtId;
  String _amount;

  @override
  void initState() {
    _controller = ScrollController();
    super.initState();
    rts = getCurrentUserRecurrentTransactions(sl.get<ApiClient>());
    _focusNode = FocusNode();
    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          if (visible) {
            _controller.animateTo(_controller.position.maxScrollExtent,
                curve: Curves.linear, duration: Duration(milliseconds: 500));
          } else {
            _controller.animateTo(_controller.position.minScrollExtent,
                curve: Curves.linear, duration: Duration(milliseconds: 500));
          }
        });
      },
    );
  }

  void _onError(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: StateContainer.of(context).curTheme.failure,
        content: Text(
          message,
          style: TextStyle(
              color: StateContainer.of(context).curTheme.neutralBackground),
        )));
  }

  void _requestFocus() {
    setState(() {
      FocusScope.of(context).requestFocus(_focusNode);
    });
  }

  _update() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }

    if (!_amountById.containsKey(_rtId)) {
      return;
    }

    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _amount = _amount.replaceAll(",", ".");

      RecurrentTransaction newT = RecurrentTransaction(
          label: _amountById[_rtId].label,
          frequency: _amountById[_rtId].frequency,
          amount: (double.parse(_amount) * 100).round(),
          startedAt: DateTime.now(),
          asset: _amountById[_rtId].asset);

      endRecurrentTransaction(
              api: sl.get<ApiClient>(),
              endedAt: DateTime.now(),
              recurrentTransactionId: _rtId)
          .then((e) {
        createRecurrentTransaction(sl.get<ApiClient>(), newT).then((e) {
          Navigator.of(context).pop();
        }).catchError((Object e) {
          _onError('Une erreur s\'est produite. Veuillez réessayer plus tard.');
        });
      }).catchError((Object e) {
        _onError('Une erreur s\'est produite. Veuillez réessayer plus tard.');
      });
    } else {
      _onError('Veuillez ne laisser aucun champs vide');
    }
  }

  Map<String, RecurrentTransaction> _amountById;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: StateContainer.of(context).curTheme.background,
          appBar: PreferredSize(
              preferredSize: Size.fromHeight(67.0),
              child: AppBar(
                  backgroundColor: Colors.transparent,
                  elevation: 0.0,
                  automaticallyImplyLeading: false,
                  title: InkWell(
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      child: Container(
                          margin: const EdgeInsets.only(
                              top: 50, right: 20, bottom: 20),
                          child: Row(children: [
                            Icon(FeatherIcons.chevronLeft,
                                color:
                                    StateContainer.of(context).curTheme.title2,
                                size: 19),
                            Container(
                                padding:
                                    const EdgeInsets.only(left: 12, top: 4),
                                alignment: Alignment.bottomLeft,
                                child: Text('Retour',
                                    style: TextStyle(
                                        fontSize: 14,
                                        height: 1.18,
                                        color: StateContainer.of(context)
                                            .curTheme
                                            .title2))),
                          ])),
                      onTap: () => Navigator.of(context).pop()))),
          body: SafeArea(
              child: SingleChildScrollView(
                  controller: _controller,
                  child: Column(children: <Widget>[
                    FutureBuilder<List<RecurrentTransaction>>(
                        future: rts,
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            _amountById = Map<String, RecurrentTransaction>();
                            for (final rt in snapshot.data) {
                              _amountById[rt.id] = rt;
                            }
                            return Container(
                                height: 200,
                                padding: const EdgeInsets.only(
                                    left: 30, right: 30, top: 30),
                                child: Form(
                                    key: _formKey,
                                    autovalidate: true,
                                    child: ListView(
                                        physics: NeverScrollableScrollPhysics(),
                                        padding:
                                            const EdgeInsets.only(bottom: 5.0),
                                        children: <Widget>[
                                          FormField(
                                            builder: (FormFieldState state) {
                                              return InputDecorator(
                                                decoration: InputDecoration(
                                                  labelText: "Flux",
                                                  labelStyle: TextStyle(
                                                      color: StateContainer.of(
                                                              context)
                                                          .curTheme
                                                          .title2),
                                                  focusedBorder:
                                                      UnderlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color:
                                                            StateContainer.of(
                                                                    context)
                                                                .curTheme
                                                                .primary),
                                                  ),
                                                  enabledBorder:
                                                      UnderlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color:
                                                            StateContainer.of(
                                                                    context)
                                                                .curTheme
                                                                .title2),
                                                  ),
                                                ),
                                                child:
                                                    DropdownButtonHideUnderline(
                                                  child: DropdownButton(
                                                    value: _rtId,
                                                    isDense: true,
                                                    onChanged:
                                                        (String newValue) {
                                                      setState(() {
                                                        _rtId = newValue;

                                                        state.didChange(
                                                            newValue);
                                                      });
                                                    },
                                                    items: snapshot.data.map(
                                                        (RecurrentTransaction
                                                            value) {
                                                      return DropdownMenuItem(
                                                        value: value.id,
                                                        child:
                                                            Text(value.label),
                                                      );
                                                    }).toList(),
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                          SizedBox(
                                            height: 30,
                                          ),
                                          TextFormField(
                                            initialValue: _rtId != null &&
                                                    _rtId.isNotEmpty
                                                ? formatAmountInCts(
                                                    _amountById[_rtId].amount)
                                                : '',
                                            focusNode: _focusNode,
                                            onTap: _requestFocus,
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(
                                              labelText: 'Montant',
                                              labelStyle: TextStyle(
                                                color: _focusNode.hasFocus
                                                    ? StateContainer.of(context)
                                                        .curTheme
                                                        .primary
                                                    : StateContainer.of(context)
                                                        .curTheme
                                                        .title2,
                                              ),
                                              focusedBorder:
                                                  UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: StateContainer.of(
                                                            context)
                                                        .curTheme
                                                        .primary),
                                              ),
                                              enabledBorder:
                                                  UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: StateContainer.of(
                                                            context)
                                                        .curTheme
                                                        .title2),
                                              ),
                                            ),
                                            validator: (input) => null,
                                            onSaved: (input) => input.isNotEmpty
                                                ? _amount = input
                                                : _amount = '0',
                                            obscureText: false,
                                            inputFormatters: [
                                              //WhitelistingTextInputFormatter.digitsOnly,
                                              WhitelistingTextInputFormatter(RegExp(
                                                  '^\$|^(0|([1-9][0-9]{0,}))((\\.|\\,)[0-9]{0,})?\$'))
                                            ],
                                          )
                                        ])));
                          } else if (snapshot.hasError) {
                            return Text("${snapshot.error}");
                          }
                          return CircularProgressIndicator();
                        }),
                    SizedBox(
                      height: 60,
                    ),
                    Container(
                      margin: const EdgeInsets.only(bottom: 150),
                      child: FormButton.primaryDefault(
                        context: context,
                        heroTag: 'update_recurrent_transaction',
                        text: 'Mettre à jour',
                        onPressed: _update,
                      ),
                    ),
                  ]))),
        ));
  }
}
