import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';

import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/services/api/user.dart';
import 'package:getbeagle/localization.dart';
import 'package:getbeagle/ui/components/form_button.dart';
import 'package:getbeagle/ui/components/form_text_field.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';

class UpdatePasswordScreen extends StatefulWidget {
  @override
  _UpdatePasswordScreenState createState() => _UpdatePasswordScreenState();
}

class _UpdatePasswordScreenState extends State<UpdatePasswordScreen> {
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  String _old, _new, _newConfirmed;

  ScrollController _controller;
  double _bottomPadding = 200;
  @override
  void initState() {
    _controller = ScrollController();
    super.initState();

    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          if (visible) {
            _controller.animateTo(_controller.position.maxScrollExtent,
                curve: Curves.linear, duration: Duration(milliseconds: 500));
          } else {
            _controller.animateTo(_controller.position.minScrollExtent,
                curve: Curves.linear, duration: Duration(milliseconds: 500));
          }
        });
      },
    );
  }

  _submit() {
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    print(AppLocalization.currentLocale);
    AppLocalization.load(Locale("fr", "FR"));
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      updatePassword(sl.get<ApiClient>(), _old, _new, _newConfirmed)
          .then((e) async {
        if (e == null) {
          await sl.get<ApiClient>().deleteJWT();
          await Navigator.of(context).pushNamedAndRemoveUntil(
              '/login', (Route<dynamic> route) => false);
        } else {
          String _message = '';
          e.errors.forEach((key, value) {
            if (key == 'CurrentPassword') {
              _message +=
                  'Veuillez vous assurer que votre ancien mot de passe est correct.\n';
            } else if (key == 'NewPassword') {
              _message +=
                  'Veuillez vous assurer que votre nouveau mot de passe match sa confirmation.\n';
            } else if (key == 'Password') {
              _message +=
                  'Veuillez vous assurer que votre nouveau mot de passe fait 8 charactères ou plus\nqu\'il possède au moins:un chiffre, une lettre minuscule, une lettre majuscule, un symbole.\n';
            }
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
              backgroundColor: StateContainer.of(context).curTheme.failure,
              content: Text(
                _message,
                style: TextStyle(
                    color:
                        StateContainer.of(context).curTheme.neutralBackground),
              )));
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Scaffold(
          appBar: PreferredSize(
              preferredSize: Size.fromHeight(67.0),
              child: AppBar(
                  backgroundColor: Colors.transparent,
                  elevation: 0.0,
                  automaticallyImplyLeading: false,
                  title: InkWell(
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      child: Container(
                          margin: const EdgeInsets.only(
                              top: 50, right: 20, bottom: 20),
                          child: Row(children: [
                            Icon(FeatherIcons.chevronLeft,
                                color:
                                    StateContainer.of(context).curTheme.title2,
                                size: 19),
                            Container(
                                padding:
                                    const EdgeInsets.only(left: 12, top: 4),
                                alignment: Alignment.bottomLeft,
                                child: Text('Retour',
                                    style: TextStyle(
                                        fontSize: 14,
                                        height: 1.18,
                                        color: StateContainer.of(context)
                                            .curTheme
                                            .title2))),
                          ])),
                      onTap: () => Navigator.of(context).pop()))),
          key: _scaffoldKey,
          backgroundColor: StateContainer.of(context).curTheme.background,
          body: SafeArea(
              child: Scrollbar(
                  child: SingleChildScrollView(
            controller: _controller,
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 50, top: 30.0),
                    child: Container(
                        width: double.infinity,
                        margin: const EdgeInsets.only(left: 20),
                        child: StyledFocusForm2(
                          validator: (input) => input.isEmpty
                              ? 'Mot de passe actuel requis'
                              : null,
                          onSaved: (input) => _old = input,
                          label: 'Mot de passe actuel',
                          obscureText: true,
                        )),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 50, top: 30.0),
                    child: Container(
                        width: double.infinity,
                        margin: const EdgeInsets.only(left: 20),
                        child: StyledFocusForm2(
                          validator: (input) => input.isEmpty
                              ? 'Nouveau mot de passe requis'
                              : null,
                          onSaved: (input) => _new = input,
                          label: 'Nouveau mot de passe',
                          obscureText: true,
                        )),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 50, top: 30.0),
                    child: Container(
                        width: double.infinity,
                        margin: const EdgeInsets.only(left: 20),
                        child: StyledFocusForm2(
                          validator: (input) =>
                              input.isEmpty ? 'Confirmation requise' : null,
                          onSaved: (input) => _newConfirmed = input,
                          label: 'Confirmez le mot de passe',
                          obscureText: true,
                        )),
                  ),
                  //SizedBox(height: 80.0),
                  Container(
                    margin: const EdgeInsets.only(top: 80),
                    child: FormButton.primaryDefault(
                        context: context,
                        heroTag: 'update_pass',
                        text: 'Mettre à jour',
                        onPressed: _submit),
                  ),
                  SizedBox(height: _bottomPadding),
                ],
              ),
            ),
          ))),
        ));
  }
}
