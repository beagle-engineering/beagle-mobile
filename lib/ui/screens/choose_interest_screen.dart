import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/models/api/ineterest.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/services/api/interest.dart';
import 'package:getbeagle/ui/components/form_button.dart';

class ChooseInterestScreen extends StatefulWidget {
  ChooseInterestScreen({Key key, this.firstConnection = false})
      : super(key: key);

  final bool firstConnection;

  @override
  _ChooseInterestScreenState createState() => _ChooseInterestScreenState();
}

class _ChooseInterestScreenState extends State<ChooseInterestScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<List<Interest>> interests;
  Set<String> selectedInterests = Set();

  @override
  void initState() {
    super.initState();
    setState(() {
      selectedInterests = Set();
    });

    interests = getInterests(sl.get<ApiClient>());
  }

  void _onError(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: StateContainer.of(context).curTheme.failure,
        content: Text(
          message,
          style: TextStyle(
              color: StateContainer.of(context).curTheme.neutralBackground),
        )));
  }

  _submit() {
    if (selectedInterests.isEmpty) {
      _onError('Sélectionne au moins un sujet qui t\'intéresse.');
      return;
    }
    editInterests(sl.get<ApiClient>(), selectedInterests.toList())
        .then((e) async {
      if (widget.firstConnection) {
        await Navigator.of(context)
            .pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
      } else {
        await Navigator.of(context).pop(true);
      }
    }).catchError((Object e) async {
      _onError('Il y a eu un problème, réessayer plus tard.');
      if (widget.firstConnection) {
        await Navigator.of(context)
            .pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
      } else {
        await Navigator.of(context).pop(true);
      }
    });
  }

  _selectOnDisplay(List<Interest> interests) {
    for (final i in interests) {
      if (i.isChosen) {
        selectedInterests.add(i.id);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: StateContainer.of(context).curTheme.background,
        appBar: !widget.firstConnection
            ? PreferredSize(
                preferredSize: Size.fromHeight(67.0),
                child: AppBar(
                    backgroundColor: Colors.transparent,
                    elevation: 0.0,
                    automaticallyImplyLeading: false,
                    title: InkWell(
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        child: Container(
                            margin: const EdgeInsets.only(
                                top: 50, right: 20, bottom: 20),
                            child: Row(children: [
                              Icon(FeatherIcons.chevronLeft,
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .title2,
                                  size: 19),
                              Container(
                                  padding:
                                      const EdgeInsets.only(left: 12, top: 4),
                                  alignment: Alignment.bottomLeft,
                                  child: Text('Retour',
                                      style: TextStyle(
                                          fontSize: 14,
                                          height: 1.18,
                                          color: StateContainer.of(context)
                                              .curTheme
                                              .title2))),
                            ])),
                        onTap: () => Navigator.of(context).pop())))
            : PreferredSize(
                preferredSize: Size.fromHeight(0.0),
                child: Container(),
              ),
        body: Scrollbar(
          child: SingleChildScrollView(
              child: Column(children: <Widget>[
            widget.firstConnection
                ? Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.only(top: 85),
                    child: SvgPicture.asset('assets/images/named_blue_logo.svg',
                        width: 138, height: 48))
                : Container(margin: const EdgeInsets.only(top: 0)),
            Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.only(top: 24),
                child: Text(
                  '📚 Apprends en fonction de tes objectifs.\nQuels sont les sujets qui t\'intéressent?',
                  style: TextStyle(
                      color: StateContainer.of(context).curTheme.title2,
                      fontSize: 18,
                      height: 1.33,
                      fontWeight: FontWeight.w600),
                  textAlign: TextAlign.center,
                )),
            FutureBuilder<List<Interest>>(
                future: interests,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    _selectOnDisplay(snapshot.data);
                    return Padding(
                        padding: const EdgeInsets.all(16),
                        child: Wrap(
                            runAlignment: WrapAlignment.center,
                            alignment: WrapAlignment.center,
                            spacing: 16.0, // gap between adjacent chips
                            runSpacing: 16.0, // gap between lines
                            children: snapshot.data
                                .asMap()
                                .entries
                                .map((interest) => ChoiceChip(
                                      backgroundColor:
                                          StateContainer.of(context)
                                              .curTheme
                                              .background,
                                      shape: StadiumBorder(
                                          side: BorderSide(
                                              color: StateContainer.of(context)
                                                  .curTheme
                                                  .primary)),
                                      label: Text(interest.value.title,
                                          style: TextStyle(
                                              color: interest.value.isChosen
                                                  ? StateContainer.of(context)
                                                      .curTheme
                                                      .neutralBackground
                                                  : StateContainer.of(context)
                                                      .curTheme
                                                      .primary,
                                              fontSize: 16,
                                              height: 1.2,
                                              fontWeight: FontWeight.w500)),
                                      selected: interest.value.isChosen,
                                      onSelected: (bool selected) {
                                        setState(() {
                                          if (selected) {
                                            interest.value.isChosen = true;
                                            selectedInterests
                                                .add(interest.value.id);
                                          } else {
                                            interest.value.isChosen = false;
                                            selectedInterests
                                                .remove(interest.value.id);
                                          }
                                        });
                                      },
                                      selectedColor: StateContainer.of(context)
                                          .curTheme
                                          .primary,
                                    ))
                                .toList()));
                  } else if (snapshot.hasError) {
                    return Text("${snapshot.error}");
                  }
                  return CircularProgressIndicator();
                }),
            SizedBox(height: 30.0),
            FormButton.primaryDefault(
                context: context,
                heroTag: 'submit_interest',
                text: 'C\'est parti !',
                onPressed: _submit),
            SizedBox(height: 30.0),
          ])),
        ));
  }
}
