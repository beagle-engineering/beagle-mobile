import 'dart:io';

import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/models/api/user.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/services/api/user.dart';
import 'package:getbeagle/ui/utils/action_snackbar.dart';
import 'package:getbeagle/utils/sharedprefsutil.dart';
import 'package:getbeagle/utils/strings.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingsScreen extends StatefulWidget {
  static final String id = 'settings_screen';

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<User> user;
  String _email = '';

  @override
  void initState() {
    super.initState();
    user = getCurrentUser(api: sl.get<ApiClient>());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: StateContainer.of(context).curTheme.background,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(67.0),
            child: AppBar(
                backgroundColor: Colors.transparent,
                elevation: 0.0,
                automaticallyImplyLeading: false,
                title: InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    child: Container(
                        margin: const EdgeInsets.only(
                            top: 50, right: 20, bottom: 20),
                        child: Row(children: [
                          Icon(FeatherIcons.chevronLeft,
                              color: StateContainer.of(context).curTheme.title2,
                              size: 19),
                          Container(
                              padding: const EdgeInsets.only(left: 12, top: 4),
                              alignment: Alignment.bottomLeft,
                              child: Text('Retour',
                                  style: TextStyle(
                                      fontSize: 14,
                                      height: 1.18,
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .title2))),
                        ])),
                    onTap: () => Navigator.of(context).pop()))),
        body: SafeArea(
            child: Scrollbar(
                child: SingleChildScrollView(
                    child: Column(
          children: <Widget>[
            Container(
                height: 90,
                width: double.infinity,
                margin: const EdgeInsets.only(top: 10),
                decoration: BoxDecoration(
                  color: StateContainer.of(context).curTheme.neutralBackground,
                ),
                child: FutureBuilder<User>(
                    future: user,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        _email = snapshot.data.email;
                        return Row(children: [
                          Padding(
                              padding: const EdgeInsets.only(left: 17),
                              child: SizedBox(
                                  width: 60,
                                  height: 60,
                                  child: Container(
                                      padding: const EdgeInsets.all(2),
                                      decoration: BoxDecoration(
                                          color: StateContainer.of(context)
                                              .curTheme
                                              .settingMenu,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(30))),
                                      child: Container(
                                          decoration: BoxDecoration(
                                              color: StateContainer.of(context)
                                                  .curTheme
                                                  .neutralBackground,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(30))),
                                          child: Center(
                                              child: Icon(
                                            FeatherIcons.user,
                                            color: StateContainer.of(context)
                                                .curTheme
                                                .settingMenu,
                                            size: 40,
                                          )))))),
                          Container(
                            //alignment: Alignment.center,
                            margin: const EdgeInsets.only(
                                left: 16, top: 15, bottom: 16),
                            child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  snapshot.data.firstName != null &&
                                          snapshot.data.lastName != null
                                      ? Text(
                                          '${capitalize(snapshot.data.firstName)} ${capitalize(snapshot.data.lastName)}',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            color: StateContainer.of(context)
                                                .curTheme
                                                .settingMenu,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            height: 1.18,
                                          ))
                                      : Container(height: 13),
                                  snapshot.data.phoneNumber != null
                                      ? Text('${snapshot.data.phoneNumber}',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            color: StateContainer.of(context)
                                                .curTheme
                                                .settingMenu,
                                            fontSize: 14,
                                            height: 1.21,
                                          ))
                                      : Container(),
                                  Text('${snapshot.data.email}',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        color: StateContainer.of(context)
                                            .curTheme
                                            .settingMenu,
                                        fontSize: 14,
                                        height: 1.21,
                                      ))
                                ]),
                          )
                        ]);
                      } else if (snapshot.hasError) {
                        return Text("${snapshot.error}");
                      }
                      return CircularProgressIndicator();
                    })),
            SizedBox(height: 25),
            InkWell(
                onTap: () async {
                  String url = 'https://getbeagle.typeform.com/to/LvLXA7RU';
                  //final url =
                  //    'mailto:contact@getbeagle.app?subject=Feedbacks%20${platform}%20app';
                  //final url = 'http://google.com';
                  if (await canLaunch(url)) {
                    await launch(url);
                  } else {
                    throw 'Could not launch $url';
                  }
                },
                splashColor: StateContainer.of(context)
                    .curTheme
                    .settingMenu
                    .withOpacity(0.12),
                child: Container(
                    height: 70,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color:
                          StateContainer.of(context).curTheme.neutralBackground,
                    ),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              margin: const EdgeInsets.only(left: 16),
                              alignment: Alignment.center,
                              width: 24,
                              height: 24,
                              padding: const EdgeInsets.all(0),
                              decoration: BoxDecoration(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .success,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4))),
                              child: Center(
                                  child: Icon(FeatherIcons.heart,
                                      size: 12,
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .neutralBackground))),
                          Container(
                              //alignment: Alignment.center,
                              margin: const EdgeInsets.only(left: 16),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Aide moi à améliorer l\'app',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .settingMenu,
                                  fontSize: 18,
                                  height: 1.44,
                                ),
                              )),
                          Container(
                              //alignment: Alignment.center,
                              margin:
                                  const EdgeInsets.only(left: 60, right: 16),
                              child: Icon(FeatherIcons.chevronRight,
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .settingMenu))
                        ]))),
            Divider(
              height: 0.2,
              color: StateContainer.of(context).curTheme.title2,
              indent: 20,
              endIndent: 20,
            ),
            InkWell(
                onTap: () async {
                  String url = 'https://getbeagle.typeform.com/to/RtqQPDtk';
                  //final url =
                  //    'mailto:contact@getbeagle.app?subject=Feedbacks%20${platform}%20app';
                  //final url = 'http://google.com';
                  if (await canLaunch(url)) {
                    await launch(url);
                  } else {
                    throw 'Could not launch $url';
                  }
                },
                splashColor: StateContainer.of(context)
                    .curTheme
                    .settingMenu
                    .withOpacity(0.12),
                child: Container(
                    height: 70,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color:
                          StateContainer.of(context).curTheme.neutralBackground,
                    ),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              margin: const EdgeInsets.only(left: 16),
                              alignment: Alignment.center,
                              width: 24,
                              height: 24,
                              padding: const EdgeInsets.all(0),
                              decoration: BoxDecoration(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .warning,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4))),
                              child: Center(
                                  child: Icon(FeatherIcons.star,
                                      size: 12,
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .neutralBackground))),
                          Container(
                              //alignment: Alignment.center,
                              margin: const EdgeInsets.only(left: 16),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Envie de suggérer du contenu ?',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .settingMenu,
                                  fontSize: 18,
                                  height: 1.44,
                                ),
                              )),
                          Container(
                              //alignment: Alignment.center,
                              margin:
                                  const EdgeInsets.only(left: 11, right: 16),
                              child: Icon(FeatherIcons.chevronRight,
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .settingMenu))
                        ]))),
            Divider(
              height: 0.2,
              color: StateContainer.of(context).curTheme.title2,
              indent: 20,
              endIndent: 20,
            ),
            InkWell(
                onTap: () async {
                  bool mess = true;
                  bool shouldDo = false;
                  await showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                            backgroundColor: StateContainer.of(context)
                                .curTheme
                                .neutralBackground,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12.0),
                            ),
                            //actionsPadding: const EdgeInsets.only(right: 50),
                            //buttonPadding: const EdgeInsets.only(left: 50),
                            contentPadding: const EdgeInsets.only(
                                left: 32, right: 32, top: 24, bottom: 24),
                            content: ButtonBar(
                                mainAxisSize: MainAxisSize.max,
                                alignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  IconButton(
                                    onPressed: () {
                                      mess = true;
                                      shouldDo = true;
                                      Navigator.of(context).pop();
                                    },
                                    icon: Icon(FeatherIcons.messageCircle,
                                        color: StateContainer.of(context)
                                            .curTheme
                                            .settingMenu),
                                    iconSize: 50,
                                  ),
                                  IconButton(
                                    onPressed: () {
                                      mess = false;
                                      shouldDo = true;
                                      Navigator.of(context).pop();
                                    },
                                    icon: Icon(FeatherIcons.mail,
                                        color: StateContainer.of(context)
                                            .curTheme
                                            .settingMenu),
                                    iconSize: 50,
                                  )
                                ]),
                          ));

                  String url = '';
                  if (mess) {
                    url = 'http://m.me/111046893637459';
                  } else {
                    final platform = Platform.isAndroid ? 'Android' : 'iOS';
                    final Uri params = Uri(
                        scheme: 'mailto',
                        path: 'contact@getbeagle.app',
                        query: 'subject=Feedback ${platform} app');
                    url = params.toString();
                  }

                  //final url =
                  //    'mailto:contact@getbeagle.app?subject=Feedbacks%20${platform}%20app';
                  //final url = 'http://google.com';
                  if (shouldDo) {
                    if (await canLaunch(url)) {
                      await launch(url);
                    } else {
                      neutralSnackbar(
                          'Impossible d\'accéder à ton client mail.\ncontact@getbeagle.app',
                          _scaffoldKey,
                          context,
                          Duration(milliseconds: 4000));
                    }
                  }
                },
                splashColor: StateContainer.of(context)
                    .curTheme
                    .settingMenu
                    .withOpacity(0.12),
                child: Container(
                    height: 70,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color:
                          StateContainer.of(context).curTheme.neutralBackground,
                    ),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              margin: const EdgeInsets.only(left: 16),
                              alignment: Alignment.center,
                              width: 24,
                              height: 24,
                              padding: const EdgeInsets.all(0),
                              decoration: BoxDecoration(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .success,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4))),
                              child: Center(
                                  child: Icon(FeatherIcons.mail,
                                      size: 12,
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .neutralBackground))),
                          Container(
                              //alignment: Alignment.center,
                              margin: const EdgeInsets.only(left: 16),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Contacter Beagle',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .settingMenu,
                                  fontSize: 18,
                                  height: 1.44,
                                ),
                              )),
                          Container(
                              //alignment: Alignment.center,
                              margin:
                                  const EdgeInsets.only(left: 130, right: 16),
                              child: Icon(FeatherIcons.chevronRight,
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .settingMenu))
                        ]))),
            SizedBox(height: 25),
            InkWell(
                onTap: () async {
                  await Navigator.of(context)
                      .pushNamed('/update_expenses_revenues');
                },
                splashColor: StateContainer.of(context)
                    .curTheme
                    .settingMenu
                    .withOpacity(0.12),
                child: Container(
                    height: 70,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color:
                          StateContainer.of(context).curTheme.neutralBackground,
                    ),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              margin: const EdgeInsets.only(left: 16),
                              alignment: Alignment.center,
                              width: 24,
                              height: 24,
                              padding: const EdgeInsets.all(0),
                              decoration: BoxDecoration(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .warning,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4))),
                              child: Center(
                                  child: Icon(Icons.euro_symbol,
                                      size: 12,
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .neutralBackground))),
                          Container(
                              //alignment: Alignment.center,
                              margin: const EdgeInsets.only(left: 16),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Mettre à jour tes flux financiers',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .settingMenu,
                                  fontSize: 18,
                                  height: 1.44,
                                ),
                              )),
                          Container(
                              margin:
                                  const EdgeInsets.only(left: 15, right: 16),
                              child: Icon(FeatherIcons.chevronRight,
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .settingMenu))
                        ]))),
            SizedBox(height: 25),
            InkWell(
                onTap: () async {
                  await Navigator.of(context).pushNamed('/update_password');
                },
                splashColor: StateContainer.of(context)
                    .curTheme
                    .settingMenu
                    .withOpacity(0.12),
                child: Container(
                    height: 70,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color:
                          StateContainer.of(context).curTheme.neutralBackground,
                    ),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              margin: const EdgeInsets.only(left: 16),
                              alignment: Alignment.center,
                              width: 24,
                              height: 24,
                              padding: const EdgeInsets.all(0),
                              decoration: BoxDecoration(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .warning,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4))),
                              child: Center(
                                  child: Icon(FeatherIcons.lock,
                                      size: 12,
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .neutralBackground))),
                          Container(
                              //alignment: Alignment.center,
                              margin: const EdgeInsets.only(left: 16),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Changer de mot de passe',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .settingMenu,
                                  fontSize: 18,
                                  height: 1.44,
                                ),
                              )),
                          Container(
                              margin:
                                  const EdgeInsets.only(left: 60, right: 16),
                              child: Icon(FeatherIcons.chevronRight,
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .settingMenu))
                        ]))),
            Divider(
              height: 0.2,
              color: StateContainer.of(context).curTheme.title2,
              indent: 20,
              endIndent: 20,
            ),
            InkWell(
                onTap: () async {
                  await sl.get<ApiClient>().deleteJWT();
                  await Navigator.of(context).pushNamedAndRemoveUntil(
                      '/login', (Route<dynamic> route) => false);
                },
                splashColor: StateContainer.of(context)
                    .curTheme
                    .settingMenu
                    .withOpacity(0.12),
                child: Container(
                    height: 70,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color:
                          StateContainer.of(context).curTheme.neutralBackground,
                    ),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              margin: const EdgeInsets.only(left: 16),
                              alignment: Alignment.center,
                              width: 24,
                              height: 24,
                              padding: const EdgeInsets.all(0),
                              decoration: BoxDecoration(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .classicPink,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4))),
                              child: Center(
                                  child: Icon(FeatherIcons.logOut,
                                      size: 12,
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .neutralBackground))),
                          Container(
                              //alignment: Alignment.center,
                              margin: const EdgeInsets.only(left: 16),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Se déconnecter',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .settingMenu,
                                  fontSize: 18,
                                  height: 1.44,
                                ),
                              )),
                          Container(
                              //alignment: Alignment.center,
                              margin:
                                  const EdgeInsets.only(left: 140, right: 16),
                              child: Icon(FeatherIcons.chevronRight,
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .settingMenu))
                        ]))),
            Divider(
              height: 0.2,
              color: StateContainer.of(context).curTheme.title2,
              indent: 20,
              endIndent: 20,
            ),
            InkWell(
                onTap: () async {
                  bool shouldClose = false;
                  await showCupertinoDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                            backgroundColor: StateContainer.of(context)
                                .curTheme
                                .neutralBackground,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12.0),
                            ),
                            title: Text(
                              'Supprimer mon compte?',
                              style: TextStyle(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .title),
                            ),
                            actions: <Widget>[
                              CupertinoButton(
                                child: Text(
                                  'Supprimer',
                                  style: TextStyle(
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .failure),
                                ),
                                onPressed: () {
                                  shouldClose = true;
                                  Navigator.of(context).pop();
                                },
                              ),
                              CupertinoButton(
                                child: Text(
                                  'Annuler',
                                  style: TextStyle(
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .primary),
                                ),
                                onPressed: () {
                                  shouldClose = false;
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          ));

                  if (shouldClose) {
                    await deleteUser(api: sl.get<ApiClient>())
                        .then((Object e) async {
                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                          backgroundColor:
                              StateContainer.of(context).curTheme.success,
                          content: Text(
                            'Compte supprimé avec succès.',
                            style: TextStyle(
                                color: StateContainer.of(context)
                                    .curTheme
                                    .neutralBackground),
                          )));
                      await sl
                          .get<SharedPrefsUtil>()
                          .resetUserFirstLaunch(_email);
                      await sl.get<ApiClient>().deleteJWT();
                      await sl.get<ApiClient>().deletePlan();
                      await Navigator.of(context).pushNamedAndRemoveUntil(
                          '/signup', (Route<dynamic> route) => false);
                    }).catchError((Object e) {
                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                          backgroundColor:
                              StateContainer.of(context).curTheme.failure,
                          content: Text(
                            'Un problème est survenu, veuillez réessayer plus tard.',
                            style: TextStyle(
                                color: StateContainer.of(context)
                                    .curTheme
                                    .neutralBackground),
                          )));
                    });
                  }
                },
                splashColor: StateContainer.of(context)
                    .curTheme
                    .settingMenu
                    .withOpacity(0.12),
                child: Container(
                    height: 70,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color:
                          StateContainer.of(context).curTheme.neutralBackground,
                    ),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              margin: const EdgeInsets.only(left: 16),
                              alignment: Alignment.center,
                              width: 24,
                              height: 24,
                              padding: const EdgeInsets.all(0),
                              decoration: BoxDecoration(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .classicPink,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4))),
                              child: Center(
                                  child: Icon(FeatherIcons.trash2,
                                      size: 12,
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .neutralBackground))),
                          Container(
                              //alignment: Alignment.center,
                              margin: const EdgeInsets.only(left: 16),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Supprimer le compte',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .failure,
                                  fontSize: 18,
                                  height: 1.44,
                                ),
                              )),
                          Container(
                              //alignment: Alignment.center,
                              margin:
                                  const EdgeInsets.only(left: 90, right: 16),
                              child: Icon(FeatherIcons.chevronRight,
                                  color: StateContainer.of(context)
                                      .curTheme
                                      .settingMenu))
                        ]))),
          ],
        )))));
  }
}
