import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:getbeagle/ui/utils/action_snackbar.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/models/api/knowledge.dart';
import 'package:getbeagle/models/screens/knowledge_content.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/services/api/knowledge.dart';
import 'package:getbeagle/services/firebase/analytics_service.dart';
import 'package:getbeagle/ui/components/content_card.dart';
import 'package:getbeagle/ui/components/section_title.dart';
import 'package:getbeagle/ui/utils/custom_page_scroll.dart';

class KnowledgeContentScreen extends StatefulWidget {
  static String route = '/knowledge_content';

  final KnowledgeContent fullContent;
  final List<KnowledgeContent> nextSteps;
  final Color moreColor;
  final bool isStarred;

  KnowledgeContentScreen(
      {Key key,
      @required this.fullContent,
      @required this.nextSteps,
      this.isStarred,
      this.moreColor})
      : super(key: key);

  @override
  _KnowledgeContentScreenState createState() => _KnowledgeContentScreenState();
}

class _KnowledgeContentScreenState extends State<KnowledgeContentScreen> {
  _KnowledgeContentScreenState();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<KnowledgeContent> knowledgeContent;
  var _controller = ScrollController();
  bool _titleInAppBar = false;
  bool _titleInPage = true;
  bool _isStarred;
  bool _popNavigatorWithUpdate = false;

  @override
  void initState() {
    super.initState();
    knowledgeContent =
        getContentById(sl.get<ApiClient>(), widget.fullContent.id);

    sl
        .get<AnalyticsService>()
        .logContentView(widget.fullContent.id, widget.fullContent.title);
    readContent(sl.get<ApiClient>(), widget.fullContent.id);

    _controller.addListener(() {
      if (_controller.offset <= 80 && !_controller.position.outOfRange) {
        setState(() {
          _titleInAppBar = false;
          _titleInPage = true;
        });
      }

      if (_controller.offset >= 80 && !_controller.position.outOfRange) {
        setState(() {
          _titleInAppBar = true;
          _titleInPage = false;
          ;
        });
      }
    });
    _isStarred = widget.isStarred;
  }

  void _pinContent() {
    if (_isStarred) {
      unpinContent(sl.get<ApiClient>(), [widget.fullContent.id]).then((e) {
        _popNavigatorWithUpdate = true;
        setState(() {
          _isStarred = !_isStarred;
        });
        neutralSnackbar('Contenu retiré de la liste des contenus sauvegardés !',
            _scaffoldKey, context, Duration(milliseconds: 3000));
      }).catchError((Object e) {
        onErrorSnackbar(
            'Une erreur s\'est produite. Veuillez réessayer plus tard.',
            _scaffoldKey,
            context,
            Duration(milliseconds: 2000));
      });
    } else {
      pinContent(sl.get<ApiClient>(), widget.fullContent.id).then((e) {
        _popNavigatorWithUpdate = true;
        setState(() {
          _isStarred = !_isStarred;
        });
        onSuccessSnackbar('Contenu sauvegardé !', _scaffoldKey, context,
            Duration(milliseconds: 3000));
      }).catchError((Object e) {
        onErrorSnackbar(
            'Une erreur s\'est produite. Veuillez réessayer plus tard.',
            _scaffoldKey,
            context,
            Duration(milliseconds: 2000));
      });
    }
  }

  int _index = 0;
  Completer<WebViewController> _webviewcontroller;
  String contentBase64;
  @override
  Widget build(BuildContext context) {
    // Use the Todo to create the UI.
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: StateContainer.of(context).curTheme.background,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(67.0),
            child: AppBar(
              title: Container(
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.only(top: 35),
                  child: AnimatedOpacity(
                      opacity: _titleInAppBar ? 1.0 : 0.0,
                      duration: Duration(milliseconds: 50),
                      child: Text(
                        widget.fullContent.title,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: StateContainer.of(context).curTheme.title,
                            fontSize: 14),
                      ))),
              backgroundColor: Colors.transparent,
              elevation: 0.0,
              automaticallyImplyLeading: true,
              leading: IconButton(
                padding: const EdgeInsets.only(top: 30, left: 10),
                icon: Icon(FeatherIcons.arrowLeft),
                color: StateContainer.of(context).curTheme.title,
                onPressed: () {
                  Navigator.of(context).pop(_popNavigatorWithUpdate);
                },
                tooltip: 'retour',
              ),
              actions: <Widget>[
                GestureDetector(
                    onTap: _pinContent,
                    child: Padding(
                      padding: EdgeInsets.only(right: 24.0, top: 30),
                      child: Icon(
                          _isStarred ? Icons.bookmark : FeatherIcons.bookmark,
                          color: _isStarred
                              ? StateContainer.of(context).curTheme.warning
                              : StateContainer.of(context).curTheme.title),
                    )),
              ],
            )),
        body: Scrollbar(
          child: SingleChildScrollView(
            controller: _controller,
            child: Column(
              children: <Widget>[
                Container(
                    alignment: Alignment.center,
                    //padding: const EdgeInsets.only(top: 20),
                    margin: const EdgeInsets.fromLTRB(24, 24, 24, 0),
                    child: AnimatedOpacity(
                        // If the widget is visible, animate to 0.0 (invisible).
                        // If the widget is hidden, animate to 1.0 (fully visible).
                        opacity: _titleInPage ? 1.0 : 0.0,
                        duration: Duration(milliseconds: 50),
                        child: Text(
                          //_pageTitle == 'first'
                          widget.fullContent.title,
                          //: _pageTitle,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: StateContainer.of(context).curTheme.title,
                              fontSize: 42,
                              height: 1.2),
                          textAlign: TextAlign.center,
                        ))),
                FutureBuilder<KnowledgeContent>(
                    future: knowledgeContent,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        String c = snapshot.data.content;
                        String iframe = '';

                        if (Platform.isIOS) {
                          const start = "<iframe";
                          const end = "</iframe>";

                          final startIndex = c.indexOf(start);
                          final endIndex =
                              c.indexOf(end, startIndex + start.length);

                          if (startIndex > -1 && endIndex > -1) {
                            iframe = c.substring(
                                startIndex + start.length, endIndex);
                            String html = """<!DOCTYPE html>
          <html>
            <head>
            <style>
            body {
              overflow: hidden; 
            }
        .embed-youtube {
            position: relative;
            padding-bottom: 56.25%; 
            padding-top: 0px;
            height: 0;
            overflow: hidden;
        }

        .embed-youtube iframe,
        .embed-youtube object,
        .embed-youtube embed {
            border: 0;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
        </style>

        <meta charset="UTF-8">
         <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
          <meta http-equiv="X-UA-Compatible" content="ie=edge">
           </head>
          <body bgcolor="#121212">                                    
        <div class="embed-youtube">
         <iframe
          id="vjs_video_3_Youtube_api"
          style="width:100%;height:100%;top:0;left:0;position:absolute;"
          class="vjs-tech holds-the-iframe"
          frameborder="0"
          allowfullscreen="1"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          webkitallowfullscreen mozallowfullscreen allowfullscreen
          title="Live Tv"
          frameborder="0"
          src="$iframe"
          ></iframe></div>
          </body>                                    
        </html>
  """;
                            _webviewcontroller = Completer<WebViewController>();
                            contentBase64 =
                                base64Encode(const Utf8Encoder().convert(html));
                          }
                        }
                        return Column(children: [
                          Container(
                              alignment: Alignment.center,
                              //padding: const EdgeInsets.only(top: 20),
                              margin: EdgeInsets.fromLTRB(24, 24, 24, 0),
                              child: Html(
                                data: c != null ? c : '',
                                onLinkTap: (url) async {
                                  if (await canLaunch(url)) {
                                    await launch(url);
                                  } else {
                                    throw 'Could not launch ${url}';
                                  }
                                },
                                style: {
                                  "p": Style(
                                    color: //Colors.black,
                                        StateContainer.of(context)
                                            .curTheme
                                            .title,
                                    fontSize: FontSize(16),
                                    fontWeight: FontWeight.w400,
                                    textAlign: TextAlign.left,
                                    letterSpacing: 0.4,
                                  ),
                                  "ul": Style(
                                    color: StateContainer.of(context)
                                        .curTheme
                                        .title,
                                    fontSize: FontSize(16),
                                    fontWeight: FontWeight.w400,
                                    textAlign: TextAlign.left,
                                    letterSpacing: 0.4,
                                  ),
                                  "li": Style(
                                      color: StateContainer.of(context)
                                          .curTheme
                                          .title,
                                      fontSize: FontSize(16),
                                      fontWeight: FontWeight.w400,
                                      textAlign: TextAlign.left,
                                      letterSpacing: 0.4,
                                      padding:
                                          const EdgeInsets.only(bottom: 10)),
                                  "a": Style(
                                    color: StateContainer.of(context)
                                        .curTheme
                                        .primary,
                                    fontSize: FontSize(16),
                                    fontWeight: FontWeight.w400,
                                    textDecoration: TextDecoration.underline,
                                    textAlign: TextAlign.left,
                                    letterSpacing: 0.4,
                                  ),
                                  "strong": Style(
                                    color: StateContainer.of(context)
                                        .curTheme
                                        .title,
                                    fontSize: FontSize(16),
                                    fontWeight: FontWeight.bold,
                                    textAlign: TextAlign.left,
                                    letterSpacing: 0.4,
                                  )
                                },
                              )),
                          iframe.isEmpty
                              ? Container()
                              : Container(
                                  height: 0,
                                  child: WebView(
                                    initialUrl:
                                        'data:text/html;base64,$contentBase64',
                                    javascriptMode: JavascriptMode.unrestricted,
                                    onWebViewCreated:
                                        (WebViewController webViewController) {
                                      _webviewcontroller
                                          .complete(webViewController);
                                    },
                                    onPageStarted: (String url) {
                                      print('Page started loading: $url');
                                    },
                                    onPageFinished: (String url) {
                                      print('Page finished loading: $url');
                                    },
                                    gestureNavigationEnabled: true,
                                  )),
                        ]);
                      } else if (snapshot.hasError) {
                        return Text("${snapshot.error}");
                      }
                      return CircularProgressIndicator();
                    }),
                SectionTitle(
                    title:
                        widget.nextSteps.isNotEmpty ? 'Plus de lecture' : ''),
                //SectionTitle(title: 'Plus de lecture'),
                widget.nextSteps.isNotEmpty
                    ? Container(
                        height: 200,
                        margin: const EdgeInsets.only(bottom: 16),
                        child: ListView.builder(
                            padding: const EdgeInsets.only(left: 10, right: 20),
                            scrollDirection: Axis.horizontal,
                            physics: CustomScrollPhysics(itemDimension: 120),
                            itemCount: widget.nextSteps.length,
                            itemBuilder: (_, i) {
                              return ContentCard(
                                title: i < widget.nextSteps.length
                                    ? widget.nextSteps[i].title
                                    : '',
                                onTap: () {
                                  Navigator.of(context).pushReplacementNamed(
                                    '/knowledge_content',
                                    arguments: KnowledgeContentArgs(
                                      fullContent: widget.nextSteps[i],
                                      nextSteps:
                                          widget.nextSteps.sublist(i + 1),
                                      moreColor: widget.moreColor,
                                      isStarred: false,
                                    ),
                                  );
                                },
                                cardColor: widget.nextSteps[i].read
                                    ? widget.moreColor.withAlpha(150)
                                    : widget.moreColor,
                              );
                            }))
                    : SizedBox(height: 0)
              ],
            ),
          ),
        ));
  }
}
