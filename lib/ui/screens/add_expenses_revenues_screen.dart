import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/models/api/asset.dart';
import 'package:getbeagle/models/api/project.dart';
import 'package:getbeagle/models/api/recurrent_transaction.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/services/api/project.dart';
import 'package:getbeagle/services/api/recurrent_transactions.dart';
import 'package:getbeagle/ui/components/form_button.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:url_launcher/url_launcher.dart';

class AddExpensesRevenues extends StatefulWidget {
  static const route = '/add_expenses_revenues';
  AddExpensesRevenues({Key key, @required this.assetId, this.firstConnection})
      : super(key: key);
  final String assetId;
  final bool firstConnection;

  @override
  _AddExpensesRevenuesState createState() => _AddExpensesRevenuesState();
}

class _AddExpensesRevenuesState extends State<AddExpensesRevenues> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  FocusNode _focusNodeRevs;
  FocusNode _focusNodeMonthlyExps;
  FocusNode _focusNodeAnnualExps;

  ScrollController _controller;
  String _monthlyExpenses;
  String _annualExpenses;
  String _monthlyRevenues;

  @override
  void initState() {
    _controller = ScrollController();
    super.initState();
    _focusNodeRevs = FocusNode();
    _focusNodeMonthlyExps = FocusNode();
    _focusNodeAnnualExps = FocusNode();
    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          if (visible) {
            _controller.animateTo(_controller.position.maxScrollExtent,
                curve: Curves.linear, duration: Duration(milliseconds: 500));
          } else {
            _controller.animateTo(_controller.position.minScrollExtent,
                curve: Curves.linear, duration: Duration(milliseconds: 500));
          }
        });
      },
    );
  }

  void _onError(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: StateContainer.of(context).curTheme.failure,
        content: Text(
          message,
          style: TextStyle(
              color: StateContainer.of(context).curTheme.neutralBackground),
        )));
  }

  void _onSuccess(int monthlyExp, int yearlyExp) async {
    List<Project> projects = await getCurrentUserProjects(sl.get<ApiClient>());
    bool doCreatePreco = true;
    bool doCreateBudget = true;
    for (final p in projects) {
      if (p.label.id == '87d88fc2-486e-4dbe-837b-d5b5dd4f22e0') {
        doCreatePreco = false;
      } else if (p.label.id == '6e04c0a2-9517-4133-a818-a9c398765eb8') {
        doCreateBudget = false;
      }
    }
    if (doCreatePreco) {
      Project prec = Project(
          name: 'Épargne de précaution',
          label: ProjectLabel(id: '87d88fc2-486e-4dbe-837b-d5b5dd4f22e0'),
          endedAt: DateTime.now().add(Duration(days: 30)),
          goal: monthlyExp,
          current: 0,
          startedAt: DateTime.now(),
          active: true);
      await createProject(sl.get<ApiClient>(), prec);
    }
    if (doCreateBudget) {
      Project budget = Project(
          name: 'Budget annuel',
          label: ProjectLabel(id: '6e04c0a2-9517-4133-a818-a9c398765eb8'),
          endedAt: DateTime.now().add(Duration(days: 365)),
          goal: yearlyExp,
          current: 0,
          startedAt: DateTime.now(),
          active: true);

      await createProject(sl.get<ApiClient>(), budget);
    }
  }

  _create() {
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _monthlyExpenses = _monthlyExpenses.replaceAll(",", ".");
      _annualExpenses = _annualExpenses.replaceAll(",", ".");
      _monthlyRevenues = _monthlyRevenues.replaceAll(",", ".");
      RecurrentTransaction monthlyRev = RecurrentTransaction(
          label: 'Revenu mensuel principal',
          frequency: Duration(days: 30),
          amount: (double.parse(_monthlyRevenues) * 100).round(),
          startedAt: DateTime.now(),
          asset: Asset(id: widget.assetId));
      RecurrentTransaction monthlyExp = RecurrentTransaction(
          label: 'Dépenses mensuelles',
          frequency: Duration(days: 365),
          amount: (double.parse(_monthlyExpenses) * 100).round(),
          startedAt: DateTime.now(),
          asset: Asset(id: widget.assetId));
      RecurrentTransaction annualExp = RecurrentTransaction(
          label: 'Dépenses annuelles',
          frequency: Duration(days: 30),
          amount: (double.parse(_annualExpenses) * 100).round(),
          startedAt: DateTime.now(),
          asset: Asset(id: widget.assetId));
      createRecurrentTransaction(sl.get<ApiClient>(), monthlyRev).then((e) {
        createRecurrentTransaction(sl.get<ApiClient>(), monthlyExp).then((e) {
          createRecurrentTransaction(sl.get<ApiClient>(), annualExp)
              .then((e) async {
            await _onSuccess((double.parse(_monthlyExpenses) * 100 * 3).round(),
                (double.parse(_annualExpenses) * 100).round());
            if (widget.firstConnection != null && widget.firstConnection) {
              await Navigator.of(context).pushNamedAndRemoveUntil(
                  '/choose_interests', (Route<dynamic> route) => false,
                  arguments: widget.firstConnection);
            } else {
              await Navigator.of(context).pushNamedAndRemoveUntil(
                  '/home', (Route<dynamic> route) => false);
            }
          }).catchError((Object e) {
            _onError(
                'Une erreur s\'est produite. Veuillez réessayer plus tard.');
          });
        }).catchError((Object e) {
          _onError('Une erreur s\'est produite. Veuillez réessayer plus tard.');
        });
      }).catchError((Object e) {
        _onError('Une erreur s\'est produite. Veuillez réessayer plus tard.');
      });
    } else {
      _onError('Veuillez ne laisser aucun champs vide');
    }
  }

  String introText = '''
  <p>Pour pouvoir t’apporter un minimum d’intelligence</p>
  ''';

  String outroText = '''
  <p>✅ Et voilà, on est tout bon.</p>
  
  <p>Comme disait Churchill 👴🏻 : “Ceci n’est pas la fin, ni le début de la fin, mais c’est peut-être la fin du début !”.</p>
  ''';

  void _requestFocusRevs() {
    setState(() {
      FocusScope.of(context).requestFocus(_focusNodeRevs);
    });
  }

  void _requestFocusMonthlyExps() {
    setState(() {
      FocusScope.of(context).requestFocus(_focusNodeMonthlyExps);
    });
  }

  void _requestFocusAnnualExps() {
    setState(() {
      FocusScope.of(context).requestFocus(_focusNodeAnnualExps);
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: StateContainer.of(context).curTheme.background,
          body: SafeArea(
              child: SingleChildScrollView(
                  controller: _controller,
                  child: Column(children: <Widget>[
                    Container(
                        alignment: Alignment.center,
                        //padding: const EdgeInsets.only(top: 20),
                        margin: const EdgeInsets.fromLTRB(24, 67, 24, 0),
                        child: Text(
                          "Tes flux courants",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: StateContainer.of(context).curTheme.title,
                              fontSize: 42,
                              height: 1.2),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        height: 320,
                        padding:
                            const EdgeInsets.only(left: 24, right: 24, top: 24),
                        child: Form(
                            key: _formKey,
                            autovalidate: true,
                            child: ListView(
                              physics: NeverScrollableScrollPhysics(),
                              padding: const EdgeInsets.only(bottom: 5.0),
                              children: <Widget>[
                                Text('🤑 Ton revenu mensuel',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: _focusNodeRevs.hasFocus
                                          ? StateContainer.of(context)
                                              .curTheme
                                              .primary
                                          : StateContainer.of(context)
                                              .curTheme
                                              .title2,
                                    )),
                                TextFormField(
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: StateContainer.of(context)
                                        .curTheme
                                        .title,
                                  ),
                                  focusNode: _focusNodeRevs,
                                  onTap: _requestFocusRevs,
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    isDense: true,
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: StateContainer.of(context)
                                              .curTheme
                                              .primary),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: StateContainer.of(context)
                                              .curTheme
                                              .title2),
                                    ),
                                  ),
                                  validator: (input) => null,
                                  onSaved: (input) => input.isNotEmpty
                                      ? _monthlyRevenues = input
                                      : _monthlyRevenues = '0',
                                  obscureText: false,
                                  inputFormatters: [
                                    //WhitelistingTextInputFormatter.digitsOnly,
                                    WhitelistingTextInputFormatter(RegExp(
                                        '^\$|^(0|([1-9][0-9]{0,}))((\\.|\\,)[0-9]{0,})?\$'))
                                  ],
                                ),
                                SizedBox(
                                  height: 32,
                                ),
                                Text(
                                    '💸 Tes dépenses mensuelles (environ, je ne suis pas à l’€ près)',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: _focusNodeMonthlyExps.hasFocus
                                          ? StateContainer.of(context)
                                              .curTheme
                                              .primary
                                          : StateContainer.of(context)
                                              .curTheme
                                              .title2,
                                    )),
                                TextFormField(
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: StateContainer.of(context)
                                        .curTheme
                                        .title,
                                  ),
                                  focusNode: _focusNodeMonthlyExps,
                                  onTap: _requestFocusMonthlyExps,
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    isDense: true,
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: StateContainer.of(context)
                                              .curTheme
                                              .primary),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: StateContainer.of(context)
                                              .curTheme
                                              .title2),
                                    ),
                                  ),
                                  validator: (input) => null,
                                  onSaved: (input) => input.isNotEmpty
                                      ? _monthlyExpenses = input
                                      : _monthlyExpenses = '0',
                                  obscureText: false,
                                  inputFormatters: [
                                    //WhitelistingTextInputFormatter.digitsOnly,
                                    WhitelistingTextInputFormatter(RegExp(
                                        '^\$|^(0|([1-9][0-9]{0,}))((\\.|\\,)[0-9]{0,})?\$'))
                                  ],
                                ),
                                SizedBox(
                                  height: 32,
                                ),
                                Text(
                                    '📊 Ton budget annuel (ce que tu dépenses environ chaque année pour les vacances, Noël, …)',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: _focusNodeAnnualExps.hasFocus
                                          ? StateContainer.of(context)
                                              .curTheme
                                              .primary
                                          : StateContainer.of(context)
                                              .curTheme
                                              .title2,
                                    )),
                                TextFormField(
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: StateContainer.of(context)
                                        .curTheme
                                        .title,
                                  ),
                                  focusNode: _focusNodeAnnualExps,
                                  onTap: _requestFocusAnnualExps,
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    isDense: true,
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: StateContainer.of(context)
                                              .curTheme
                                              .primary),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: StateContainer.of(context)
                                              .curTheme
                                              .title2),
                                    ),
                                  ),
                                  validator: (input) => null,
                                  onSaved: (input) => input.isNotEmpty
                                      ? _annualExpenses = input
                                      : _annualExpenses = '0',
                                  obscureText: false,
                                  inputFormatters: [
                                    //WhitelistingTextInputFormatter.digitsOnly,
                                    WhitelistingTextInputFormatter(RegExp(
                                        '^\$|^(0|([1-9][0-9]{0,}))((\\.|\\,)[0-9]{0,})?\$'))
                                  ],
                                ),
                              ],
                            ))),
                    Container(
                        alignment: Alignment.centerLeft,
                        //padding: const EdgeInsets.only(top: 20),
                        padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
                        child: Html(
                          data: outroText,
                          onLinkTap: (url) async {
                            if (await canLaunch(url)) {
                              await launch(url);
                            } else {
                              throw 'Could not launch ${url}';
                            }
                          },
                          style: {
                            "p": Style(
                              color: StateContainer.of(context).curTheme.title2,
                              fontSize: FontSize(16),
                              fontWeight: FontWeight.w500,
                              textAlign: TextAlign.left,
                              letterSpacing: 0.4,
                            ),
                            "ul": Style(
                              color: StateContainer.of(context).curTheme.title2,
                              fontSize: FontSize(16),
                              fontWeight: FontWeight.w500,
                              textAlign: TextAlign.left,
                              letterSpacing: 0.4,
                            ),
                            "li": Style(
                                color:
                                    StateContainer.of(context).curTheme.title2,
                                fontSize: FontSize(16),
                                fontWeight: FontWeight.w500,
                                textAlign: TextAlign.left,
                                letterSpacing: 0.4,
                                padding: const EdgeInsets.only(bottom: 10)),
                            "a": Style(
                              color:
                                  StateContainer.of(context).curTheme.primary,
                              fontSize: FontSize(16),
                              fontWeight: FontWeight.w500,
                              textDecoration: TextDecoration.underline,
                              textAlign: TextAlign.left,
                              letterSpacing: 0.4,
                            ),
                            "strong": Style(
                              color: StateContainer.of(context).curTheme.title2,
                              fontSize: FontSize(16),
                              fontWeight: FontWeight.bold,
                              textAlign: TextAlign.left,
                              letterSpacing: 0.4,
                            )
                          },
                        )),
                    SizedBox(
                      height: 24,
                    ),
                    Container(
                      margin: const EdgeInsets.only(bottom: 150),
                      child: FormButton.primaryDefault(
                        context: context,
                        heroTag: 'add_exp_revenues',
                        text: widget.firstConnection
                            ? 'C\'est bon !'
                            : 'C\'est parti !',
                        onPressed: _create,
                      ),
                    ),
                  ]))),
        ));
  }
}
