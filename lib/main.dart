// Flutter
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:getbeagle/models/screens/intro.dart';
import 'package:getbeagle/ui/screens/update_revenues_expenses_screen.dart';

// 3rd party
import 'package:oktoast/oktoast.dart';
import 'package:logger/logger.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

// Beagle
import 'package:getbeagle/utils/sharedprefsutil.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/appstate_container.dart';
import 'package:getbeagle/localization.dart';
import 'package:getbeagle/styles.dart';
import 'package:getbeagle/models/available_language.dart';
import 'package:getbeagle/utils/routes.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/models/api/user.dart';
import 'package:getbeagle/services/api/user.dart';
import 'package:getbeagle/services/api/asset.dart';
import 'package:getbeagle/services/api/recurrent_transactions.dart';
import 'package:getbeagle/services/firebase/analytics_service.dart';
import 'package:getbeagle/services/navigation_service.dart';
import 'package:getbeagle/models/screens/add_expenses_revenues.dart';
import 'package:getbeagle/models/screens/knowledge_content.dart';

// UI
import 'package:getbeagle/ui/screens/login_screen.dart';
import 'package:getbeagle/ui/screens/home_screen.dart';
import 'package:getbeagle/ui/screens/settings_screen.dart';
import 'package:getbeagle/ui/screens/create_project_screen.dart';
import 'package:getbeagle/ui/screens/update_password.dart';
import 'package:getbeagle/ui/screens/choose_interest_screen.dart';
import 'package:getbeagle/ui/screens/free_home_screen.dart';
import 'package:getbeagle/ui/screens/signup_screen.dart';
import 'package:getbeagle/ui/screens/add_asset_screen.dart';
import 'package:getbeagle/ui/screens/add_current_account_screen.dart';
import 'package:getbeagle/ui/screens/add_expenses_revenues_screen.dart';
import 'package:getbeagle/ui/screens/introduction_screen.dart';
import 'package:getbeagle/ui/screens/knowledge_content.dart';

Future main() async {
  // load environment variables file.
  await DotEnv().load('.env');

  WidgetsFlutterBinding.ensureInitialized();
  // Setup Service Provide
  setupServiceLocator();

  // Setup logger, only show warning and higher in release mode.
  if (kReleaseMode) {
    Logger.level = Level.warning;
  } else {
    Logger.level = Level.debug;
  }
  // Run app
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(StateContainer(child: App()));
  });
}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  void initState() {
    super.initState();
  }

  // This widget is the root of the application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        StateContainer.of(context).curTheme.statusBar);
    return OKToast(
      textStyle: AppStyles.textStyleSnackbar(context),
      backgroundColor: StateContainer.of(context).curTheme.background,
      child: MaterialApp(
        navigatorKey: sl.get<NavigationService>().navigatorKey,
        debugShowCheckedModeBanner: false,
        title: 'Beagle',
        theme: ThemeData(
          dialogBackgroundColor: StateContainer.of(context).curTheme.background,
          primaryColor: StateContainer.of(context).curTheme.primary,
          accentColor:
              StateContainer.of(context).curTheme.primary.withOpacity(0.10),
          backgroundColor: StateContainer.of(context).curTheme.background,
          //fontFamily: 'NunitoSans',
          brightness: Brightness.light,
        ),
        localizationsDelegates: [
          AppLocalizationsDelegate(StateContainer.of(context).curLanguage),
          GlobalMaterialLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate
        ],
        locale: StateContainer.of(context).curLanguage == null ||
                StateContainer.of(context).curLanguage.language ==
                    AvailableLanguage.DEFAULT
            ? null
            : StateContainer.of(context).curLanguage.getLocale(),
        supportedLocales: [
          const Locale("fr", "FR"),
          const Locale('en', 'US'), // English
          // Currency-default requires country included
          const Locale("en", "AU"),
          const Locale("en", "TW"),
          const Locale("en", "ZA"),
          const Locale("en", "US"),
          const Locale("fr", "BE"),
        ],
        navigatorObservers: [
          sl.get<AnalyticsService>().getAnalyticsObserver(),
        ],
        initialRoute: '/',
        onGenerateRoute: (RouteSettings settings) {
          switch (settings.name) {
            case '/':
              return NoTransitionRoute(
                builder: (_) => Splash(),
                settings: settings,
              );
            case '/home':
              bool _onLaunch = settings.arguments != null
                  ? settings.arguments as bool
                  : false;
              return NoTransitionRoute(
                builder: (_) => HomeScreen(onLaunch: _onLaunch),
                settings: settings,
              );
            case '/login':
              return NoTransitionRoute(
                builder: (_) => LoginScreen(),
                settings: settings,
              );
            case '/signup':
              return NoTransitionRoute(
                builder: (_) => SignupScreen(),
                settings: settings,
              );
            case '/update_password':
              return MaterialPageRoute(
                builder: (_) => UpdatePasswordScreen(),
                settings: settings,
              );
            case '/settings':
              return MaterialPageRoute(
                builder: (_) => SettingsScreen(),
                settings: settings,
              );
            case '/create_project':
              return MaterialPageRoute(
                builder: (_) => CreateProjectScreen(),
                settings: settings,
              );
            case '/add_asset':
              return MaterialPageRoute(
                builder: (_) => AddAssetScreen(),
                settings: settings,
              );
            case '/choose_interests':
              bool _firstConnection = settings.arguments != null
                  ? settings.arguments as bool
                  : false;
              return MaterialPageRoute(
                builder: (_) =>
                    ChooseInterestScreen(firstConnection: _firstConnection),
                settings: settings,
              );
            case '/free_home':
              bool _onLaunch = settings.arguments != null
                  ? settings.arguments as bool
                  : false;
              return NoTransitionRoute(
                builder: (_) => FreeHomeScreen(onLaunch: _onLaunch),
                settings: settings,
              );
            case '/knowledge_content':
              KnowledgeContentArgs args =
                  settings.arguments as KnowledgeContentArgs;

              return NoTransitionRoute(
                builder: (_) => KnowledgeContentScreen(
                    fullContent: args.fullContent,
                    nextSteps: args.nextSteps,
                    moreColor: args.moreColor,
                    isStarred: args.isStarred),
                settings: settings,
              );
            case '/intro_screen':
              IntroArgs args = settings.arguments as IntroArgs;
              return MaterialPageRoute(
                builder: (_) => IntroductionScreen(
                    firstConnection: args.firstConnection,
                    assetId: args.assetId,
                    hasRecurrentTransactions: args.hasRecurrentTransactions),
                settings: settings,
              );
            case '/add_current_account':
              bool arg = settings.arguments as bool;
              return MaterialPageRoute(
                builder: (_) => AddCurrentAccountScreen(firstConnection: arg),
                settings: settings,
              );
            case '/add_expenses_revenues':
              AddExpensesRevenuesArgs args =
                  settings.arguments as AddExpensesRevenuesArgs;
              return MaterialPageRoute(
                builder: (_) => AddExpensesRevenues(
                  assetId: args.assetId,
                  firstConnection: args.firstConnection,
                ),
                settings: settings,
              );
            case '/update_expenses_revenues':
              return MaterialPageRoute(
                builder: (_) => UpdateRevenuesExpensesScreen(),
                settings: settings,
              );
            default:
              return null;
          }
        },
      ),
    );
  }
}

/// Splash
/// Default page route that determines if user is logged in and routes them appropriately.
class Splash extends StatefulWidget {
  @override
  SplashState createState() => SplashState();
}

class SplashState extends State<Splash> with WidgetsBindingObserver {
  bool _hasCheckedLoggedIn;

  Future onLaunch() async {
    if (!_hasCheckedLoggedIn) {
      _hasCheckedLoggedIn = true;
    } else {
      return;
    }
    // Check if logged in.
    String jwt = await sl.get<ApiClient>().getJWT();
    bool isLoggedIn = jwt != null && jwt.isNotEmpty;

    if (isLoggedIn) {
      User user = await getCurrentUser(api: sl.get<ApiClient>(), jwt: jwt)
          .catchError((object) async {
        await sl.get<NavigationService>().navigateToReplacementNamed('/login');
      });
      await setUserPrefs(user);

      bool firstLaunch =
          (await sl.get<SharedPrefsUtil>().getUserFirstLaunch(user.email));

      String mainAsset = await getMainCurrentAccount(sl.get<ApiClient>());
      bool hasRecTransactions;
      if (mainAsset != null) {
        hasRecTransactions = await hasOnBoardingRecurrentTransaction(
            sl.get<ApiClient>(), mainAsset);
      }
      print(hasRecTransactions);

      if (firstLaunch || mainAsset == null || !hasRecTransactions) {
        await sl.get<SharedPrefsUtil>().setUserFirstLaunch(user.email);
        await sl.get<NavigationService>().navigateToReplacementNamed(
            '/intro_screen',
            arguments: IntroArgs(
                assetId: mainAsset,
                firstConnection: firstLaunch,
                hasRecurrentTransactions: hasRecTransactions));
      } else {
        await sl
            .get<NavigationService>()
            .navigateToReplacementNamed('/home', arguments: true);
      }
    } else {
      await sl.get<NavigationService>().navigateToReplacementNamed('/login');
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _hasCheckedLoggedIn = false;
    if (SchedulerBinding.instance.schedulerPhase ==
        SchedulerPhase.persistentCallbacks) {
      SchedulerBinding.instance.addPostFrameCallback((_) => onLaunch());
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // Account for user changing locale when leaving the app
    switch (state) {
      case AppLifecycleState.paused:
        super.didChangeAppLifecycleState(state);
        break;
      case AppLifecycleState.resumed:
        setLanguage();
        super.didChangeAppLifecycleState(state);
        break;
      default:
        super.didChangeAppLifecycleState(state);
        break;
    }
  }

  void setLanguage() {
    setState(() {
      StateContainer.of(context).deviceLocale = Localizations.localeOf(context);
    });
    sl.get<SharedPrefsUtil>().getLanguage().then((setting) {
      setState(() {
        StateContainer.of(context).updateLanguage(setting);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // This seems to be the earliest place we can retrieve the device Locale
    setLanguage();
    return Scaffold(
      backgroundColor: StateContainer.of(context).curTheme.background,
    );
  }
}
