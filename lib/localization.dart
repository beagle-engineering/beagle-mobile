import 'dart:async';

import 'package:flutter/material.dart';
import 'package:getbeagle/l10n/messages_all.dart';
import 'package:intl/intl.dart';
import 'package:getbeagle/models/available_language.dart';

/// Localization
class AppLocalization {
  static Locale currentLocale = Locale('fr', 'FR');

  static Future<AppLocalization> load(Locale locale) {
    currentLocale = locale;
    final String name =
        locale.countryCode == null ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return AppLocalization();
    });
  }

  static AppLocalization of(BuildContext context) {
    return Localizations.of<AppLocalization>(context, AppLocalization);
  }

  /// -- GENERIC ITEMS
  String get title {
    return Intl.message('GetBeagle',
        name: 'title', desc: 'Title of the application');
  }

  String get pinMethod {
    return Intl.message("PIN", desc: 'settings_pin_method', name: 'pinMethod');
  }

  String get biometricsMethod {
    return Intl.message("Biometrics",
        desc: 'settings_fingerprint_method', name: 'biometricsMethod');
  }

  String get systemDefault {
    return Intl.message("System Default",
        desc: 'settings_default_language_string', name: 'systemDefault');
  }

  String get login {
    return Intl.message("Login", desc: 'login', name: 'login');
  }

  String get signup {
    return Intl.message("Signup", desc: 'signup', name: 'signup');
  }

  String get password {
    return Intl.message("Password", desc: 'password', name: 'password');
  }

  String get noAccount {
    return Intl.message("No account?", desc: 'no_account', name: 'noAccount');
  }

  String get forgotPassword {
    return Intl.message("Forgot Password?",
        desc: 'forgot_password', name: 'forgotPassword');
  }

  /// -- END NON-TRANSLATABLE ITEMS
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalization> {
  final LanguageSetting languageSetting;

  const AppLocalizationsDelegate(this.languageSetting);

  @override
  bool isSupported(Locale locale) {
    return languageSetting != null;
  }

  @override
  Future<AppLocalization> load(Locale locale) {
    if (languageSetting.language == AvailableLanguage.DEFAULT) {
      return AppLocalization.load(locale);
    }
    return AppLocalization.load(Locale(languageSetting.getLocaleString()));
  }

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalization> old) {
    return false;
  }
}
