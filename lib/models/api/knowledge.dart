/// KnowledgeJourney encapsulates the data for a list of knowledge
/// content grouped under the same theme "journey".
class KnowledgeJourney {
  final String id;
  final String title;
  final String description;
  final List<KnowledgeContent> content = <KnowledgeContent>[];

  KnowledgeJourney({this.id, this.title, this.description});

  factory KnowledgeJourney.fromJson(Map<String, dynamic> json) {
    KnowledgeJourney kj = KnowledgeJourney(
      id: json['id'] as String,
      title: json['title'] as String,
      description: json['description'] as String,
    );

    for (final c in json['contents'] as List<dynamic>) {
      kj.content.add(KnowledgeContent.fromJson(c as Map<String, dynamic>));
    }
    return kj;
  }
}

/// The content of a single knowledge reference.
class KnowledgeContent {
  final String id;
  final String title;
  final String description;
  final String content;
  bool read;

  KnowledgeContent(
      {this.id, this.title, this.description, this.content, this.read});

  factory KnowledgeContent.fromJson(Map<String, dynamic> json) {
    return KnowledgeContent(
        id: json['id'] as String,
        title: json['title'] as String,
        description: json['description'] as String,
        content: json['content'] as String,
        read: json['read'] as bool);
  }
}
