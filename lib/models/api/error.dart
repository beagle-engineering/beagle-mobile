/// The unified error object returned by the API.
class APIError {
  final String title;
  final int status;
  final Map<String, List<String>> errors;

  APIError({this.title, this.status, this.errors});

  factory APIError.fromJson(Map<String, dynamic> json) {
    Map<String, List<String>> errors = Map<String, List<String>>();
    (json['errors'] as Map<String, dynamic>).forEach((key, value) {
      List<String> v = List<String>();
      for (final c in value as List<dynamic>) {
        v.add(c as String);
      }
      errors[key] = v;
    });
    return APIError(
      title: json['title'] as String,
      status: json['status'] as int,
      errors: errors,
    );
  }
}
