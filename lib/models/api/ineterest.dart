/// Interest encapsulates the interest of a user for a particular knowledge journey.
class Interest {
  final String id;
  final String title;
  final String description;
  bool isChosen;

  Interest({this.id, this.title, this.description, this.isChosen});

  factory Interest.fromJson(Map<String, dynamic> json) {
    return Interest(
        id: json['id'] as String,
        title: json['title'] as String,
        description: json['description'] as String,
        isChosen: json['isChoose'] as bool);
  }
}
