/// KnowledgeJourney encapsulates the data for a client's project.
class Project {
  final String id;
  final String name;
  final String description;
  final ProjectLabel label;
  final int goal;
  final int current;
  final DateTime startedAt;
  final DateTime createdAt;
  final DateTime endedAt;
  final bool active;

  Project(
      {this.id,
      this.name,
      this.description,
      this.label,
      this.goal,
      this.current,
      this.startedAt,
      this.createdAt,
      this.endedAt,
      this.active});

  factory Project.fromJson(Map<String, dynamic> json) {
    int goal;
    int current;
    try {
      goal = json['goal'] as int;
    } on CastError {
      goal = (json['goal'] as double).round();
    }

    try {
      current = json['current'] as int;
      ;
    } on CastError {
      current = (json['current'] as double).round();
    }

    return Project(
        id: json['id'] as String,
        name: json['name'] as String,
        description: json['description'] as String,
        goal: goal,
        current: current,
        startedAt: DateTime.parse(json['startedAt'] as String),
        createdAt: DateTime.parse(json['createdAt'] as String),
        endedAt: DateTime.parse(json['endedAt'] as String),
        active: json['active'] as bool,
        label: ProjectLabel.fromJson(json['label'] as Map<String, dynamic>));
  }

  Map<String, dynamic> toJson() {
    return {
      "name": name,
      "description": description != null ? description : "",
      "labelId": label.id,
      "goal": goal,
      "current": current,
      "startedAt": startedAt.toIso8601String(),
      "endedAt": endedAt != null ? endedAt.toIso8601String() : null,
      "active": active
    };
  }
}

class ProjectLabel {
  final String id;
  final String name;
  final String description;

  ProjectLabel({this.id, this.name, this.description});

  factory ProjectLabel.fromJson(Map<String, dynamic> json) {
    return ProjectLabel(
        id: json['id'] as String,
        name: json['name'] as String,
        description: json['description'] as String);
  }
}
