class Asset {
  final String id;
  final String name;
  final int balance;
  final String riskProfile;
  AssetProvider provider;
  AssetType type;

  Asset(
      {this.id,
      this.name,
      this.balance,
      this.riskProfile,
      this.type,
      this.provider});

  factory Asset.fromJson(Map<String, dynamic> json) {
    Asset asset = Asset(
      id: json['id'] as String,
      name: json['name'] as String,
      balance: json['balance'] as int,
      riskProfile: json['riskProfile'] as String,
    );
    asset.type = AssetType.fromJson(json['type'] as Map<String, dynamic>);
    asset.provider =
        AssetProvider.fromJson(json['provider'] as Map<String, dynamic>);
    return asset;
  }

  Map<String, dynamic> toJson() {
    return {
      "name": name,
      "balance": balance,
      "riskProfile": riskProfile,
      "typeId": type.id,
      "providerId": provider.id
    };
  }
}

/// The types of assets
class AssetType {
  final String id;
  final String name;

  AssetType({this.id, this.name});

  factory AssetType.fromJson(Map<String, dynamic> json) {
    return AssetType(id: json['id'] as String, name: json['name'] as String);
  }
}

// Financial provider of the asset.
class AssetProvider {
  final String id;
  final String name;

  AssetProvider({this.id, this.name});

  factory AssetProvider.fromJson(Map<String, dynamic> json) {
    return AssetProvider(
        id: json['id'] as String, name: json['name'] as String);
  }
}

Map<String, String> providersLogos = {
  "088030cb-1671-47e8-b34b-87857bc8fd0d": "bred.svg",
  "11beeee7-f9bb-4936-8c83-bb7d8977e37d": "revolut.svg",
  "187ca542-1ace-461d-91ea-b22cc338079a": "hellobank.svg",
  "19e9195a-de6a-4f42-9d6a-ee4d0dd4f060": "banquedesavoie.svg",
  "1b4f546c-657d-472b-b67d-9107cac08e75": "boursorama.svg",
  "22fe57eb-585e-4451-aecf-54f201873f23": "bnp.svg",
  "2deb4d27-4553-4c65-b719-9fdf2e11875e": "fortuneo.svg",
  "356c2142-9273-4870-ade1-646aa63ae563": "nalo.svg",
  "38d04602-886a-4f3a-8e5b-f9641dfe7198": "credit-mutuel.svg",
  "3c1e0576-61c7-4ff1-aafd-8ba2d9531fbd": "hsbc.svg",
  "56552a3d-09d3-4cd1-8c1a-2628ced6d923": "SG.svg",
  "6dc303ef-6e41-46cd-8f7e-b96914a00f60": "banque-postale.svg",
  "8fe77122-a4e1-4b0a-90b7-9c7f46eb6e38": "caisse-d-epargne.svg",
  "90a96c3b-24a2-4ed6-a565-79ec0d5bdd7c": "crdit-agricole.svg",
  "a10b19ed-b859-4f98-96af-1564b8fb0cec": "lcl.svg",
  "b5c81290-aada-4488-9c11-61ab77353166": "cic.svg",
  "cb732758-1db0-4a56-8c47-7cb12deb5a99": "yomoni.svg",
  "db785d74-3307-43bb-bcbb-71f883b8597f": "ing.svg",
  "e95a9d45-c292-4947-aded-8b573a0afda3": "n26.svg",
};
