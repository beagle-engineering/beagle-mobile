/// Action encapsulates a meta action
class RecommendedAction {
  final String id;
  final String name;
  final String description;
  final String currentStep;
  final bool done;
  final String projectId;
  final int order;
  final String url;

  RecommendedAction(
      {this.id,
      this.name,
      this.description,
      this.currentStep,
      this.done,
      this.projectId,
      this.order,
      this.url});

  factory RecommendedAction.fromJson(Map<String, dynamic> json) {
    return RecommendedAction(
        id: json['id'] as String,
        name: json['name'] as String,
        description: json['description'] as String,
        currentStep: json['currentStep'] as String,
        done: json['done'] as bool,
        projectId: json['projectId'] as String,
        order: json['order'] as int,
        url: json['url'] as String);
  }
}

/// The steps of each action and their next steps.
class RecommendedActionStep {
  final String id;
  final String name;
  final String content;
  final bool done;
  final String actionId;
  final List<ActionStepChoice> choices = <ActionStepChoice>[];

  RecommendedActionStep(
      {this.id, this.name, this.content, this.done, this.actionId});

  factory RecommendedActionStep.fromJson(Map<String, dynamic> json) {
    RecommendedActionStep step = RecommendedActionStep(
        id: json['id'] as String,
        name: json['name'] as String,
        content: json['content'] as String,
        actionId: json['actionId'] as String,
        done: json['done'] as bool);

    for (final c in json['choices'] as List<dynamic>) {
      step.choices.add(ActionStepChoice.fromJson(c as Map<String, dynamic>));
    }
    return step;
  }
}

class ActionStepChoice {
  final String id;
  final String label;
  final String value;
  final String type;
  final bool finalizing;
  final bool visited;

  ActionStepChoice(
      {this.id,
      this.label,
      this.value,
      this.type,
      this.finalizing,
      this.visited});

  factory ActionStepChoice.fromJson(Map<String, dynamic> json) {
    return ActionStepChoice(
        id: json['id'] as String,
        label: json['label'] as String,
        value: json['value'] as String,
        type: json['type'] as String,
        finalizing: json['finalizing'] as bool,
        visited: json['visited'] as bool);
  }

  bool isNextStep() {
    if (this.type == 'next_step') {
      return true;
    }
    return false;
  }

  bool isKnowledgeContent() {
    if (this.type == 'knowledge_content') {
      return true;
    }
    return false;
  }

  bool isExternalUrl() {
    if (this.type == 'external_url') {
      return true;
    }
    return false;
  }

  int getChoiceWeight() {
    switch (this.type) {
      case 'next_step':
        return 0;
      case 'knowledge_content':
        return 1;
      case 'external_url':
        return 2;
    }
    return 1000;
  }
}
