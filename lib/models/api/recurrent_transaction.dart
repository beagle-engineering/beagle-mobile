import 'package:getbeagle/models/api/asset.dart';

class RecurrentTransaction {
  final String id;
  final String label;
  final int amount;
  Duration frequency;
  final DateTime startedAt;
  final DateTime createdAt;
  final DateTime endedAt;
  Asset asset;

  RecurrentTransaction(
      {this.id,
      this.label,
      this.amount,
      this.frequency,
      this.startedAt,
      this.endedAt,
      this.createdAt,
      this.asset});

  factory RecurrentTransaction.fromJson(Map<String, dynamic> json) {
    RecurrentTransaction rt = RecurrentTransaction(
      id: json['id'] as String,
      label: json['label'] as String,
      amount: json['amount'] as int,
      startedAt: DateTime.parse(json['startedAt'] as String),
      createdAt: DateTime.parse(json['createdAt'] as String),
      endedAt: json['endedAt'] as String != null
          ? DateTime.parse(json['endedAt'] as String)
          : null,
    );
    rt.asset = Asset.fromJson(json['asset'] as Map<String, dynamic>);
    Map<String, dynamic> frequency = json['frequency'] as Map<String, dynamic>;
    rt.frequency = Duration(
      days: frequency['days'] as int,
      hours: frequency['hours'] as int,
      minutes: frequency['minutes'] as int,
      seconds: frequency['seconds'] as int,
    );
    return rt;
  }

  Map<String, dynamic> toJson() {
    return {
      "label": label,
      "amount": amount,
      "frequency": {
        "ticks": 0,
        "days": 0,
        "hours": 0,
        "milliseconds": 0,
        "minutes": 0,
        "seconds": 0,
        "totalDays": 0,
        "totalHours": 0,
        "totalMilliseconds": 0,
        "totalMinutes": 0,
        "totalSeconds": frequency.inSeconds
      },
      "startedAt": startedAt.toIso8601String(),
      "endedAt": endedAt != null ? endedAt.toIso8601String() : null,
      "assetId": asset.id,
    };
  }
}
