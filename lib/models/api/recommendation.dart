/// Recommendation encapsulates a recommendation pushed as a notification.
class Recommendation {
  final String id;
  final String title;
  final String content;
  final String resourceId;
  final String type;
  final String status;
  final String userId;

  Recommendation(
      {this.id,
      this.title,
      this.content,
      this.resourceId,
      this.type,
      this.status,
      this.userId});

  factory Recommendation.fromJson(Map<String, dynamic> json) {
    return Recommendation(
        id: json['id'] as String,
        title: json['title'] as String,
        content: json['content'] as String,
        resourceId: json['resourceId'] as String,
        type: json['type'] as String,
        status: json['status'] as String,
        userId: json['userId'] as String);
  }
}
