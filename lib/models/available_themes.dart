import 'package:flutter/material.dart';
import 'package:getbeagle/themes/themes.dart';
import 'package:getbeagle/themes/base.dart';

enum ThemeOptions { BEAGLECLASSIC, BEAGLEDARK }

/// Represent the app's theme. For now, only classic is available.
class ThemeSetting {
  ThemeOptions theme;

  ThemeSetting(this.theme);

  String getDisplayName(BuildContext context) {
    switch (theme) {
      case ThemeOptions.BEAGLEDARK:
        return "BeagleDark";
      case ThemeOptions.BEAGLECLASSIC:
      default:
        return "BeagleClassic";
    }
  }

  BaseTheme getTheme() {
    switch (theme) {
      case ThemeOptions.BEAGLEDARK:
        return BeagleDarkTheme();
      case ThemeOptions.BEAGLECLASSIC:
      default:
        return BeagleClassicTheme();
    }
  }

  // For saving to shared prefs
  int getIndex() {
    return theme.index;
  }
}
