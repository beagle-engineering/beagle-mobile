class AddExpensesRevenuesArgs {
  final String assetId;
  final bool firstConnection;
  final bool fromSettingsMenu;

  AddExpensesRevenuesArgs(
      {this.assetId, this.firstConnection, this.fromSettingsMenu});
}
