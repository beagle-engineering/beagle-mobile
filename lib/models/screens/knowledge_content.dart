import 'package:flutter/material.dart';
import 'package:getbeagle/models/api/knowledge.dart';

class KnowledgeContentArgs {
  final KnowledgeContent fullContent;
  final List<KnowledgeContent> nextSteps;
  final Color moreColor;
  final bool isStarred;

  KnowledgeContentArgs(
      {this.fullContent, this.nextSteps, this.moreColor, this.isStarred});
}
