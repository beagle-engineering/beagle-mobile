class IntroArgs {
  final String assetId;
  final bool firstConnection;
  final bool hasRecurrentTransactions;

  IntroArgs(
      {this.assetId, this.firstConnection, this.hasRecurrentTransactions});
}
