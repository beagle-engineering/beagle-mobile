import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get_it/get_it.dart';
import 'package:getbeagle/services/firebase/analytics_service.dart';
import 'package:getbeagle/services/navigation_service.dart';
import 'package:getbeagle/services/firebase/notification_service.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:logger/logger.dart';

import 'package:getbeagle/utils/sharedprefsutil.dart';

import 'services/firebase/dynamic_links_service.dart';

GetIt sl = GetIt.instance;

void setupServiceLocator() {
  sl.registerLazySingleton<Logger>(() => Logger(printer: PrettyPrinter()));
  //sl.registerLazySingleton<AccountService>(() => AccountService());
  sl.registerLazySingleton<SharedPrefsUtil>(() => SharedPrefsUtil());
  sl.registerLazySingleton<ApiClient>(
      () => ApiClient(DotEnv().env['BEAGLE_API_URL']));
  sl.registerLazySingleton<PushNotificationService>(
      () => PushNotificationService());
  sl.registerLazySingleton<NavigationService>(() => NavigationService());
  sl.registerLazySingleton(() => DynamicLinkService());
  sl.registerLazySingleton(() => AnalyticsService());
}
