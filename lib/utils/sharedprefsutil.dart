import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:getbeagle/models/available_themes.dart';
import 'package:getbeagle/models/available_language.dart';

/// Singleton wrapper for shared preferences
class SharedPrefsUtil {
  // Keys
  static const String first_launch_key = 'fkalium_first_launch';
  static const String seed_backed_up_key = 'fkalium_seed_backup';
  static const String cur_currency = 'fkalium_currency_pref';
  static const String cur_language = 'fkalium_language_pref';
  static const String cur_theme = 'fkalium_theme_pref';
  // For certain keystore incompatible androids
  static const String use_legacy_storage = 'fkalium_legacy_storage';

  static const String userFullNameKey = 'user_full_name';
  static const String userEmailKey = 'user_email';
  static const String userPhoneKey = 'user_phone';

  String _getUserFirstLaunchKey(String email) {
    return '${email}_first_launch';
  }

  // For plain-text data
  Future<void> set(String key, value) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    if (value is bool) {
      await sharedPreferences.setBool(key, value);
    } else if (value is String) {
      await sharedPreferences.setString(key, value);
    } else if (value is double) {
      await sharedPreferences.setDouble(key, value);
    } else if (value is int) {
      await sharedPreferences.setInt(key, value);
    }
  }

  Future<dynamic> get(String key, {dynamic defaultValue}) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return await sharedPreferences.get(key) ?? defaultValue;
  }

  // Setting user full name.
  Future<void> setUserFullName(String fullName) async {
    return await set(userFullNameKey, fullName);
  }

  // Get user full name if exists.
  Future<String> getUserFullName() async {
    return await get(userFullNameKey, defaultValue: '') as String;
  }

  // Setting user email.
  Future<void> setUserEmail(String email) async {
    return await set(userEmailKey, email);
  }

  // Get user email if exists.
  Future<String> getUserEmail() async {
    return await get(userEmailKey, defaultValue: '') as String;
  }

  // Setting user email.
  Future<void> setUserPhone(String phone) async {
    return await set(userPhoneKey, phone);
  }

  // Get user email if exists.
  Future<String> getUserPhone() async {
    return await get(userPhoneKey, defaultValue: '') as String;
  }

  // Setting first launch param by user.
  Future<void> setUserFirstLaunch(String email) async {
    return await set(_getUserFirstLaunchKey(email), false);
  }

  // Get first launch param by user.
  Future<bool> getUserFirstLaunch(String email) async {
    return await get(_getUserFirstLaunchKey(email), defaultValue: true) as bool;
  }

  // Reset first launch param by user.
  Future<void> resetUserFirstLaunch(String email) async {
    return await set(_getUserFirstLaunchKey(email), true);
  }

  Future<void> setLanguage(LanguageSetting language) async {
    return await set(cur_language, language.getIndex());
  }

  Future<LanguageSetting> getLanguage() async {
    // TODO fix this issue, no idea why the cast isn't working ... going UI a bit will fix this later on
    //return LanguageSetting(AvailableLanguage.values[await get(cur_language, defaultValue: AvailableLanguage.DEFAULT.index)]);
    return LanguageSetting(AvailableLanguage.DEFAULT);
  }

  Future<void> setTheme(ThemeSetting theme) async {
    return await set(cur_theme, theme.getIndex());
  }

  Future<ThemeSetting> getTheme() async {
    // TODO fix this issue, no idea why the cast isn't working ... going UI a bit will fix this later on
    //return ThemeSetting(ThemeOptions.values[await get(cur_theme, defaultValue: ThemeOptions.BEAGLECLASSIC.index)]);
    return ThemeSetting(ThemeOptions.BEAGLECLASSIC);
  }

  Future<dynamic> useLegacyStorage() async {
    return await get(use_legacy_storage, defaultValue: false);
  }

  Future<void> setUseLegacyStorage() async {
    await set(use_legacy_storage, true);
  }

  // For logging out
  Future<void> deleteAll() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //await prefs.remove(notification_enabled);
  }
}
