bool isFree(String plan) {
  return plan == 'free';
}

bool isPremium(String plan) {
  return plan == 'premium';
}
