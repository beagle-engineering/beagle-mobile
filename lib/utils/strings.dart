import 'package:intl/intl.dart';

String capitalize(String s) {
  return s[0].toUpperCase() + s.substring(1);
}

NumberFormat f = NumberFormat("# ###.##", "fr_FR");

String formatAmountInCts(int amount) {
  return '${f.format(amount / 100)}'.replaceAllMapped(RegExp(r"(\d+)(\d{3})"),
      (match) => "${match.group(1)} ${match.group(2)}");
}
