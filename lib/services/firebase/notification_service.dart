import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:getbeagle/models/api/knowledge.dart';
import 'package:getbeagle/models/screens/knowledge_content.dart';
import 'package:getbeagle/services/firebase/analytics_service.dart';
import 'package:getbeagle/services/navigation_service.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/services/api/knowledge.dart';
import 'package:getbeagle/ui/screens/knowledge_content.dart';

class PushNotificationService {
  final FirebaseMessaging _fcm = FirebaseMessaging();

  Future initialise() async {
    if (Platform.isIOS) {
      _fcm.requestNotificationPermissions(IosNotificationSettings());
    }

    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");

        _handleNotification(message: message, doOpen: false);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");

        _handleNotification(message: message, doOpen: true);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        _handleNotification(message: message, doOpen: true);
      },
    );

    String token = await _fcm.getToken();

    print("FirebaseMessaging token: $token");
  }

  void _handleNotification({Map<String, dynamic> message, bool doOpen}) async {
    final dynamic data = message['data'] ?? message;
    final String contentId = data['content_id'] as String;

    try {
      if (contentId != null && contentId.isNotEmpty && doOpen) {
        KnowledgeContent content =
            await getContentById(sl.get<ApiClient>(), contentId);

        await sl.get<AnalyticsService>().logEmailCampaign(content.title);

        await sl.get<NavigationService>().navigateToNamed('/knowledge_content',
            arguments: KnowledgeContentArgs(
                fullContent: content,
                nextSteps: List<KnowledgeContent>(),
                isStarred: false));
      }
    } catch (e) {
      await sl.get<NavigationService>().navigateToReplacementNamed('/login');
    }
  }

  void subscribe(String topic) async {
    await _fcm.subscribeToTopic(topic);
  }

  void unsubscribe(String topic) async {
    await _fcm.unsubscribeFromTopic(topic);
  }
}
