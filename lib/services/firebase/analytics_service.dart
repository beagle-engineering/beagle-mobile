import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

class AnalyticsService {
  final FirebaseAnalytics _analytics = FirebaseAnalytics();

  FirebaseAnalyticsObserver getAnalyticsObserver() =>
      FirebaseAnalyticsObserver(analytics: _analytics);

  Future logLogin() async {
    await _analytics.logLogin(loginMethod: 'email');
  }

  Future logSignUp() async {
    await _analytics.logSignUp(signUpMethod: 'email');
  }

  Future logContentView(String id, String title) async {
    await _analytics.logViewItem(
        itemId: id, itemName: title, itemCategory: 'knowledge_content');
  }

  Future logNotificationCampaign(String title) async {
    await _analytics.logCampaignDetails(
        source: 'push_notification_${DateTime.now()}',
        medium: 'push_notification',
        campaign: title);
  }

  Future logEmailCampaign(String title) async {
    await _analytics.logCampaignDetails(
        source: 'email_${DateTime.now()}', medium: 'email', campaign: title);
  }

  Future logEvent({String eventName}) async {
    await _analytics.logEvent(
      name: eventName,
    );
  }

  Future setUserProps({String uid, String email, String plan}) async {
    await _analytics.setUserId(uid);
    await _analytics.setUserProperty(name: 'email', value: email);
    await _analytics.setUserProperty(name: 'plan', value: plan);
  }
}
