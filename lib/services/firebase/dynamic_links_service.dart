import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:getbeagle/models/api/knowledge.dart';
import 'package:getbeagle/models/screens/knowledge_content.dart';
import 'package:getbeagle/services/firebase/analytics_service.dart';
import 'package:getbeagle/services/navigation_service.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/services/api/knowledge.dart';
import 'package:getbeagle/utils/sharedprefsutil.dart';

class DynamicLinkService {
  Future handleDynamicLinks() async {
    // 1. Get the initial dynamic link if the app is opened with a dynamic link
    final PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.getInitialLink();

    // 2. handle link that has been retrieved
    _handleDeepLink(data);

    // 3. Register a link callback to fire if the app is opened up from the background
    // using a dynamic link.
    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      // 3a. handle link that has been retrieved
      _handleDeepLink(dynamicLink);
    }, onError: (OnLinkErrorException e) async {
      print('Link Failed: ${e.message}');
    });
  }

  void _handleDeepLink(PendingDynamicLinkData data) async {
    final Uri deepLink = data?.link;
    if (deepLink != null) {
      print('_handleDeepLink | deeplink: $deepLink');
      if (deepLink.queryParameters.containsKey('content_id')) {
        String contentId = deepLink.queryParameters['content_id'];
        KnowledgeContent content =
            await getContentById(sl.get<ApiClient>(), contentId);

        await sl.get<AnalyticsService>().logEmailCampaign(content.title);

        await sl.get<NavigationService>().navigateToNamed('/knowledge_content',
            arguments: KnowledgeContentArgs(
                fullContent: content,
                nextSteps: List<KnowledgeContent>(),
                isStarred: false));
      } else if (deepLink.pathSegments.contains('interests')) {
        await sl
            .get<AnalyticsService>()
            .logEmailCampaign('new_journey_in_interest');
        await sl
            .get<NavigationService>()
            .navigateToNamed('/choose_interests', arguments: false);
      } else if ((deepLink.pathSegments.contains('add_project'))) {
        await sl.get<NavigationService>().navigateToNamed('/create_project');
      } else if (deepLink.pathSegments.contains('add_asset')) {
        await sl.get<NavigationService>().navigateToNamed('/add_asset');
      }
    }
  }
}
