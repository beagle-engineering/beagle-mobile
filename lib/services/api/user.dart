import 'dart:convert';
import 'dart:io';

import 'package:getbeagle/models/api/error.dart';
import 'package:getbeagle/models/api/user.dart';
import 'package:getbeagle/service_locator.dart';
import 'package:getbeagle/services/api/client.dart';
import 'package:getbeagle/utils/sharedprefsutil.dart';
import 'package:getbeagle/utils/strings.dart';

Future<APIError> signup(
    {ApiClient api,
    String email,
    String password,
    String firstName,
    String lastName}) async {
  final response = await api.client.post(
    api.baseURL + ApiClient.usersBasePathV1 + '/signUp',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'mail': email,
      'password': password,
      'firstName': firstName,
      'lastName': lastName
    }),
  );
  if (response.statusCode != 200) {
    Map<String, dynamic> respBody =
        json.decode(response.body) as Map<String, dynamic>;
    return APIError.fromJson(respBody);
  }
  return null;
}

Future<void> login(ApiClient api, String email, String password) async {
  final response = await api.client.post(
      api.baseURL + ApiClient.usersBasePathV1 + '/signIn',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{'mail': email, 'password': password}));
  if (response.statusCode == 200) {
    Map<String, dynamic> mapResp =
        json.decode(response.body) as Map<String, dynamic>;
    await api.setJWT('${mapResp["auth_token"]}');
  } else {
    throw Exception('login failed with code: ${response.statusCode}');
  }
}

Future<APIError> updatePassword(ApiClient api, String currentPassword,
    String newPassword, String newPasswordConfirmation) async {
  final String jwt = await api.getJWT();
  final response = await api.client
      .put(api.baseURL + ApiClient.usersBasePathV1 + '/me/password',
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            HttpHeaders.authorizationHeader: 'Bearer ${jwt}'
          },
          body: jsonEncode(<String, String>{
            'currentPassword': currentPassword,
            'newPassword': newPassword,
            'newPasswordConfirmation': newPasswordConfirmation
          }));
  if (response.statusCode != 200) {
    Map<String, dynamic> respBody =
        json.decode(response.body) as Map<String, dynamic>;
    return APIError.fromJson(respBody);
  }
  return null;
}

Future<User> getCurrentUser({ApiClient api, String jwt = ''}) async {
  if (jwt == null || jwt.isEmpty) {
    jwt = await api.getJWT();
  }
  final response = await api.client.get(
      api.baseURL + ApiClient.usersBasePathV1 + '/me',
      headers: {HttpHeaders.authorizationHeader: 'Bearer ${jwt}'});

  if (response.statusCode == 200) {
    Map<String, dynamic> respBody =
        json.decode(response.body) as Map<String, dynamic>;
    return User.fromJson(respBody);
  } else {
    throw Exception(
        'Failed to get current user data with code: ${response.statusCode}');
  }
}

Future<void> deleteUser({ApiClient api, String jwt = ''}) async {
  if (jwt == null || jwt.isEmpty) {
    jwt = await api.getJWT();
  }
  final response = await api.client.delete(
      api.baseURL + ApiClient.usersBasePathV1 + '/delete',
      headers: {HttpHeaders.authorizationHeader: 'Bearer ${jwt}'});

  if (response.statusCode != 200) {
    throw Exception(
        'Failed to delete user data with code: ${response.statusCode}');
  }
}

Future<void> setUserPrefs(User user) async {
  await sl.get<ApiClient>().deletePlan();
  await sl.get<ApiClient>().setPlan(user.plan);
  await sl.get<SharedPrefsUtil>().setUserEmail(user.email);
  if (user.firstName != null && user.lastName != null) {
    await sl.get<SharedPrefsUtil>().setUserFullName(
        '${capitalize(user.firstName)} ${capitalize(user.lastName)}');
  }
  if (user.phoneNumber != null) {
    await sl.get<SharedPrefsUtil>().setUserPhone(user.phoneNumber);
  }
}
