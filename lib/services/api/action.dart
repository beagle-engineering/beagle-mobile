import 'dart:convert';
import 'dart:io';

import 'package:getbeagle/models/api/action.dart';
import 'package:getbeagle/services/api/client.dart';

Future<RecommendedActionStep> getcurrentActionStep(
    ApiClient api, String actionId) async {
  final String jwt = await api.getJWT();
  final response = await api.client.get(
      api.baseURL +
          ApiClient.usersBasePathV1 +
          '/me/actions/${actionId}/currentStep',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer ${jwt}'
      });
  if (response.statusCode == 200) {
    Map<String, dynamic> respBody =
        json.decode(response.body) as Map<String, dynamic>;
    return RecommendedActionStep.fromJson(respBody);
  } else {
    throw Exception(
        'Failed to load current step  for acton ${actionId} with code: ${response.statusCode}');
  }
}

Future<void> visitChoice(
    ApiClient api, String actionId, String choiceId) async {
  final String jwt = await api.getJWT();
  final response = await api.client.put(
      api.baseURL +
          ApiClient.usersBasePathV1 +
          '/me/actions/${actionId}/currentStep/choices/${choiceId}/visit',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer ${jwt}'
      });
  if (response.statusCode != 200) {
    throw Exception(
        'Failed to visit choice ${choiceId} for currentStep of action ${actionId} as read with code: ${response.statusCode}');
  }
}

Future<List<RecommendedAction>> getCurrentUserActions(ApiClient api) async {
  final String jwt = await api.getJWT();
  final response = await api.client.get(
      api.baseURL + ApiClient.usersBasePathV2 + "/me/usersactions/all",
      headers: {HttpHeaders.authorizationHeader: 'Bearer ${jwt}'});

  if (response.statusCode == 200) {
    List<RecommendedAction> actions = <RecommendedAction>[];
    List<dynamic> respBody = json.decode(response.body) as List<dynamic>;
    for (final p in respBody) {
      actions.add(RecommendedAction.fromJson(p as Map<String, dynamic>));
    }
    return actions;
  } else {
    throw Exception(
        'Failed to load projects for current user with code: ${response.statusCode}');
  }
}

Future<List<RecommendedAction>> getProjectsActions(ApiClient api) async {
  final String jwt = await api.getJWT();
  final response = await api.client.get(
      api.baseURL + ApiClient.usersBasePathV2 + "/me/projectsactions/all",
      headers: {HttpHeaders.authorizationHeader: 'Bearer ${jwt}'});

  if (response.statusCode == 200) {
    List<RecommendedAction> actions = <RecommendedAction>[];
    List<dynamic> respBody = json.decode(response.body) as List<dynamic>;
    for (final p in respBody) {
      actions.add(RecommendedAction.fromJson(p as Map<String, dynamic>));
    }
    return actions;
  } else {
    throw Exception(
        'Failed to load projects for current user with code: ${response.statusCode}');
  }
}
