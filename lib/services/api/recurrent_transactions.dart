import 'dart:convert';
import 'dart:io';

import 'package:getbeagle/models/api/recurrent_transaction.dart';
import 'package:getbeagle/services/api/client.dart';

Future<List<RecurrentTransaction>> getCurrentUserRecurrentTransactions(
    ApiClient api) async {
  final String jwt = await api.getJWT();
  final response = await api.client.get(
      api.baseURL + ApiClient.usersBasePathV1 + "/me/recurrenttransactions",
      headers: {HttpHeaders.authorizationHeader: 'Bearer ${jwt}'});

  if (response.statusCode == 200) {
    List<RecurrentTransaction> rts = <RecurrentTransaction>[];
    List<dynamic> respBody = json.decode(response.body) as List<dynamic>;
    for (final p in respBody) {
      rts.add(RecurrentTransaction.fromJson(p as Map<String, dynamic>));
    }
    return rts;
  } else {
    throw Exception(
        'Failed to load projects for current user with code: ${response.statusCode}');
  }
}

Future<String> createRecurrentTransaction(
    ApiClient api, RecurrentTransaction rt) async {
  final String jwt = await api.getJWT();
  final jsonBody = jsonEncode(rt.toJson());
  final response = await api.client.post(
      api.baseURL + ApiClient.usersBasePathV1 + "/me/recurrenttransactions",
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${jwt}',
        HttpHeaders.contentTypeHeader: 'application/json'
      },
      body: jsonBody);

  if (response.statusCode == 200) {
    Map<String, dynamic> mapResp =
        json.decode(response.body) as Map<String, dynamic>;
    return mapResp['id'] as String;
  } else {
    throw Exception('Failed to create asset with code: ${response.statusCode}');
  }
}

Future<void> endRecurrentTransaction(
    {ApiClient api, DateTime endedAt, String recurrentTransactionId}) async {
  final String jwt = await api.getJWT();
  final jsonBody = '{"endedAt":"${endedAt.toIso8601String()}"}';
  final response = await api.client.put(
      api.baseURL +
          ApiClient.usersBasePathV1 +
          "/me/recurrenttransactions/${recurrentTransactionId}",
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${jwt}',
        HttpHeaders.contentTypeHeader: 'application/json'
      },
      body: jsonBody);

  if (response.statusCode != 200) {
    throw Exception('Failed to update asset with code: ${response.statusCode}');
  }
}

Future<bool> hasOnBoardingRecurrentTransaction(
    ApiClient api, String assetId) async {
  List<RecurrentTransaction> recurrentTransactions =
      await getCurrentUserRecurrentTransactions(api);
  for (RecurrentTransaction recurrentTransaction in recurrentTransactions) {
    if (recurrentTransaction.label == 'Revenu mensuel principal' &&
        recurrentTransaction.asset.id == assetId) {
      return true;
    }
  }
  return false;
}
