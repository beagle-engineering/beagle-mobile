import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' show Client;

class ApiClient {
  final String baseURL;
  Client client = Client();
  final _storage = FlutterSecureStorage();

  static const usersBasePathV1 = '/v1/users';
  static const usersBasePathV2 = '/v2/users';
  static const knowledgeBasePath = '/v1/knowledge';
  static const projectsBasePath = '/v1/projects';
  static const assetsBasePath = "/v1/assets";

  ApiClient(this.baseURL);

  Future<String> getJWT() async {
    String k = await _storage.read(key: "jwt");
    return k;
  }

  Future<void> setJWT(String jwt) async {
    await _storage.write(key: "jwt", value: jwt);
  }

  Future<void> deleteJWT() async {
    await _storage.delete(key: "jwt");
  }

  Future<String> getPlan() async {
    String k = await _storage.read(key: "plan");
    return k;
  }

  Future<void> setPlan(String plan) async {
    await _storage.write(key: "plan", value: plan);
  }

  Future<void> deletePlan() async {
    await _storage.delete(key: "plan");
  }
}
