import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

import 'package:getbeagle/models/api/error.dart';
import 'package:getbeagle/models/api/knowledge.dart';
import 'package:getbeagle/services/api/client.dart';

Future<List<KnowledgeJourney>> fetchAllKnowledge(ApiClient api) async {
  final String jwt = await api.getJWT();
  final response = await api.client.get(
      api.baseURL + ApiClient.knowledgeBasePath + '/journeys/all/contents',
      headers: {HttpHeaders.authorizationHeader: 'Bearer ${jwt}'});

  List<KnowledgeJourney> knowledgeJourneys = <KnowledgeJourney>[];
  if (response.statusCode == 200) {
    List<dynamic> respBody = json.decode(response.body) as List<dynamic>;
    for (final journey in respBody) {
      knowledgeJourneys
          .add(KnowledgeJourney.fromJson(journey as Map<String, dynamic>));
    }
  } else {
    throw Exception(
        'Failed to load knowledge journeys with code: ${response.statusCode}');
  }
  return knowledgeJourneys;
}

Future<KnowledgeContent> getContentById(ApiClient api, String id) async {
  final String jwt = await api.getJWT();
  final response = await api.client.get(
      api.baseURL + ApiClient.knowledgeBasePath + '/contents/' + id,
      headers: {HttpHeaders.authorizationHeader: 'Bearer ${jwt}'});

  if (response.statusCode == 200) {
    Map<String, dynamic> respBody =
        json.decode(response.body) as Map<String, dynamic>;
    return KnowledgeContent.fromJson(respBody);
  } else {
    throw Exception(
        'Failed to load knowledge content for step ${id} with code: ${response.statusCode}');
  }
}

Future<List<KnowledgeJourney>> getKnowledgeByInterest(ApiClient api) async {
  final String jwt = await api.getJWT();
  final response = await api.client.get(
      api.baseURL +
          ApiClient.usersBasePathV1 +
          '/me/knowledge/journeys/all/contents',
      headers: {HttpHeaders.authorizationHeader: 'Bearer ${jwt}'});

  List<KnowledgeJourney> knowledgeJourneys = <KnowledgeJourney>[];
  if (response.statusCode == 200) {
    List<dynamic> respBody = json.decode(response.body) as List<dynamic>;
    for (final journey in respBody) {
      knowledgeJourneys
          .add(KnowledgeJourney.fromJson(journey as Map<String, dynamic>));
    }
    if (knowledgeJourneys.isEmpty) {
      return fetchAllKnowledge(api);
    }
  } else if (response.statusCode == 404) {
    return fetchAllKnowledge(api);
  } else {
    throw Exception(
        'Failed to load knowledge journeys with code: ${response.statusCode}');
  }
  return knowledgeJourneys;
}

Future<APIError> readContent(ApiClient api, String contentId) async {
  final String jwt = await api.getJWT();
  final response = await api.client.post(
    api.baseURL + ApiClient.knowledgeBasePath + "/contents/${contentId}/read",
    headers: {
      HttpHeaders.authorizationHeader: 'Bearer ${jwt}',
      HttpHeaders.contentTypeHeader: 'application/json'
    },
  );

  if (response.statusCode != 200) {
    Map<String, dynamic> respBody =
        json.decode(response.body) as Map<String, dynamic>;
    return APIError.fromJson(respBody);
  }
  return null;
}

Future<void> pinContent(ApiClient api, String contentId) async {
  final String jwt = await api.getJWT();
  final response = await api.client.put(
    api.baseURL +
        ApiClient.usersBasePathV1 +
        "/me/knowledge/contents/${contentId}/pin",
    headers: {
      HttpHeaders.authorizationHeader: 'Bearer ${jwt}',
      HttpHeaders.contentTypeHeader: 'application/json'
    },
  );

  if (response.statusCode != 200) {
    throw Exception('Failed to pin content with code: ${response.statusCode}');
  }
  return null;
}

Future<void> unpinContent(ApiClient api, List<String> contentIds) async {
  final String jwt = await api.getJWT();
  final jsonBody = jsonEncode({"pinnedContentsIds": contentIds});
  final url = Uri.parse(
      api.baseURL + ApiClient.usersBasePathV1 + "/me/knowledge/pinned");
  final request = http.Request("DELETE", url);
  request.headers.addAll({
    HttpHeaders.authorizationHeader: 'Bearer ${jwt}',
    HttpHeaders.contentTypeHeader: 'application/json'
  });
  request.body = jsonBody;
  final response = await request.send();
  if (response.statusCode != 200) {
    throw Exception(
        'Failed to load unpin content with code: ${response.statusCode}');
  }
}

Future<List<KnowledgeContent>> getPinnedContent(ApiClient api) async {
  final String jwt = await api.getJWT();
  final response = await api.client.get(
    api.baseURL + ApiClient.usersBasePathV1 + "/me/knowledge/pinned",
    headers: {
      HttpHeaders.authorizationHeader: 'Bearer ${jwt}',
      HttpHeaders.contentTypeHeader: 'application/json'
    },
  );

  List<KnowledgeContent> knowledgeContents = <KnowledgeContent>[];
  if (response.statusCode == 200) {
    List<dynamic> respBody = json.decode(response.body) as List<dynamic>;
    for (final c in respBody) {
      knowledgeContents
          .add(KnowledgeContent.fromJson(c as Map<String, dynamic>));
    }
  } else if (response.statusCode == 404) {
    return [];
  } else {
    throw Exception(
        'Failed to load knowledge journeys with code: ${response.statusCode}');
  }
  return knowledgeContents;
}
