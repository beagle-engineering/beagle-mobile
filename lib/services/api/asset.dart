import 'dart:convert';
import 'dart:io';

import 'package:getbeagle/models/api/asset.dart';
import 'package:getbeagle/services/api/client.dart';

Future<List<Asset>> getCurrentUserAssets(ApiClient api) async {
  final String jwt = await api.getJWT();
  final response = await api.client.get(
      api.baseURL + ApiClient.usersBasePathV1 + "/me/assets",
      headers: {HttpHeaders.authorizationHeader: 'Bearer ${jwt}'});

  if (response.statusCode == 200) {
    List<Asset> assets = <Asset>[];
    List<dynamic> respBody = json.decode(response.body) as List<dynamic>;
    for (final p in respBody) {
      assets.add(Asset.fromJson(p as Map<String, dynamic>));
    }
    return assets;
  } else {
    throw Exception(
        'Failed to load projects for current user with code: ${response.statusCode}');
  }
}

Future<String> createAsset(ApiClient api, Asset asset) async {
  final String jwt = await api.getJWT();
  final jsonBody = jsonEncode(asset.toJson());
  final response = await api.client
      .post(api.baseURL + ApiClient.usersBasePathV1 + "/me/assets",
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer ${jwt}',
            HttpHeaders.contentTypeHeader: 'application/json'
          },
          body: jsonBody);

  if (response.statusCode == 200) {
    Map<String, dynamic> mapResp =
        json.decode(response.body) as Map<String, dynamic>;
    return mapResp['id'] as String;
  } else {
    throw Exception('Failed to create asset with code: ${response.statusCode}');
  }
}

Future<void> updateAsset(
    {ApiClient api,
    int balance,
    String assetId,
    String name,
    String riskProfile}) async {
  final String jwt = await api.getJWT();
  final jsonBody =
      '{"balance":${balance},"name":"${name}","riskProfile":"${riskProfile}"}';
  final response = await api.client
      .put(api.baseURL + ApiClient.usersBasePathV1 + "/me/assets/${assetId}",
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer ${jwt}',
            HttpHeaders.contentTypeHeader: 'application/json'
          },
          body: jsonBody);

  if (response.statusCode != 200) {
    throw Exception('Failed to update asset with code: ${response.statusCode}');
  }
}

Future<List<AssetType>> getAssetTypes(ApiClient api) async {
  final String jwt = await api.getJWT();
  final response = await api.client.get(
      api.baseURL + ApiClient.assetsBasePath + "/types",
      headers: {HttpHeaders.authorizationHeader: 'Bearer ${jwt}'});

  if (response.statusCode == 200) {
    List<AssetType> types = <AssetType>[];
    List<dynamic> respBody = json.decode(response.body) as List<dynamic>;
    for (final t in respBody) {
      types.add(AssetType.fromJson(t as Map<String, dynamic>));
    }
    types.sort((a, b) => a.name.compareTo(b.name));
    return types;
  } else {
    throw Exception(
        'Failed to load projects labels with code: ${response.statusCode}');
  }
}

Future<List<AssetProvider>> getAssetProviders(ApiClient api) async {
  final String jwt = await api.getJWT();
  final response = await api.client.get(
      api.baseURL + ApiClient.assetsBasePath + "/providers",
      headers: {HttpHeaders.authorizationHeader: 'Bearer ${jwt}'});

  if (response.statusCode == 200) {
    List<AssetProvider> providers = <AssetProvider>[];
    List<dynamic> respBody = json.decode(response.body) as List<dynamic>;
    for (final t in respBody) {
      providers.add(AssetProvider.fromJson(t as Map<String, dynamic>));
    }
    providers.sort((a, b) => a.name.compareTo(b.name));
    return providers;
  } else {
    throw Exception(
        'Failed to load projects labels with code: ${response.statusCode}');
  }
}

Future<void> deleteAsset(ApiClient api, String assetId) async {
  final String jwt = await api.getJWT();
  final response = await api.client.delete(
      api.baseURL + ApiClient.usersBasePathV1 + "/me/assets/${assetId}",
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${jwt}',
        HttpHeaders.contentTypeHeader: 'application/json'
      });

  if (response.statusCode != 200) {
    throw Exception(
        'Failed to delete project with code: ${response.statusCode}');
  }
}

Future<String> getMainCurrentAccount(ApiClient api) async {
  List<Asset> assets = await getCurrentUserAssets(api);
  for (Asset a in assets) {
    if (a.type.id == '46242805-5742-4c00-846f-dc6882db6c52') {
      return a.id;
    }
  }
  return null;
}
