import 'dart:convert';
import 'dart:io';

import 'package:getbeagle/models/api/action.dart';
import 'package:getbeagle/models/api/project.dart';
import 'package:getbeagle/services/api/client.dart';

Future<List<Project>> getCurrentUserProjects(ApiClient api) async {
  final String jwt = await api.getJWT();
  final response = await api.client.get(
      api.baseURL + ApiClient.usersBasePathV1 + "/me/projects",
      headers: {HttpHeaders.authorizationHeader: 'Bearer ${jwt}'});

  if (response.statusCode == 200) {
    List<Project> projects = <Project>[];
    List<dynamic> respBody = json.decode(response.body) as List<dynamic>;
    for (final p in respBody) {
      projects.add(Project.fromJson(p as Map<String, dynamic>));
    }
    return projects;
  } else {
    throw Exception(
        'Failed to load projects for current user with code: ${response.statusCode}');
  }
}

Future<List<ProjectLabel>> getProjectLabels(ApiClient api) async {
  final String jwt = await api.getJWT();
  final response = await api.client.get(
      api.baseURL + ApiClient.projectsBasePath + "/labels",
      headers: {HttpHeaders.authorizationHeader: 'Bearer ${jwt}'});

  if (response.statusCode == 200) {
    List<ProjectLabel> labels = <ProjectLabel>[];
    List<dynamic> respBody = json.decode(response.body) as List<dynamic>;
    for (final label in respBody) {
      labels.add(ProjectLabel.fromJson(label as Map<String, dynamic>));
    }
    return labels;
  } else {
    throw Exception(
        'Failed to load projects labels with code: ${response.statusCode}');
  }
}

Future<String> createProject(ApiClient api, Project project) async {
  final String jwt = await api.getJWT();
  final jsonBody = jsonEncode(project.toJson());
  final response = await api.client
      .post(api.baseURL + ApiClient.usersBasePathV1 + "/me/projects",
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer ${jwt}',
            HttpHeaders.contentTypeHeader: 'application/json'
          },
          body: jsonBody);

  if (response.statusCode == 200) {
    Map<String, dynamic> mapResp =
        json.decode(response.body) as Map<String, dynamic>;
    return mapResp['id'] as String;
  } else {
    throw Exception(
        'Failed to create project with code: ${response.statusCode}');
  }
}

Future<List> getCurrentUserProjectsAndActions(ApiClient api) async {
  final String jwt = await api.getJWT();
  final response = await api.client.get(
      api.baseURL +
          ApiClient.usersBasePathV1 +
          "/me/projects/all/actions?ActionDone=false",
      headers: {HttpHeaders.authorizationHeader: 'Bearer ${jwt}'});

  if (response.statusCode == 200) {
    List<Project> projects = <Project>[];
    List<RecommendedAction> actions = <RecommendedAction>[];
    List<dynamic> respBody = json.decode(response.body) as List<dynamic>;
    for (final p in respBody) {
      final pmap = p as Map<String, dynamic>;
      projects.add(Project.fromJson(pmap));
      for (final a in pmap['actions'] as List<dynamic>) {
        actions.add(RecommendedAction.fromJson(a as Map<String, dynamic>));
      }
    }
    return [projects, actions];
  } else {
    throw Exception(
        'Failed to load projects for current user with code: ${response.statusCode}');
  }
}

Future<void> deleteProject(ApiClient api, String projectId) async {
  final String jwt = await api.getJWT();
  final response = await api.client.delete(
      api.baseURL + ApiClient.usersBasePathV1 + "/me/projects/${projectId}",
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${jwt}',
        HttpHeaders.contentTypeHeader: 'application/json'
      });

  if (response.statusCode != 200) {
    throw Exception(
        'Failed to delete project with code: ${response.statusCode}');
  }
}

Future<void> updateProject(
    {ApiClient api, int current, String projectId, String name}) async {
  final String jwt = await api.getJWT();
  final jsonBody = '{"current":${current},"name":"${name}"}';
  final response = await api.client.put(
      api.baseURL + ApiClient.usersBasePathV1 + "/me/projects/${projectId}",
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${jwt}',
        HttpHeaders.contentTypeHeader: 'application/json'
      },
      body: jsonBody);

  if (response.statusCode != 200) {
    throw Exception(
        'Failed to update project with code: ${response.statusCode}');
  }
}
