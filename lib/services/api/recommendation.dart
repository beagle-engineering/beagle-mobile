import 'dart:convert';
import 'dart:io';

import 'package:getbeagle/models/api/recommendation.dart';
import 'package:getbeagle/services/api/client.dart';

Future<Recommendation> getNextRecommendation(ApiClient api) async {
  final String jwt = await api.getJWT();
  final response = await api.client.get(
      api.baseURL + ApiClient.usersBasePathV1 + '/me/next_recommendation',
      headers: {HttpHeaders.authorizationHeader: 'Bearer ${jwt}'});

  if (response.statusCode == 200) {
    List<dynamic> respBody = json.decode(response.body) as List<dynamic>;
    return Recommendation.fromJson(respBody[0] as Map<String, dynamic>);
  } else if (response.statusCode == 404) {
    return null;
  } else {
    throw Exception(
        'Failed to load the next_recommendation with code: ${response.statusCode}');
  }
}

Future<void> markRecommendationAsRead(ApiClient api, String id) async {
  final String jwt = await api.getJWT();
  final response = await api.client.put(
      api.baseURL +
          ApiClient.usersBasePathV1 +
          '/me/recommendations/${id}/read',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer ${jwt}'
      });
  if (response.statusCode != 200) {
    throw Exception(
        'Failed to mark recommendation ${id} as read with code: ${response.statusCode}');
  }
}
