import 'dart:convert';
import 'dart:io';

import 'package:getbeagle/models/api/ineterest.dart';
import 'package:getbeagle/services/api/client.dart';

Future<void> editInterests(ApiClient api, List<String> interestIds) async {
  Map<String, List<String>> body = {"interestsId": interestIds};
  final String jwt = await api.getJWT();
  final response = await api.client
      .post(api.baseURL + ApiClient.usersBasePathV1 + "/me/knowledge/interests",
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer ${jwt}',
            HttpHeaders.contentTypeHeader: 'application/json'
          },
          body: jsonEncode(body));

  if (response.statusCode != 200) {
    throw Exception(
        'Failed to create project with code: ${response.statusCode}');
  }
}

Future<List<Interest>> getInterests(ApiClient api) async {
  final String jwt = await api.getJWT();
  final response = await api.client.get(
      api.baseURL + ApiClient.usersBasePathV1 + "/me/knowledge/interests",
      headers: {HttpHeaders.authorizationHeader: 'Bearer ${jwt}'});

  List<Interest> interests = <Interest>[];
  if (response.statusCode == 200) {
    List<dynamic> respBody = json.decode(response.body) as List<dynamic>;
    for (final i in respBody) {
      interests.add(Interest.fromJson(i as Map<String, dynamic>));
    }
  } else {
    throw Exception(
        'Failed to load knowledge journeys with code: ${response.statusCode}');
  }
  return interests;
}
